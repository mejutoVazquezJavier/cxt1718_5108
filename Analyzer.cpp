#include "Analyzer.h"

#include "iostream"
#include "iomanip"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"

#include <vector>
#include <map>
#include <functional>
#include <iterator>

using namespace llvm;
using namespace std;

struct MapSort {
  bool operator()(const llvm::Function* MapA, const llvm::Function* MapB)
  const{
    return (MapA->getName().str() > MapB->getName().str());
  }
};

bool CallInstSort(const CallInst* callInstA, const CallInst* callInstB){
  return(callInstA->getCalledFunction()->getName().str() < callInstB->getCalledFunction()->getName().str());
}

void analyzeKeyPerformanceIndicators(const Module *M)
{
  // Collect statistics
  unsigned nFunctions = 0;
  unsigned nInstructions = 0;
  bool opm = false;

  std::map<const llvm::Function*, vector<const llvm::CallInst*>, MapSort> mymaplab2;
  std::map<const llvm::Function*, vector<const llvm::CallInst*>, MapSort> mymaplab3;
  std::string name;
  std::vector<std::string> myvectorlab1;

  for (const Function &F : *M) {
      nFunctions++;
      std::vector<const CallInst*> myvectorlab3;
      std::vector<const CallInst*> myvectorlab2;
      myvectorlab1.push_back(F.getName().str());
      for (const BasicBlock &BB : F) {
        for (const llvm::Instruction &I : BB) {
          if (isa< llvm::CallInst>(I)) {
            const  CallInst* callInst=dyn_cast< llvm::CallInst>(&I);
            myvectorlab2.push_back(callInst);
            nInstructions++;
            name = callInst->getCalledFunction()->getName().str();
            if (name.compare(0, 4, "omp_") == 0 || name.compare(0, 7, "__kmpc_") == 0){
                  myvectorlab3.push_back(callInst);
                  nInstructions++;
                  opm =true;
                }
          }
        }
      }

      std::sort(myvectorlab2.begin(), myvectorlab2.end(), CallInstSort);
      mymaplab2.insert(std::pair<const Function*, vector<const CallInst*>>(&F,myvectorlab2));

      if(opm == true){
        std::sort(myvectorlab3.begin(), myvectorlab3.end(), CallInstSort);
        mymaplab3.insert(std::pair<const Function*, vector<const CallInst*>>(&F,myvectorlab3));
        opm = false;
      }

   }

   std::sort(myvectorlab1.begin(), myvectorlab1.end());
   printf("LAB 3.1:");
   for (std::vector<std::string>::iterator it=myvectorlab1.begin(); it!=myvectorlab1.end(); ++it)
    std::cout << '\n' << *it;
    std::cout << '\n';

   printf("LAB 3.2:\n");
   std::map<const Function*, std::vector<const llvm::CallInst*>>::iterator
   iter=mymaplab2.begin();
   while(iter!=mymaplab2.end()){
     std::cout<<" "<<iter->first->getName().str()<<":\n";
     std::vector<const llvm::CallInst*>::iterator i=iter->second.begin();
     while(i!=iter->second.end()){
       std::cout<<"    "<<(*i)->getCalledFunction()->getName().str()<<"\n";
       i++;
     }
       iter++;
   }

   printf("LAB 3.3:\n");
   std::map<const Function*, std::vector<const llvm::CallInst*>>::iterator
   itera=mymaplab3.begin();
   while(itera!=mymaplab3.end()){
     std::cout<<" "<<itera->first->getName().str()<<":\n";
     std::vector<const llvm::CallInst*>::iterator i=itera->second.begin();
     while(i!=itera->second.end()){
       std::cout<<"    "<<(*i)->getCalledFunction()->getName().str()<<"\n";
       i++;
     }
       itera++;
   }


   /*for (std::vector<std::string>::iterator it=myvectorlab2.begin(); it!=myvectorlab2.end(); ++it)
    std::cout << '\n' << *it;
    std::cout << '\n';*/


  // Get benchmark name
  string bench = M->getName().str();

  //Dump statistics
  /*std::cout << "   " << std::right << std::setw(5) << std::setfill(' ')<< nFunctions
            << "   " << std::right << std::setw(9) << std::setfill(' ')<< nInstructions
            << "   " << bench
            << "\n";*/
}
