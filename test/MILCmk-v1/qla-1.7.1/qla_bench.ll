; ModuleID = 'qla_bench.c'
source_filename = "qla_bench.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%ident_t = type { i32, i32, i32, i32, i8* }
%struct.QLA_F_Complex = type { float, float }
%struct.cpu_set_t = type { [16 x i64] }

@.str = private unnamed_addr constant [20 x i8] c"QLA_Precision = %c\0A\00", align 1
@.str.1 = private unnamed_addr constant [18 x i8] c"OMP THREADS = %i\0A\00", align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"omp_get_wtick = %g\0A\00", align 1
@.str.3 = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) }, align 8
@.str.4 = private unnamed_addr constant [10 x i8] c"len = %i\0A\00", align 1
@.str.5 = private unnamed_addr constant [17 x i8] c"len/thread = %i\0A\00", align 1
@.str.7 = private unnamed_addr constant [7 x i8] c"%-32s:\00", align 1
@.str.8 = private unnamed_addr constant [22 x i8] c"QLA_V_vpeq_M_times_pV\00", align 1
@.str.9 = private unnamed_addr constant [40 x i8] c"%12g time=%5.2f mem=%5.0f mflops=%5.0f\0A\00", align 1
@.str.11 = private unnamed_addr constant [21 x i8] c"QLA_V_veq_Ma_times_V\00", align 1
@.str.13 = private unnamed_addr constant [14 x i8] c"QLA_V_vmeq_pV\00", align 1
@.str.15 = private unnamed_addr constant [29 x i8] c"QLA_D_vpeq_spproj_M_times_pD\00", align 1
@.str.17 = private unnamed_addr constant [21 x i8] c"QLA_M_veq_M_times_pM\00", align 1
@.str.19 = private unnamed_addr constant [18 x i8] c"QLA_r_veq_norm2_V\00", align 1
@.str.21 = private unnamed_addr constant [18 x i8] c"QLA_c_veq_V_dot_V\00", align 1

; Function Attrs: nounwind uwtable
define i8* @aligned_malloc(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = add i64 %5, 64
  %7 = call noalias i8* @malloc(i64 %6) #4
  %8 = ptrtoint i8* %7 to i64
  store i64 %8, i64* %3, align 8
  %9 = load i64, i64* %3, align 8
  %10 = urem i64 %9, 64
  store i64 %10, i64* %4, align 8
  %11 = load i64, i64* %4, align 8
  %12 = icmp ne i64 %11, 0
  br i1 %12, label %13, label %18

; <label>:13:                                     ; preds = %1
  %14 = load i64, i64* %4, align 8
  %15 = sub i64 64, %14
  %16 = load i64, i64* %3, align 8
  %17 = add i64 %16, %15
  store i64 %17, i64* %3, align 8
  br label %18

; <label>:18:                                     ; preds = %13, %1
  %19 = load i64, i64* %3, align 8
  %20 = inttoptr i64 %19 to i8*
  ret i8* %20
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #1

; Function Attrs: nounwind uwtable
define double @dtime() #0 {
  %1 = call double @omp_get_wtime() #4
  ret double %1
}

; Function Attrs: nounwind
declare double @omp_get_wtime() #1

; Function Attrs: nounwind uwtable
define void @set_R(float*, i32) #0 {
  %3 = alloca float*, align 8
  %4 = alloca i32, align 4
  store float* %0, float** %3, align 8
  store i32 %1, i32* %4, align 4
  %5 = load i32, i32* %4, align 4
  %6 = sitofp i32 %5 to double
  %7 = call double @cos(double %6) #4
  %8 = fadd double 1.000000e+00, %7
  %9 = fptrunc double %8 to float
  %10 = load float*, float** %3, align 8
  store float %9, float* %10, align 4
  ret void
}

; Function Attrs: nounwind
declare double @cos(double) #1

; Function Attrs: nounwind uwtable
define void @set_C(%struct.QLA_F_Complex*, i32) #0 {
  %3 = alloca %struct.QLA_F_Complex*, align 8
  %4 = alloca i32, align 4
  store %struct.QLA_F_Complex* %0, %struct.QLA_F_Complex** %3, align 8
  store i32 %1, i32* %4, align 4
  %5 = load i32, i32* %4, align 4
  %6 = sitofp i32 %5 to double
  %7 = call double @cos(double %6) #4
  %8 = fadd double 1.000000e+00, %7
  %9 = fptrunc double %8 to float
  %10 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %11 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %10, i32 0, i32 0
  store float %9, float* %11, align 8
  %12 = load i32, i32* %4, align 4
  %13 = sitofp i32 %12 to double
  %14 = call double @sin(double %13) #4
  %15 = fadd double 1.000000e+00, %14
  %16 = fptrunc double %15 to float
  %17 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %18 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %17, i32 0, i32 1
  store float %16, float* %18, align 4
  ret void
}

; Function Attrs: nounwind
declare double @sin(double) #1

; Function Attrs: nounwind uwtable
define void @set_V([3 x %struct.QLA_F_Complex]*, i32) #0 {
  %3 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store [3 x %struct.QLA_F_Complex]* %0, [3 x %struct.QLA_F_Complex]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %6

; <label>:6:                                      ; preds = %36, %2
  %7 = load i32, i32* %5, align 4
  %8 = icmp slt i32 %7, 3
  br i1 %8, label %9, label %39

; <label>:9:                                      ; preds = %6
  %10 = load i32, i32* %5, align 4
  %11 = add nsw i32 %10, 1
  %12 = sitofp i32 %11 to double
  %13 = load i32, i32* %4, align 4
  %14 = sitofp i32 %13 to double
  %15 = call double @cos(double %14) #4
  %16 = fadd double %12, %15
  %17 = fptrunc double %16 to float
  %18 = load i32, i32* %5, align 4
  %19 = sext i32 %18 to i64
  %20 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %21 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %20, i64 0, i64 %19
  %22 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %21, i32 0, i32 0
  store float %17, float* %22, align 8
  %23 = load i32, i32* %5, align 4
  %24 = add nsw i32 %23, 1
  %25 = sitofp i32 %24 to double
  %26 = load i32, i32* %4, align 4
  %27 = sitofp i32 %26 to double
  %28 = call double @sin(double %27) #4
  %29 = fadd double %25, %28
  %30 = fptrunc double %29 to float
  %31 = load i32, i32* %5, align 4
  %32 = sext i32 %31 to i64
  %33 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %34 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %33, i64 0, i64 %32
  %35 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %34, i32 0, i32 1
  store float %30, float* %35, align 4
  br label %36

; <label>:36:                                     ; preds = %9
  %37 = load i32, i32* %5, align 4
  %38 = add nsw i32 %37, 1
  store i32 %38, i32* %5, align 4
  br label %6

; <label>:39:                                     ; preds = %6
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_H([3 x [2 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [2 x %struct.QLA_F_Complex]]* %0, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %57, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %60

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %53, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 2
  br i1 %13, label %14, label %56

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = add nsw i32 %15, 4
  %17 = load i32, i32* %6, align 4
  %18 = add nsw i32 %17, 1
  %19 = mul nsw i32 %16, %18
  %20 = sitofp i32 %19 to double
  %21 = load i32, i32* %4, align 4
  %22 = sitofp i32 %21 to double
  %23 = call double @cos(double %22) #4
  %24 = fadd double %20, %23
  %25 = fptrunc double %24 to float
  %26 = load i32, i32* %6, align 4
  %27 = sext i32 %26 to i64
  %28 = load i32, i32* %5, align 4
  %29 = sext i32 %28 to i64
  %30 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %31 = getelementptr inbounds [3 x [2 x %struct.QLA_F_Complex]], [3 x [2 x %struct.QLA_F_Complex]]* %30, i64 0, i64 %29
  %32 = getelementptr inbounds [2 x %struct.QLA_F_Complex], [2 x %struct.QLA_F_Complex]* %31, i64 0, i64 %27
  %33 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %32, i32 0, i32 0
  store float %25, float* %33, align 8
  %34 = load i32, i32* %5, align 4
  %35 = add nsw i32 %34, 4
  %36 = load i32, i32* %6, align 4
  %37 = add nsw i32 %36, 1
  %38 = mul nsw i32 %35, %37
  %39 = sitofp i32 %38 to double
  %40 = load i32, i32* %4, align 4
  %41 = sitofp i32 %40 to double
  %42 = call double @sin(double %41) #4
  %43 = fadd double %39, %42
  %44 = fptrunc double %43 to float
  %45 = load i32, i32* %6, align 4
  %46 = sext i32 %45 to i64
  %47 = load i32, i32* %5, align 4
  %48 = sext i32 %47 to i64
  %49 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %50 = getelementptr inbounds [3 x [2 x %struct.QLA_F_Complex]], [3 x [2 x %struct.QLA_F_Complex]]* %49, i64 0, i64 %48
  %51 = getelementptr inbounds [2 x %struct.QLA_F_Complex], [2 x %struct.QLA_F_Complex]* %50, i64 0, i64 %46
  %52 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %51, i32 0, i32 1
  store float %44, float* %52, align 4
  br label %53

; <label>:53:                                     ; preds = %14
  %54 = load i32, i32* %6, align 4
  %55 = add nsw i32 %54, 1
  store i32 %55, i32* %6, align 4
  br label %11

; <label>:56:                                     ; preds = %11
  br label %57

; <label>:57:                                     ; preds = %56
  %58 = load i32, i32* %5, align 4
  %59 = add nsw i32 %58, 1
  store i32 %59, i32* %5, align 4
  br label %7

; <label>:60:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_D([3 x [4 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [4 x %struct.QLA_F_Complex]]* %0, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %57, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %60

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %53, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 4
  br i1 %13, label %14, label %56

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = add nsw i32 %15, 4
  %17 = load i32, i32* %6, align 4
  %18 = add nsw i32 %17, 1
  %19 = mul nsw i32 %16, %18
  %20 = sitofp i32 %19 to double
  %21 = load i32, i32* %4, align 4
  %22 = sitofp i32 %21 to double
  %23 = call double @cos(double %22) #4
  %24 = fadd double %20, %23
  %25 = fptrunc double %24 to float
  %26 = load i32, i32* %6, align 4
  %27 = sext i32 %26 to i64
  %28 = load i32, i32* %5, align 4
  %29 = sext i32 %28 to i64
  %30 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %31 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %30, i64 0, i64 %29
  %32 = getelementptr inbounds [4 x %struct.QLA_F_Complex], [4 x %struct.QLA_F_Complex]* %31, i64 0, i64 %27
  %33 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %32, i32 0, i32 0
  store float %25, float* %33, align 8
  %34 = load i32, i32* %5, align 4
  %35 = add nsw i32 %34, 4
  %36 = load i32, i32* %6, align 4
  %37 = add nsw i32 %36, 1
  %38 = mul nsw i32 %35, %37
  %39 = sitofp i32 %38 to double
  %40 = load i32, i32* %4, align 4
  %41 = sitofp i32 %40 to double
  %42 = call double @sin(double %41) #4
  %43 = fadd double %39, %42
  %44 = fptrunc double %43 to float
  %45 = load i32, i32* %6, align 4
  %46 = sext i32 %45 to i64
  %47 = load i32, i32* %5, align 4
  %48 = sext i32 %47 to i64
  %49 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %50 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %49, i64 0, i64 %48
  %51 = getelementptr inbounds [4 x %struct.QLA_F_Complex], [4 x %struct.QLA_F_Complex]* %50, i64 0, i64 %46
  %52 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %51, i32 0, i32 1
  store float %44, float* %52, align 4
  br label %53

; <label>:53:                                     ; preds = %14
  %54 = load i32, i32* %6, align 4
  %55 = add nsw i32 %54, 1
  store i32 %55, i32* %6, align 4
  br label %11

; <label>:56:                                     ; preds = %11
  br label %57

; <label>:57:                                     ; preds = %56
  %58 = load i32, i32* %5, align 4
  %59 = add nsw i32 %58, 1
  store i32 %59, i32* %5, align 4
  br label %7

; <label>:60:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_M([3 x [3 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [3 x %struct.QLA_F_Complex]]* %0, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %64, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %67

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %60, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 3
  br i1 %13, label %14, label %63

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = load i32, i32* %6, align 4
  %17 = sub nsw i32 %15, %16
  %18 = add nsw i32 %17, 3
  %19 = add nsw i32 %18, 1
  %20 = load i32, i32* %5, align 4
  %21 = load i32, i32* %6, align 4
  %22 = add nsw i32 %20, %21
  %23 = add nsw i32 %22, 1
  %24 = mul nsw i32 %19, %23
  %25 = srem i32 %24, 19
  %26 = sitofp i32 %25 to double
  %27 = load i32, i32* %4, align 4
  %28 = sitofp i32 %27 to double
  %29 = call double @cos(double %28) #4
  %30 = fadd double %26, %29
  %31 = fptrunc double %30 to float
  %32 = load i32, i32* %6, align 4
  %33 = sext i32 %32 to i64
  %34 = load i32, i32* %5, align 4
  %35 = sext i32 %34 to i64
  %36 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %37 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %36, i64 0, i64 %35
  %38 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %37, i64 0, i64 %33
  %39 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %38, i32 0, i32 0
  store float %31, float* %39, align 8
  %40 = load i32, i32* %5, align 4
  %41 = add nsw i32 %40, 4
  %42 = load i32, i32* %6, align 4
  %43 = add nsw i32 %42, 1
  %44 = mul nsw i32 %41, %43
  %45 = srem i32 %44, 17
  %46 = sitofp i32 %45 to double
  %47 = load i32, i32* %4, align 4
  %48 = sitofp i32 %47 to double
  %49 = call double @sin(double %48) #4
  %50 = fadd double %46, %49
  %51 = fptrunc double %50 to float
  %52 = load i32, i32* %6, align 4
  %53 = sext i32 %52 to i64
  %54 = load i32, i32* %5, align 4
  %55 = sext i32 %54 to i64
  %56 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %57 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %56, i64 0, i64 %55
  %58 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %57, i64 0, i64 %53
  %59 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %58, i32 0, i32 1
  store float %51, float* %59, align 4
  br label %60

; <label>:60:                                     ; preds = %14
  %61 = load i32, i32* %6, align 4
  %62 = add nsw i32 %61, 1
  store i32 %62, i32* %6, align 4
  br label %11

; <label>:63:                                     ; preds = %11
  br label %64

; <label>:64:                                     ; preds = %63
  %65 = load i32, i32* %5, align 4
  %66 = add nsw i32 %65, 1
  store i32 %66, i32* %5, align 4
  br label %7

; <label>:67:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define float @sum_C(%struct.QLA_F_Complex*, i32) #0 {
  %3 = alloca %struct.QLA_F_Complex*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store %struct.QLA_F_Complex* %0, %struct.QLA_F_Complex** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %10 = bitcast %struct.QLA_F_Complex* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 8
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_V([3 x %struct.QLA_F_Complex]*, i32) #0 {
  %3 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x %struct.QLA_F_Complex]* %0, [3 x %struct.QLA_F_Complex]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %10 = bitcast [3 x %struct.QLA_F_Complex]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 24
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_H([3 x [2 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [2 x %struct.QLA_F_Complex]]* %0, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [2 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 48
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_D([3 x [4 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [4 x %struct.QLA_F_Complex]]* %0, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [4 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 96
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_M([3 x [3 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [3 x %struct.QLA_F_Complex]]* %0, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [3 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 72
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca float, align 4
  %7 = alloca float*, align 8
  %8 = alloca %struct.QLA_F_Complex*, align 8
  %9 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %10 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %11 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %12 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %13 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %14 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %15 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %16 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %17 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %18 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %19 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %20 = alloca [3 x [2 x %struct.QLA_F_Complex]]**, align 8
  %21 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %22 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %28 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %29 = alloca double, align 8
  %30 = alloca double, align 8
  %31 = alloca double, align 8
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca double, align 8
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  %40 = alloca i32, align 4
  %41 = alloca i32, align 4
  %42 = alloca i32, align 4
  %43 = alloca i32, align 4
  %44 = alloca i32, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i32 1, i32* %35, align 4
  %45 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i32 70)
  %46 = call i32 @omp_get_max_threads() #4
  store i32 %46, i32* %35, align 4
  %47 = load i32, i32* %35, align 4
  %48 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.1, i32 0, i32 0), i32 %47)
  %49 = call double @omp_get_wtick() #4
  %50 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), double %49)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 0, void (i32*, i32*, ...)* bitcast (void (i32*, i32*)* @.omp_outlined. to void (i32*, i32*, ...)*))
  %51 = load i32, i32* %35, align 4
  %52 = mul nsw i32 64, %51
  store i32 %52, i32* %32, align 4
  %53 = load i32, i32* %35, align 4
  %54 = mul nsw i32 262144, %53
  store i32 %54, i32* %33, align 4
  %55 = load i32, i32* %33, align 4
  %56 = sext i32 %55 to i64
  %57 = mul i64 %56, 4
  %58 = call i8* @aligned_malloc(i64 %57)
  %59 = bitcast i8* %58 to float*
  store float* %59, float** %7, align 8
  %60 = load i32, i32* %33, align 4
  %61 = sext i32 %60 to i64
  %62 = mul i64 %61, 8
  %63 = call i8* @aligned_malloc(i64 %62)
  %64 = bitcast i8* %63 to %struct.QLA_F_Complex*
  store %struct.QLA_F_Complex* %64, %struct.QLA_F_Complex** %8, align 8
  %65 = load i32, i32* %33, align 4
  %66 = sext i32 %65 to i64
  %67 = mul i64 %66, 24
  %68 = call i8* @aligned_malloc(i64 %67)
  %69 = bitcast i8* %68 to [3 x %struct.QLA_F_Complex]*
  store [3 x %struct.QLA_F_Complex]* %69, [3 x %struct.QLA_F_Complex]** %9, align 8
  %70 = load i32, i32* %33, align 4
  %71 = sext i32 %70 to i64
  %72 = mul i64 %71, 24
  %73 = call i8* @aligned_malloc(i64 %72)
  %74 = bitcast i8* %73 to [3 x %struct.QLA_F_Complex]*
  store [3 x %struct.QLA_F_Complex]* %74, [3 x %struct.QLA_F_Complex]** %10, align 8
  %75 = load i32, i32* %33, align 4
  %76 = sext i32 %75 to i64
  %77 = mul i64 %76, 8
  %78 = call i8* @aligned_malloc(i64 %77)
  %79 = bitcast i8* %78 to [3 x %struct.QLA_F_Complex]**
  store [3 x %struct.QLA_F_Complex]** %79, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %80 = load i32, i32* %33, align 4
  %81 = sext i32 %80 to i64
  %82 = mul i64 %81, 96
  %83 = call i8* @aligned_malloc(i64 %82)
  %84 = bitcast i8* %83 to [3 x [4 x %struct.QLA_F_Complex]]*
  store [3 x [4 x %struct.QLA_F_Complex]]* %84, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %85 = load i32, i32* %33, align 4
  %86 = sext i32 %85 to i64
  %87 = mul i64 %86, 96
  %88 = call i8* @aligned_malloc(i64 %87)
  %89 = bitcast i8* %88 to [3 x [4 x %struct.QLA_F_Complex]]*
  store [3 x [4 x %struct.QLA_F_Complex]]* %89, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %90 = load i32, i32* %33, align 4
  %91 = sext i32 %90 to i64
  %92 = mul i64 %91, 8
  %93 = call i8* @aligned_malloc(i64 %92)
  %94 = bitcast i8* %93 to [3 x [4 x %struct.QLA_F_Complex]]**
  store [3 x [4 x %struct.QLA_F_Complex]]** %94, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %95 = load i32, i32* %33, align 4
  %96 = sext i32 %95 to i64
  %97 = mul i64 %96, 72
  %98 = call i8* @aligned_malloc(i64 %97)
  %99 = bitcast i8* %98 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %99, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %100 = load i32, i32* %33, align 4
  %101 = sext i32 %100 to i64
  %102 = mul i64 %101, 72
  %103 = call i8* @aligned_malloc(i64 %102)
  %104 = bitcast i8* %103 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %104, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %105 = load i32, i32* %33, align 4
  %106 = sext i32 %105 to i64
  %107 = mul i64 %106, 72
  %108 = call i8* @aligned_malloc(i64 %107)
  %109 = bitcast i8* %108 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %109, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %110 = load i32, i32* %33, align 4
  %111 = sext i32 %110 to i64
  %112 = mul i64 %111, 8
  %113 = call i8* @aligned_malloc(i64 %112)
  %114 = bitcast i8* %113 to [3 x [3 x %struct.QLA_F_Complex]]**
  store [3 x [3 x %struct.QLA_F_Complex]]** %114, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %115 = load i32, i32* %32, align 4
  store i32 %115, i32* %36, align 4
  br label %116

; <label>:116:                                    ; preds = %503, %2
  %117 = load i32, i32* %36, align 4
  %118 = load i32, i32* %33, align 4
  %119 = icmp sle i32 %117, %118
  br i1 %119, label %120, label %506

; <label>:120:                                    ; preds = %116
  %121 = load i32, i32* %36, align 4
  %122 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.4, i32 0, i32 0), i32 %121)
  %123 = load i32, i32* %36, align 4
  %124 = load i32, i32* %35, align 4
  %125 = sdiv i32 %123, %124
  %126 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.5, i32 0, i32 0), i32 %125)
  %127 = load i32, i32* %36, align 4
  %128 = sitofp i32 %127 to double
  %129 = fdiv double 9.000000e+09, %128
  store double %129, double* %37, align 8
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..6 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 1.440000e+02, double* %30, align 8
  store double 7.200000e+01, double* %29, align 8
  %130 = load double, double* %37, align 8
  %131 = load double, double* %29, align 8
  %132 = load double, double* %30, align 8
  %133 = fadd double %131, %132
  %134 = fdiv double %130, %133
  %135 = fadd double 1.000000e+00, %134
  %136 = fptosi double %135 to i32
  store i32 %136, i32* %34, align 4
  %137 = call double @dtime()
  store double %137, double* %31, align 8
  store i32 0, i32* %38, align 4
  br label %138

; <label>:138:                                    ; preds = %147, %120
  %139 = load i32, i32* %38, align 4
  %140 = load i32, i32* %34, align 4
  %141 = icmp slt i32 %139, %140
  br i1 %141, label %142, label %150

; <label>:142:                                    ; preds = %138
  %143 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %144 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %145 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %146 = load i32, i32* %36, align 4
  call void @QLA_F3_V_vpeq_M_times_pV([3 x %struct.QLA_F_Complex]* %143, [3 x [3 x %struct.QLA_F_Complex]]* %144, [3 x %struct.QLA_F_Complex]** %145, i32 %146)
  br label %147

; <label>:147:                                    ; preds = %142
  %148 = load i32, i32* %38, align 4
  %149 = add nsw i32 %148, 1
  store i32 %149, i32* %38, align 4
  br label %138

; <label>:150:                                    ; preds = %138
  %151 = call double @dtime()
  %152 = load double, double* %31, align 8
  %153 = fsub double %151, %152
  store double %153, double* %31, align 8
  %154 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %155 = load i32, i32* %36, align 4
  %156 = call float @sum_V([3 x %struct.QLA_F_Complex]* %154, i32 %155)
  store float %156, float* %6, align 4
  %157 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.8, i32 0, i32 0))
  %158 = load float, float* %6, align 4
  %159 = fpext float %158 to double
  %160 = load double, double* %31, align 8
  %161 = load double, double* %30, align 8
  %162 = load i32, i32* %36, align 4
  %163 = sitofp i32 %162 to double
  %164 = fmul double %161, %163
  %165 = load i32, i32* %34, align 4
  %166 = sitofp i32 %165 to double
  %167 = fmul double %164, %166
  %168 = load double, double* %31, align 8
  %169 = fmul double 1.000000e+06, %168
  %170 = fdiv double %167, %169
  %171 = load double, double* %29, align 8
  %172 = load i32, i32* %36, align 4
  %173 = sitofp i32 %172 to double
  %174 = fmul double %171, %173
  %175 = load i32, i32* %34, align 4
  %176 = sitofp i32 %175 to double
  %177 = fmul double %174, %176
  %178 = load double, double* %31, align 8
  %179 = fmul double 1.000000e+06, %178
  %180 = fdiv double %177, %179
  %181 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %159, double %160, double %170, double %180)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..10 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 1.200000e+02, double* %30, align 8
  store double 6.600000e+01, double* %29, align 8
  %182 = load double, double* %37, align 8
  %183 = load double, double* %29, align 8
  %184 = load double, double* %30, align 8
  %185 = fadd double %183, %184
  %186 = fdiv double %182, %185
  %187 = fadd double 1.000000e+00, %186
  %188 = fptosi double %187 to i32
  store i32 %188, i32* %34, align 4
  %189 = call double @dtime()
  store double %189, double* %31, align 8
  store i32 0, i32* %39, align 4
  br label %190

; <label>:190:                                    ; preds = %199, %150
  %191 = load i32, i32* %39, align 4
  %192 = load i32, i32* %34, align 4
  %193 = icmp slt i32 %191, %192
  br i1 %193, label %194, label %202

; <label>:194:                                    ; preds = %190
  %195 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %196 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %197 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %198 = load i32, i32* %36, align 4
  call void @QLA_F3_V_veq_Ma_times_V([3 x %struct.QLA_F_Complex]* %195, [3 x [3 x %struct.QLA_F_Complex]]* %196, [3 x %struct.QLA_F_Complex]* %197, i32 %198)
  br label %199

; <label>:199:                                    ; preds = %194
  %200 = load i32, i32* %39, align 4
  %201 = add nsw i32 %200, 1
  store i32 %201, i32* %39, align 4
  br label %190

; <label>:202:                                    ; preds = %190
  %203 = call double @dtime()
  %204 = load double, double* %31, align 8
  %205 = fsub double %203, %204
  store double %205, double* %31, align 8
  %206 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %207 = load i32, i32* %36, align 4
  %208 = call float @sum_V([3 x %struct.QLA_F_Complex]* %206, i32 %207)
  store float %208, float* %6, align 4
  %209 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.11, i32 0, i32 0))
  %210 = load float, float* %6, align 4
  %211 = fpext float %210 to double
  %212 = load double, double* %31, align 8
  %213 = load double, double* %30, align 8
  %214 = load i32, i32* %36, align 4
  %215 = sitofp i32 %214 to double
  %216 = fmul double %213, %215
  %217 = load i32, i32* %34, align 4
  %218 = sitofp i32 %217 to double
  %219 = fmul double %216, %218
  %220 = load double, double* %31, align 8
  %221 = fmul double 1.000000e+06, %220
  %222 = fdiv double %219, %221
  %223 = load double, double* %29, align 8
  %224 = load i32, i32* %36, align 4
  %225 = sitofp i32 %224 to double
  %226 = fmul double %223, %225
  %227 = load i32, i32* %34, align 4
  %228 = sitofp i32 %227 to double
  %229 = fmul double %226, %228
  %230 = load double, double* %31, align 8
  %231 = fmul double 1.000000e+06, %230
  %232 = fdiv double %229, %231
  %233 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %211, double %212, double %222, double %232)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..12 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 7.200000e+01, double* %30, align 8
  store double 6.000000e+00, double* %29, align 8
  %234 = load double, double* %37, align 8
  %235 = load double, double* %29, align 8
  %236 = load double, double* %30, align 8
  %237 = fadd double %235, %236
  %238 = fdiv double %234, %237
  %239 = fadd double 1.000000e+00, %238
  %240 = fptosi double %239 to i32
  store i32 %240, i32* %34, align 4
  %241 = call double @dtime()
  store double %241, double* %31, align 8
  store i32 0, i32* %40, align 4
  br label %242

; <label>:242:                                    ; preds = %250, %202
  %243 = load i32, i32* %40, align 4
  %244 = load i32, i32* %34, align 4
  %245 = icmp slt i32 %243, %244
  br i1 %245, label %246, label %253

; <label>:246:                                    ; preds = %242
  %247 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %248 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %249 = load i32, i32* %36, align 4
  call void @QLA_F3_V_vmeq_pV([3 x %struct.QLA_F_Complex]* %247, [3 x %struct.QLA_F_Complex]** %248, i32 %249)
  br label %250

; <label>:250:                                    ; preds = %246
  %251 = load i32, i32* %40, align 4
  %252 = add nsw i32 %251, 1
  store i32 %252, i32* %40, align 4
  br label %242

; <label>:253:                                    ; preds = %242
  %254 = call double @dtime()
  %255 = load double, double* %31, align 8
  %256 = fsub double %254, %255
  store double %256, double* %31, align 8
  %257 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %258 = load i32, i32* %36, align 4
  %259 = call float @sum_V([3 x %struct.QLA_F_Complex]* %257, i32 %258)
  store float %259, float* %6, align 4
  %260 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.13, i32 0, i32 0))
  %261 = load float, float* %6, align 4
  %262 = fpext float %261 to double
  %263 = load double, double* %31, align 8
  %264 = load double, double* %30, align 8
  %265 = load i32, i32* %36, align 4
  %266 = sitofp i32 %265 to double
  %267 = fmul double %264, %266
  %268 = load i32, i32* %34, align 4
  %269 = sitofp i32 %268 to double
  %270 = fmul double %267, %269
  %271 = load double, double* %31, align 8
  %272 = fmul double 1.000000e+06, %271
  %273 = fdiv double %270, %272
  %274 = load double, double* %29, align 8
  %275 = load i32, i32* %36, align 4
  %276 = sitofp i32 %275 to double
  %277 = fmul double %274, %276
  %278 = load i32, i32* %34, align 4
  %279 = sitofp i32 %278 to double
  %280 = fmul double %277, %279
  %281 = load double, double* %31, align 8
  %282 = fmul double 1.000000e+06, %281
  %283 = fdiv double %280, %282
  %284 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %262, double %263, double %273, double %283)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..14 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 3.600000e+02, double* %30, align 8
  store double 1.680000e+02, double* %29, align 8
  %285 = load double, double* %37, align 8
  %286 = load double, double* %29, align 8
  %287 = load double, double* %30, align 8
  %288 = fadd double %286, %287
  %289 = fdiv double %285, %288
  %290 = fadd double 1.000000e+00, %289
  %291 = fptosi double %290 to i32
  store i32 %291, i32* %34, align 4
  %292 = call double @dtime()
  store double %292, double* %31, align 8
  store i32 0, i32* %41, align 4
  br label %293

; <label>:293:                                    ; preds = %302, %253
  %294 = load i32, i32* %41, align 4
  %295 = load i32, i32* %34, align 4
  %296 = icmp slt i32 %294, %295
  br i1 %296, label %297, label %305

; <label>:297:                                    ; preds = %293
  %298 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %299 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %300 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %301 = load i32, i32* %36, align 4
  call void @QLA_F3_D_vpeq_spproj_M_times_pD([3 x [4 x %struct.QLA_F_Complex]]* %298, [3 x [3 x %struct.QLA_F_Complex]]* %299, [3 x [4 x %struct.QLA_F_Complex]]** %300, i32 0, i32 1, i32 %301)
  br label %302

; <label>:302:                                    ; preds = %297
  %303 = load i32, i32* %41, align 4
  %304 = add nsw i32 %303, 1
  store i32 %304, i32* %41, align 4
  br label %293

; <label>:305:                                    ; preds = %293
  %306 = call double @dtime()
  %307 = load double, double* %31, align 8
  %308 = fsub double %306, %307
  store double %308, double* %31, align 8
  %309 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %310 = load i32, i32* %36, align 4
  %311 = call float @sum_D([3 x [4 x %struct.QLA_F_Complex]]* %309, i32 %310)
  store float %311, float* %6, align 4
  %312 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.15, i32 0, i32 0))
  %313 = load float, float* %6, align 4
  %314 = fpext float %313 to double
  %315 = load double, double* %31, align 8
  %316 = load double, double* %30, align 8
  %317 = load i32, i32* %36, align 4
  %318 = sitofp i32 %317 to double
  %319 = fmul double %316, %318
  %320 = load i32, i32* %34, align 4
  %321 = sitofp i32 %320 to double
  %322 = fmul double %319, %321
  %323 = load double, double* %31, align 8
  %324 = fmul double 1.000000e+06, %323
  %325 = fdiv double %322, %324
  %326 = load double, double* %29, align 8
  %327 = load i32, i32* %36, align 4
  %328 = sitofp i32 %327 to double
  %329 = fmul double %326, %328
  %330 = load i32, i32* %34, align 4
  %331 = sitofp i32 %330 to double
  %332 = fmul double %329, %331
  %333 = load double, double* %31, align 8
  %334 = fmul double 1.000000e+06, %333
  %335 = fdiv double %332, %334
  %336 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %314, double %315, double %325, double %335)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..16 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 2.160000e+02, double* %30, align 8
  store double 1.980000e+02, double* %29, align 8
  %337 = load double, double* %37, align 8
  %338 = load double, double* %29, align 8
  %339 = load double, double* %30, align 8
  %340 = fadd double %338, %339
  %341 = fdiv double %337, %340
  %342 = fadd double 1.000000e+00, %341
  %343 = fptosi double %342 to i32
  store i32 %343, i32* %34, align 4
  %344 = call double @dtime()
  store double %344, double* %31, align 8
  store i32 0, i32* %42, align 4
  br label %345

; <label>:345:                                    ; preds = %354, %305
  %346 = load i32, i32* %42, align 4
  %347 = load i32, i32* %34, align 4
  %348 = icmp slt i32 %346, %347
  br i1 %348, label %349, label %357

; <label>:349:                                    ; preds = %345
  %350 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %351 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %352 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %353 = load i32, i32* %36, align 4
  call void @QLA_F3_M_veq_M_times_pM([3 x [3 x %struct.QLA_F_Complex]]* %350, [3 x [3 x %struct.QLA_F_Complex]]* %351, [3 x [3 x %struct.QLA_F_Complex]]** %352, i32 %353)
  br label %354

; <label>:354:                                    ; preds = %349
  %355 = load i32, i32* %42, align 4
  %356 = add nsw i32 %355, 1
  store i32 %356, i32* %42, align 4
  br label %345

; <label>:357:                                    ; preds = %345
  %358 = call double @dtime()
  %359 = load double, double* %31, align 8
  %360 = fsub double %358, %359
  store double %360, double* %31, align 8
  %361 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %362 = load i32, i32* %36, align 4
  %363 = call float @sum_M([3 x [3 x %struct.QLA_F_Complex]]* %361, i32 %362)
  store float %363, float* %6, align 4
  %364 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.17, i32 0, i32 0))
  %365 = load float, float* %6, align 4
  %366 = fpext float %365 to double
  %367 = load double, double* %31, align 8
  %368 = load double, double* %30, align 8
  %369 = load i32, i32* %36, align 4
  %370 = sitofp i32 %369 to double
  %371 = fmul double %368, %370
  %372 = load i32, i32* %34, align 4
  %373 = sitofp i32 %372 to double
  %374 = fmul double %371, %373
  %375 = load double, double* %31, align 8
  %376 = fmul double 1.000000e+06, %375
  %377 = fdiv double %374, %376
  %378 = load double, double* %29, align 8
  %379 = load i32, i32* %36, align 4
  %380 = sitofp i32 %379 to double
  %381 = fmul double %378, %380
  %382 = load i32, i32* %34, align 4
  %383 = sitofp i32 %382 to double
  %384 = fmul double %381, %383
  %385 = load double, double* %31, align 8
  %386 = fmul double 1.000000e+06, %385
  %387 = fdiv double %384, %386
  %388 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %366, double %367, double %377, double %387)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..18 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 2.400000e+01, double* %30, align 8
  store double 1.200000e+01, double* %29, align 8
  %389 = load double, double* %37, align 8
  %390 = load double, double* %29, align 8
  %391 = load double, double* %30, align 8
  %392 = fadd double %390, %391
  %393 = fdiv double %389, %392
  %394 = fadd double 1.000000e+00, %393
  %395 = fptosi double %394 to i32
  store i32 %395, i32* %34, align 4
  %396 = call double @dtime()
  store double %396, double* %31, align 8
  store i32 0, i32* %43, align 4
  br label %397

; <label>:397:                                    ; preds = %405, %357
  %398 = load i32, i32* %43, align 4
  %399 = load i32, i32* %34, align 4
  %400 = icmp slt i32 %398, %399
  br i1 %400, label %401, label %408

; <label>:401:                                    ; preds = %397
  %402 = load float*, float** %7, align 8
  %403 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %404 = load i32, i32* %36, align 4
  call void @QLA_F3_r_veq_norm2_V(float* %402, [3 x %struct.QLA_F_Complex]* %403, i32 %404)
  br label %405

; <label>:405:                                    ; preds = %401
  %406 = load i32, i32* %43, align 4
  %407 = add nsw i32 %406, 1
  store i32 %407, i32* %43, align 4
  br label %397

; <label>:408:                                    ; preds = %397
  %409 = call double @dtime()
  %410 = load double, double* %31, align 8
  %411 = fsub double %409, %410
  store double %411, double* %31, align 8
  %412 = load float*, float** %7, align 8
  %413 = load float, float* %412, align 4
  store float %413, float* %6, align 4
  %414 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.19, i32 0, i32 0))
  %415 = load float, float* %6, align 4
  %416 = fpext float %415 to double
  %417 = load double, double* %31, align 8
  %418 = load double, double* %30, align 8
  %419 = load i32, i32* %36, align 4
  %420 = sitofp i32 %419 to double
  %421 = fmul double %418, %420
  %422 = load i32, i32* %34, align 4
  %423 = sitofp i32 %422 to double
  %424 = fmul double %421, %423
  %425 = load double, double* %31, align 8
  %426 = fmul double 1.000000e+06, %425
  %427 = fdiv double %424, %426
  %428 = load double, double* %29, align 8
  %429 = load i32, i32* %36, align 4
  %430 = sitofp i32 %429 to double
  %431 = fmul double %428, %430
  %432 = load i32, i32* %34, align 4
  %433 = sitofp i32 %432 to double
  %434 = fmul double %431, %433
  %435 = load double, double* %31, align 8
  %436 = fmul double 1.000000e+06, %435
  %437 = fdiv double %434, %436
  %438 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %416, double %417, double %427, double %437)
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 13, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, float**, %struct.QLA_F_Complex**, [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]**, [3 x %struct.QLA_F_Complex]***, [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]***)* @.omp_outlined..20 to void (i32*, i32*, ...)*), i32* %36, float** %7, %struct.QLA_F_Complex** %8, [3 x %struct.QLA_F_Complex]** %9, [3 x %struct.QLA_F_Complex]** %10, [3 x [4 x %struct.QLA_F_Complex]]** %21, [3 x [4 x %struct.QLA_F_Complex]]** %22, [3 x [3 x %struct.QLA_F_Complex]]** %24, [3 x [3 x %struct.QLA_F_Complex]]** %25, [3 x [3 x %struct.QLA_F_Complex]]** %26, [3 x %struct.QLA_F_Complex]*** %14, [3 x [4 x %struct.QLA_F_Complex]]*** %23, [3 x [3 x %struct.QLA_F_Complex]]*** %28)
  store double 4.800000e+01, double* %30, align 8
  store double 2.400000e+01, double* %29, align 8
  %439 = load double, double* %37, align 8
  %440 = load double, double* %29, align 8
  %441 = load double, double* %30, align 8
  %442 = fadd double %440, %441
  %443 = fdiv double %439, %442
  %444 = fadd double 1.000000e+00, %443
  %445 = fptosi double %444 to i32
  store i32 %445, i32* %34, align 4
  %446 = call double @dtime()
  store double %446, double* %31, align 8
  store i32 0, i32* %44, align 4
  br label %447

; <label>:447:                                    ; preds = %456, %408
  %448 = load i32, i32* %44, align 4
  %449 = load i32, i32* %34, align 4
  %450 = icmp slt i32 %448, %449
  br i1 %450, label %451, label %459

; <label>:451:                                    ; preds = %447
  %452 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %453 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %454 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %455 = load i32, i32* %36, align 4
  call void @QLA_F3_c_veq_V_dot_V(%struct.QLA_F_Complex* %452, [3 x %struct.QLA_F_Complex]* %453, [3 x %struct.QLA_F_Complex]* %454, i32 %455)
  br label %456

; <label>:456:                                    ; preds = %451
  %457 = load i32, i32* %44, align 4
  %458 = add nsw i32 %457, 1
  store i32 %458, i32* %44, align 4
  br label %447

; <label>:459:                                    ; preds = %447
  %460 = call double @dtime()
  %461 = load double, double* %31, align 8
  %462 = fsub double %460, %461
  store double %462, double* %31, align 8
  %463 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %464 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %463, i32 0, i32 0
  %465 = load float, float* %464, align 8
  %466 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %467 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %466, i32 0, i32 0
  %468 = load float, float* %467, align 8
  %469 = fmul float %465, %468
  %470 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %471 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %470, i32 0, i32 1
  %472 = load float, float* %471, align 4
  %473 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %474 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %473, i32 0, i32 1
  %475 = load float, float* %474, align 4
  %476 = fmul float %472, %475
  %477 = fadd float %469, %476
  store float %477, float* %6, align 4
  %478 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.21, i32 0, i32 0))
  %479 = load float, float* %6, align 4
  %480 = fpext float %479 to double
  %481 = load double, double* %31, align 8
  %482 = load double, double* %30, align 8
  %483 = load i32, i32* %36, align 4
  %484 = sitofp i32 %483 to double
  %485 = fmul double %482, %484
  %486 = load i32, i32* %34, align 4
  %487 = sitofp i32 %486 to double
  %488 = fmul double %485, %487
  %489 = load double, double* %31, align 8
  %490 = fmul double 1.000000e+06, %489
  %491 = fdiv double %488, %490
  %492 = load double, double* %29, align 8
  %493 = load i32, i32* %36, align 4
  %494 = sitofp i32 %493 to double
  %495 = fmul double %492, %494
  %496 = load i32, i32* %34, align 4
  %497 = sitofp i32 %496 to double
  %498 = fmul double %495, %497
  %499 = load double, double* %31, align 8
  %500 = fmul double 1.000000e+06, %499
  %501 = fdiv double %498, %500
  %502 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.9, i32 0, i32 0), double %480, double %481, double %491, double %501)
  br label %503

; <label>:503:                                    ; preds = %459
  %504 = load i32, i32* %36, align 4
  %505 = mul nsw i32 %504, 2
  store i32 %505, i32* %36, align 4
  br label %116

; <label>:506:                                    ; preds = %116
  ret i32 0
}

declare i32 @printf(i8*, ...) #2

; Function Attrs: nounwind
declare i32 @omp_get_max_threads() #1

; Function Attrs: nounwind
declare double @omp_get_wtick() #1

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined.(i32* noalias, i32* noalias) #0 {
  %3 = alloca i32*, align 8
  %4 = alloca i32*, align 8
  %5 = alloca i32, align 4
  %6 = alloca %struct.cpu_set_t, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64, align 8
  store i32* %0, i32** %3, align 8
  store i32* %1, i32** %4, align 8
  %9 = call i32 @omp_get_thread_num() #4
  store i32 %9, i32* %5, align 4
  br label %10

; <label>:10:                                     ; preds = %2
  %11 = bitcast %struct.cpu_set_t* %6 to i8*
  call void @llvm.memset.p0i8.i64(i8* %11, i8 0, i64 128, i32 8, i1 false)
  br label %12

; <label>:12:                                     ; preds = %10
  br label %13

; <label>:13:                                     ; preds = %12
  %14 = load i32, i32* %5, align 4
  %15 = sext i32 %14 to i64
  store i64 %15, i64* %7, align 8
  %16 = load i64, i64* %7, align 8
  %17 = udiv i64 %16, 8
  %18 = icmp ult i64 %17, 128
  br i1 %18, label %19, label %30

; <label>:19:                                     ; preds = %13
  %20 = load i64, i64* %7, align 8
  %21 = urem i64 %20, 64
  %22 = shl i64 1, %21
  %23 = load i64, i64* %7, align 8
  %24 = udiv i64 %23, 64
  %25 = getelementptr inbounds %struct.cpu_set_t, %struct.cpu_set_t* %6, i32 0, i32 0
  %26 = getelementptr inbounds [16 x i64], [16 x i64]* %25, i32 0, i32 0
  %27 = getelementptr inbounds i64, i64* %26, i64 %24
  %28 = load i64, i64* %27, align 8
  %29 = or i64 %28, %22
  store i64 %29, i64* %27, align 8
  br label %31

; <label>:30:                                     ; preds = %13
  br label %31

; <label>:31:                                     ; preds = %30, %19
  %32 = phi i64 [ %29, %19 ], [ 0, %30 ]
  store i64 %32, i64* %8, align 8
  %33 = load i64, i64* %8, align 8
  %34 = call i32 @sched_setaffinity(i32 0, i64 128, %struct.cpu_set_t* %6) #4
  ret void
}

; Function Attrs: nounwind
declare i32 @omp_get_thread_num() #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #3

; Function Attrs: nounwind
declare i32 @sched_setaffinity(i32, i64, %struct.cpu_set_t*) #1

declare void @__kmpc_fork_call(%ident_t*, i32, void (i32*, i32*, ...)*, ...)

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..6(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @__kmpc_for_static_init_4(%ident_t*, i32, i32, i32*, i32*, i32*, i32*, i32, i32)

declare void @__kmpc_for_static_fini(%ident_t*, i32)

declare void @QLA_F3_V_vpeq_M_times_pV([3 x %struct.QLA_F_Complex]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x %struct.QLA_F_Complex]**, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..10(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_V_veq_Ma_times_V([3 x %struct.QLA_F_Complex]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x %struct.QLA_F_Complex]*, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..12(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_V_vmeq_pV([3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]**, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..14(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_D_vpeq_spproj_M_times_pD([3 x [4 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]**, i32, i32, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..16(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_M_veq_M_times_pM([3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]**, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..18(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_r_veq_norm2_V(float*, [3 x %struct.QLA_F_Complex]*, i32) #2

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..20(i32* noalias, i32* noalias, i32* dereferenceable(4), float** dereferenceable(8), %struct.QLA_F_Complex** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x %struct.QLA_F_Complex]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]** dereferenceable(8), [3 x %struct.QLA_F_Complex]*** dereferenceable(8), [3 x [4 x %struct.QLA_F_Complex]]*** dereferenceable(8), [3 x [3 x %struct.QLA_F_Complex]]*** dereferenceable(8)) #0 {
  %16 = alloca i32*, align 8
  %17 = alloca i32*, align 8
  %18 = alloca i32*, align 8
  %19 = alloca float**, align 8
  %20 = alloca %struct.QLA_F_Complex**, align 8
  %21 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %22 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %28 = alloca [3 x %struct.QLA_F_Complex]***, align 8
  %29 = alloca [3 x [4 x %struct.QLA_F_Complex]]***, align 8
  %30 = alloca [3 x [3 x %struct.QLA_F_Complex]]***, align 8
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  store i32* %0, i32** %16, align 8
  store i32* %1, i32** %17, align 8
  store i32* %2, i32** %18, align 8
  store float** %3, float*** %19, align 8
  store %struct.QLA_F_Complex** %4, %struct.QLA_F_Complex*** %20, align 8
  store [3 x %struct.QLA_F_Complex]** %5, [3 x %struct.QLA_F_Complex]*** %21, align 8
  store [3 x %struct.QLA_F_Complex]** %6, [3 x %struct.QLA_F_Complex]*** %22, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %7, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]** %8, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %9, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %10, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]** %11, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  store [3 x %struct.QLA_F_Complex]*** %12, [3 x %struct.QLA_F_Complex]**** %28, align 8
  store [3 x [4 x %struct.QLA_F_Complex]]*** %13, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  store [3 x [3 x %struct.QLA_F_Complex]]*** %14, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %40 = load i32*, i32** %18, align 8
  %41 = load float**, float*** %19, align 8
  %42 = load %struct.QLA_F_Complex**, %struct.QLA_F_Complex*** %20, align 8
  %43 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %21, align 8
  %44 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %22, align 8
  %45 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %46 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %24, align 8
  %47 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %25, align 8
  %48 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %26, align 8
  %49 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %27, align 8
  %50 = load [3 x %struct.QLA_F_Complex]***, [3 x %struct.QLA_F_Complex]**** %28, align 8
  %51 = load [3 x [4 x %struct.QLA_F_Complex]]***, [3 x [4 x %struct.QLA_F_Complex]]**** %29, align 8
  %52 = load [3 x [3 x %struct.QLA_F_Complex]]***, [3 x [3 x %struct.QLA_F_Complex]]**** %30, align 8
  %53 = load i32, i32* %40, align 4
  store i32 %53, i32* %32, align 4
  %54 = load i32, i32* %32, align 4
  %55 = sub nsw i32 %54, 0
  %56 = sub nsw i32 %55, 1
  %57 = add nsw i32 %56, 1
  %58 = sdiv i32 %57, 1
  %59 = sub nsw i32 %58, 1
  store i32 %59, i32* %33, align 4
  store i32 0, i32* %34, align 4
  %60 = load i32, i32* %32, align 4
  %61 = icmp slt i32 0, %60
  br i1 %61, label %62, label %166

; <label>:62:                                     ; preds = %15
  store i32 0, i32* %35, align 4
  %63 = load i32, i32* %33, align 4
  store i32 %63, i32* %36, align 4
  store i32 1, i32* %37, align 4
  store i32 0, i32* %38, align 4
  %64 = load i32*, i32** %16, align 8
  %65 = load i32, i32* %64, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %65, i32 34, i32* %38, i32* %35, i32* %36, i32* %37, i32 1, i32 1)
  %66 = load i32, i32* %36, align 4
  %67 = load i32, i32* %33, align 4
  %68 = icmp sgt i32 %66, %67
  br i1 %68, label %69, label %71

; <label>:69:                                     ; preds = %62
  %70 = load i32, i32* %33, align 4
  br label %73

; <label>:71:                                     ; preds = %62
  %72 = load i32, i32* %36, align 4
  br label %73

; <label>:73:                                     ; preds = %71, %69
  %74 = phi i32 [ %70, %69 ], [ %72, %71 ]
  store i32 %74, i32* %36, align 4
  %75 = load i32, i32* %35, align 4
  store i32 %75, i32* %31, align 4
  br label %76

; <label>:76:                                     ; preds = %159, %73
  %77 = load i32, i32* %31, align 4
  %78 = load i32, i32* %36, align 4
  %79 = icmp sle i32 %77, %78
  br i1 %79, label %80, label %162

; <label>:80:                                     ; preds = %76
  %81 = load i32, i32* %31, align 4
  %82 = mul nsw i32 %81, 1
  %83 = add nsw i32 0, %82
  store i32 %83, i32* %34, align 4
  %84 = load i32, i32* %34, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %41, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load i32, i32* %34, align 4
  call void @set_R(float* %87, i32 %88)
  %89 = load i32, i32* %34, align 4
  %90 = sext i32 %89 to i64
  %91 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %42, align 8
  %92 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %91, i64 %90
  %93 = load i32, i32* %34, align 4
  call void @set_C(%struct.QLA_F_Complex* %92, i32 %93)
  %94 = load i32, i32* %34, align 4
  %95 = sext i32 %94 to i64
  %96 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %43, align 8
  %97 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %96, i64 %95
  %98 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %97, i32 %98)
  %99 = load i32, i32* %34, align 4
  %100 = sext i32 %99 to i64
  %101 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %102 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %101, i64 %100
  %103 = load i32, i32* %34, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %102, i32 %103)
  %104 = load i32, i32* %34, align 4
  %105 = sext i32 %104 to i64
  %106 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %45, align 8
  %107 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %106, i64 %105
  %108 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %107, i32 %108)
  %109 = load i32, i32* %34, align 4
  %110 = sext i32 %109 to i64
  %111 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %112 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %111, i64 %110
  %113 = load i32, i32* %34, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %112, i32 %113)
  %114 = load i32, i32* %34, align 4
  %115 = sext i32 %114 to i64
  %116 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %47, align 8
  %117 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %116, i64 %115
  %118 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %117, i32 %118)
  %119 = load i32, i32* %34, align 4
  %120 = sext i32 %119 to i64
  %121 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %48, align 8
  %122 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %121, i64 %120
  %123 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %122, i32 %123)
  %124 = load i32, i32* %34, align 4
  %125 = sext i32 %124 to i64
  %126 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %127 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %126, i64 %125
  %128 = load i32, i32* %34, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %127, i32 %128)
  %129 = load i32, i32* %34, align 4
  %130 = or i32 %129, 16
  %131 = add nsw i32 %130, 256
  %132 = load i32, i32* %40, align 4
  %133 = srem i32 %131, %132
  store i32 %133, i32* %39, align 4
  %134 = load i32, i32* %39, align 4
  %135 = sext i32 %134 to i64
  %136 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %44, align 8
  %137 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %136, i64 %135
  %138 = load i32, i32* %34, align 4
  %139 = sext i32 %138 to i64
  %140 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %50, align 8
  %141 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %140, i64 %139
  store [3 x %struct.QLA_F_Complex]* %137, [3 x %struct.QLA_F_Complex]** %141, align 8
  %142 = load i32, i32* %39, align 4
  %143 = sext i32 %142 to i64
  %144 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %46, align 8
  %145 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %144, i64 %143
  %146 = load i32, i32* %34, align 4
  %147 = sext i32 %146 to i64
  %148 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %51, align 8
  %149 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %148, i64 %147
  store [3 x [4 x %struct.QLA_F_Complex]]* %145, [3 x [4 x %struct.QLA_F_Complex]]** %149, align 8
  %150 = load i32, i32* %39, align 4
  %151 = sext i32 %150 to i64
  %152 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %49, align 8
  %153 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %152, i64 %151
  %154 = load i32, i32* %34, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %52, align 8
  %157 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %156, i64 %155
  store [3 x [3 x %struct.QLA_F_Complex]]* %153, [3 x [3 x %struct.QLA_F_Complex]]** %157, align 8
  br label %158

; <label>:158:                                    ; preds = %80
  br label %159

; <label>:159:                                    ; preds = %158
  %160 = load i32, i32* %31, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %31, align 4
  br label %76

; <label>:162:                                    ; preds = %76
  br label %163

; <label>:163:                                    ; preds = %162
  %164 = load i32*, i32** %16, align 8
  %165 = load i32, i32* %164, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %165)
  br label %166

; <label>:166:                                    ; preds = %163, %15
  ret void
}

declare void @QLA_F3_c_veq_V_dot_V(%struct.QLA_F_Complex*, [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]*, i32) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind }
attributes #4 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
