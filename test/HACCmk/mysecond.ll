; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/mysecond.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/mysecond.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.timeval = type { i64, i64 }
%struct.timezone = type { i32, i32 }

; Function Attrs: nounwind uwtable
define double @mysecond() #0 {
  %1 = alloca %struct.timeval, align 8
  %2 = alloca %struct.timezone, align 4
  %3 = alloca i32, align 4
  %4 = call i32 @gettimeofday(%struct.timeval* %1, %struct.timezone* %2) #2
  store i32 %4, i32* %3, align 4
  %5 = getelementptr inbounds %struct.timeval, %struct.timeval* %1, i32 0, i32 0
  %6 = load i64, i64* %5, align 8
  %7 = sitofp i64 %6 to double
  %8 = getelementptr inbounds %struct.timeval, %struct.timeval* %1, i32 0, i32 1
  %9 = load i64, i64* %8, align 8
  %10 = sitofp i64 %9 to double
  %11 = fmul double %10, 1.000000e-06
  %12 = fadd double %7, %11
  ret double %12
}

; Function Attrs: nounwind
declare i32 @gettimeofday(%struct.timeval*, %struct.timezone*) #1

; Function Attrs: nounwind uwtable
define double @mysecond_() #0 {
  %1 = call double @mysecond()
  ret double %1
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
