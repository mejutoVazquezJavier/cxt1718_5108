; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/main.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%ident_t = type { i32, i32, i32, i32, i8* }

@main.xx = internal global [15000 x float] zeroinitializer, align 16
@main.yy = internal global [15000 x float] zeroinitializer, align 16
@main.zz = internal global [15000 x float] zeroinitializer, align 16
@main.mass = internal global [15000 x float] zeroinitializer, align 16
@main.vx1 = internal global [15000 x float] zeroinitializer, align 16
@main.vy1 = internal global [15000 x float] zeroinitializer, align 16
@main.vz1 = internal global [15000 x float] zeroinitializer, align 16
@.str = private unnamed_addr constant [17 x i8] c"count is set %d\0A\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"Total MPI ranks %d\0A\00", align 1
@.str.2 = private unnamed_addr constant [27 x i8] c"Number of OMP threads %d\0A\0A\00", align 1
@.str.3 = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) }, align 8
@.str.5 = private unnamed_addr constant [34 x i8] c"\0AKernel elapsed time, s: %18.8lf\0A\00", align 1
@.str.6 = private unnamed_addr constant [33 x i8] c"Total  elapsed time, s: %18.8lf\0A\00", align 1
@.str.7 = private unnamed_addr constant [28 x i8] c"Result validation: %18.8lf\0A\00", align 1
@.str.8 = private unnamed_addr constant [40 x i8] c"Result expected  : 6636045675.12190628\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca float, align 4
  %7 = alloca float, align 4
  %8 = alloca float, align 4
  %9 = alloca float, align 4
  %10 = alloca float, align 4
  %11 = alloca float, align 4
  %12 = alloca [16777216 x i8], align 16
  %13 = alloca [16777216 x i8], align 16
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i64, align 8
  %20 = alloca i64, align 8
  %21 = alloca i64, align 8
  %22 = alloca i64, align 8
  %23 = alloca i64, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i64 0, i64* %23, align 8
  store double 0.000000e+00, double* %25, align 8
  store i32 0, i32* %17, align 4
  store i32 1, i32* %18, align 4
  store i32 327, i32* %15, align 4
  %28 = load i32, i32* %17, align 4
  %29 = icmp eq i32 %28, 0
  br i1 %29, label %30, label %35

; <label>:30:                                     ; preds = %2
  %31 = load i32, i32* %15, align 4
  %32 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0), i32 %31)
  %33 = load i32, i32* %18, align 4
  %34 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0), i32 %33)
  br label %35

; <label>:35:                                     ; preds = %30, %2
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 1, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*)* @.omp_outlined. to void (i32*, i32*, ...)*), i32* %17)
  %36 = call i64 (...) @timebase()
  store i64 %36, i64* %21, align 8
  store double 0.000000e+00, double* %27, align 8
  store i32 400, i32* %14, align 4
  br label %37

; <label>:37:                                     ; preds = %184, %35
  %38 = load i32, i32* %14, align 4
  %39 = icmp slt i32 %38, 15000
  br i1 %39, label %40, label %187

; <label>:40:                                     ; preds = %37
  store float 0x3FCD70A3E0000000, float* %8, align 4
  store float 5.000000e-01, float* %6, align 4
  store float 0x3F9EB851E0000000, float* %7, align 4
  %41 = load i32, i32* %14, align 4
  %42 = sitofp i32 %41 to float
  %43 = fdiv float 1.000000e+00, %42
  store float %43, float* %9, align 4
  %44 = load i32, i32* %14, align 4
  %45 = sitofp i32 %44 to float
  %46 = fdiv float 2.000000e+00, %45
  store float %46, float* %10, align 4
  %47 = load i32, i32* %14, align 4
  %48 = sitofp i32 %47 to float
  %49 = fdiv float 3.000000e+00, %48
  store float %49, float* %11, align 4
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.xx, i64 0, i64 0), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.yy, i64 0, i64 0), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.zz, i64 0, i64 0), align 16
  store float 2.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.mass, i64 0, i64 0), align 16
  store i32 1, i32* %16, align 4
  br label %50

; <label>:50:                                     ; preds = %96, %40
  %51 = load i32, i32* %16, align 4
  %52 = load i32, i32* %14, align 4
  %53 = icmp slt i32 %51, %52
  br i1 %53, label %54, label %99

; <label>:54:                                     ; preds = %50
  %55 = load i32, i32* %16, align 4
  %56 = sub nsw i32 %55, 1
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %57
  %59 = load float, float* %58, align 4
  %60 = load float, float* %9, align 4
  %61 = fadd float %59, %60
  %62 = load i32, i32* %16, align 4
  %63 = sext i32 %62 to i64
  %64 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %63
  store float %61, float* %64, align 4
  %65 = load i32, i32* %16, align 4
  %66 = sub nsw i32 %65, 1
  %67 = sext i32 %66 to i64
  %68 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %67
  %69 = load float, float* %68, align 4
  %70 = load float, float* %10, align 4
  %71 = fadd float %69, %70
  %72 = load i32, i32* %16, align 4
  %73 = sext i32 %72 to i64
  %74 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %73
  store float %71, float* %74, align 4
  %75 = load i32, i32* %16, align 4
  %76 = sub nsw i32 %75, 1
  %77 = sext i32 %76 to i64
  %78 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %77
  %79 = load float, float* %78, align 4
  %80 = load float, float* %11, align 4
  %81 = fadd float %79, %80
  %82 = load i32, i32* %16, align 4
  %83 = sext i32 %82 to i64
  %84 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %83
  store float %81, float* %84, align 4
  %85 = load i32, i32* %16, align 4
  %86 = sitofp i32 %85 to float
  %87 = fmul float %86, 0x3F847AE140000000
  %88 = load i32, i32* %16, align 4
  %89 = sext i32 %88 to i64
  %90 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %89
  %91 = load float, float* %90, align 4
  %92 = fadd float %87, %91
  %93 = load i32, i32* %16, align 4
  %94 = sext i32 %93 to i64
  %95 = getelementptr inbounds [15000 x float], [15000 x float]* @main.mass, i64 0, i64 %94
  store float %92, float* %95, align 4
  br label %96

; <label>:96:                                     ; preds = %54
  %97 = load i32, i32* %16, align 4
  %98 = add nsw i32 %97, 1
  store i32 %98, i32* %16, align 4
  br label %50

; <label>:99:                                     ; preds = %50
  store i32 0, i32* %16, align 4
  br label %100

; <label>:100:                                    ; preds = %114, %99
  %101 = load i32, i32* %16, align 4
  %102 = load i32, i32* %14, align 4
  %103 = icmp slt i32 %101, %102
  br i1 %103, label %104, label %117

; <label>:104:                                    ; preds = %100
  %105 = load i32, i32* %16, align 4
  %106 = sext i32 %105 to i64
  %107 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %106
  store float 0.000000e+00, float* %107, align 4
  %108 = load i32, i32* %16, align 4
  %109 = sext i32 %108 to i64
  %110 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %109
  store float 0.000000e+00, float* %110, align 4
  %111 = load i32, i32* %16, align 4
  %112 = sext i32 %111 to i64
  %113 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %112
  store float 0.000000e+00, float* %113, align 4
  br label %114

; <label>:114:                                    ; preds = %104
  %115 = load i32, i32* %16, align 4
  %116 = add nsw i32 %115, 1
  store i32 %116, i32* %16, align 4
  br label %100

; <label>:117:                                    ; preds = %100
  store i32 0, i32* %16, align 4
  br label %118

; <label>:118:                                    ; preds = %125, %117
  %119 = load i32, i32* %16, align 4
  %120 = icmp slt i32 %119, 16777216
  br i1 %120, label %121, label %128

; <label>:121:                                    ; preds = %118
  %122 = load i32, i32* %16, align 4
  %123 = sext i32 %122 to i64
  %124 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %12, i64 0, i64 %123
  store i8 4, i8* %124, align 1
  br label %125

; <label>:125:                                    ; preds = %121
  %126 = load i32, i32* %16, align 4
  %127 = add nsw i32 %126, 1
  store i32 %127, i32* %16, align 4
  br label %118

; <label>:128:                                    ; preds = %118
  store i32 0, i32* %16, align 4
  br label %129

; <label>:129:                                    ; preds = %140, %128
  %130 = load i32, i32* %16, align 4
  %131 = icmp slt i32 %130, 16777216
  br i1 %131, label %132, label %143

; <label>:132:                                    ; preds = %129
  %133 = load i32, i32* %16, align 4
  %134 = sext i32 %133 to i64
  %135 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %12, i64 0, i64 %134
  %136 = load i8, i8* %135, align 1
  %137 = load i32, i32* %16, align 4
  %138 = sext i32 %137 to i64
  %139 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %13, i64 0, i64 %138
  store i8 %136, i8* %139, align 1
  br label %140

; <label>:140:                                    ; preds = %132
  %141 = load i32, i32* %16, align 4
  %142 = add nsw i32 %141, 1
  store i32 %142, i32* %16, align 4
  br label %129

; <label>:143:                                    ; preds = %129
  %144 = call i64 (...) @timebase()
  store i64 %144, i64* %19, align 8
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 6, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, i32*, i32*, float*, float*, float*)* @.omp_outlined..4 to void (i32*, i32*, ...)*), i32* %16, i32* %15, i32* %14, float* %6, float* %7, float* %8)
  %145 = call i64 (...) @timebase()
  store i64 %145, i64* %20, align 8
  store double 0.000000e+00, double* %26, align 8
  store i32 0, i32* %16, align 4
  br label %146

; <label>:146:                                    ; preds = %168, %143
  %147 = load i32, i32* %16, align 4
  %148 = load i32, i32* %14, align 4
  %149 = icmp slt i32 %147, %148
  br i1 %149, label %150, label %171

; <label>:150:                                    ; preds = %146
  %151 = load double, double* %26, align 8
  %152 = load i32, i32* %16, align 4
  %153 = sext i32 %152 to i64
  %154 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %153
  %155 = load float, float* %154, align 4
  %156 = load i32, i32* %16, align 4
  %157 = sext i32 %156 to i64
  %158 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %157
  %159 = load float, float* %158, align 4
  %160 = fadd float %155, %159
  %161 = load i32, i32* %16, align 4
  %162 = sext i32 %161 to i64
  %163 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %162
  %164 = load float, float* %163, align 4
  %165 = fadd float %160, %164
  %166 = fpext float %165 to double
  %167 = fadd double %151, %166
  store double %167, double* %26, align 8
  br label %168

; <label>:168:                                    ; preds = %150
  %169 = load i32, i32* %16, align 4
  %170 = add nsw i32 %169, 1
  store i32 %170, i32* %16, align 4
  br label %146

; <label>:171:                                    ; preds = %146
  %172 = load double, double* %27, align 8
  %173 = load double, double* %26, align 8
  %174 = fadd double %172, %173
  store double %174, double* %27, align 8
  %175 = load i64, i64* %20, align 8
  %176 = load i64, i64* %19, align 8
  %177 = sub i64 %175, %176
  %178 = uitofp i64 %177 to double
  %179 = fmul double 1.000000e+06, %178
  %180 = fdiv double %179, 1.600000e+09
  store double %180, double* %24, align 8
  %181 = load double, double* %25, align 8
  %182 = load double, double* %24, align 8
  %183 = fadd double %181, %182
  store double %183, double* %25, align 8
  br label %184

; <label>:184:                                    ; preds = %171
  %185 = load i32, i32* %14, align 4
  %186 = add nsw i32 %185, 20
  store i32 %186, i32* %14, align 4
  br label %37

; <label>:187:                                    ; preds = %37
  %188 = call i64 (...) @timebase()
  store i64 %188, i64* %22, align 8
  %189 = load i32, i32* %17, align 4
  %190 = icmp eq i32 %189, 0
  br i1 %190, label %191, label %204

; <label>:191:                                    ; preds = %187
  %192 = load double, double* %25, align 8
  %193 = fmul double %192, 1.000000e-06
  %194 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.5, i32 0, i32 0), double %193)
  %195 = load i64, i64* %22, align 8
  %196 = load i64, i64* %21, align 8
  %197 = sub i64 %195, %196
  %198 = uitofp i64 %197 to double
  %199 = fdiv double %198, 1.600000e+09
  %200 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.6, i32 0, i32 0), double %199)
  %201 = load double, double* %27, align 8
  %202 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.7, i32 0, i32 0), double %201)
  %203 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.8, i32 0, i32 0))
  br label %204

; <label>:204:                                    ; preds = %191, %187
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined.(i32* noalias, i32* noalias, i32* dereferenceable(4)) #0 {
  %4 = alloca i32*, align 8
  %5 = alloca i32*, align 8
  %6 = alloca i32*, align 8
  store i32* %0, i32** %4, align 8
  store i32* %1, i32** %5, align 8
  store i32* %2, i32** %6, align 8
  %7 = load i32*, i32** %6, align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %10, label %16

; <label>:10:                                     ; preds = %3
  %11 = call i32 @omp_get_thread_num() #3
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %13, label %16

; <label>:13:                                     ; preds = %10
  %14 = call i32 @omp_get_num_threads() #3
  %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0), i32 %14)
  br label %16

; <label>:16:                                     ; preds = %13, %10, %3
  ret void
}

; Function Attrs: nounwind
declare i32 @omp_get_thread_num() #2

; Function Attrs: nounwind
declare i32 @omp_get_num_threads() #2

declare void @__kmpc_fork_call(%ident_t*, i32, void (i32*, i32*, ...)*, ...)

declare i64 @timebase(...) #1

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined..4(i32* noalias, i32* noalias, i32* dereferenceable(4), i32* dereferenceable(4), i32* dereferenceable(4), float* dereferenceable(4), float* dereferenceable(4), float* dereferenceable(4)) #0 {
  %9 = alloca i32*, align 8
  %10 = alloca i32*, align 8
  %11 = alloca i32*, align 8
  %12 = alloca i32*, align 8
  %13 = alloca i32*, align 8
  %14 = alloca float*, align 8
  %15 = alloca float*, align 8
  %16 = alloca float*, align 8
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca float, align 4
  %26 = alloca float, align 4
  %27 = alloca float, align 4
  store i32* %0, i32** %9, align 8
  store i32* %1, i32** %10, align 8
  store i32* %2, i32** %11, align 8
  store i32* %3, i32** %12, align 8
  store i32* %4, i32** %13, align 8
  store float* %5, float** %14, align 8
  store float* %6, float** %15, align 8
  store float* %7, float** %16, align 8
  %28 = load i32*, i32** %11, align 8
  %29 = load i32*, i32** %12, align 8
  %30 = load i32*, i32** %13, align 8
  %31 = load float*, float** %14, align 8
  %32 = load float*, float** %15, align 8
  %33 = load float*, float** %16, align 8
  %34 = load i32, i32* %29, align 4
  store i32 %34, i32* %18, align 4
  %35 = load i32, i32* %18, align 4
  %36 = sub nsw i32 %35, 0
  %37 = sub nsw i32 %36, 1
  %38 = add nsw i32 %37, 1
  %39 = sdiv i32 %38, 1
  %40 = sub nsw i32 %39, 1
  store i32 %40, i32* %19, align 4
  store i32 0, i32* %20, align 4
  %41 = load i32, i32* %18, align 4
  %42 = icmp slt i32 0, %41
  br i1 %42, label %43, label %121

; <label>:43:                                     ; preds = %8
  store i32 0, i32* %21, align 4
  %44 = load i32, i32* %19, align 4
  store i32 %44, i32* %22, align 4
  store i32 1, i32* %23, align 4
  store i32 0, i32* %24, align 4
  %45 = load i32*, i32** %9, align 8
  %46 = load i32, i32* %45, align 4
  call void @__kmpc_for_static_init_4(%ident_t* @0, i32 %46, i32 34, i32* %24, i32* %21, i32* %22, i32* %23, i32 1, i32 1)
  %47 = load i32, i32* %22, align 4
  %48 = load i32, i32* %19, align 4
  %49 = icmp sgt i32 %47, %48
  br i1 %49, label %50, label %52

; <label>:50:                                     ; preds = %43
  %51 = load i32, i32* %19, align 4
  br label %54

; <label>:52:                                     ; preds = %43
  %53 = load i32, i32* %22, align 4
  br label %54

; <label>:54:                                     ; preds = %52, %50
  %55 = phi i32 [ %51, %50 ], [ %53, %52 ]
  store i32 %55, i32* %22, align 4
  %56 = load i32, i32* %21, align 4
  store i32 %56, i32* %17, align 4
  br label %57

; <label>:57:                                     ; preds = %114, %54
  %58 = load i32, i32* %17, align 4
  %59 = load i32, i32* %22, align 4
  %60 = icmp sle i32 %58, %59
  br i1 %60, label %61, label %117

; <label>:61:                                     ; preds = %57
  %62 = load i32, i32* %17, align 4
  %63 = mul nsw i32 %62, 1
  %64 = add nsw i32 0, %63
  store i32 %64, i32* %20, align 4
  %65 = load i32, i32* %30, align 4
  %66 = load i32, i32* %20, align 4
  %67 = sext i32 %66 to i64
  %68 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %67
  %69 = load float, float* %68, align 4
  %70 = load i32, i32* %20, align 4
  %71 = sext i32 %70 to i64
  %72 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %71
  %73 = load float, float* %72, align 4
  %74 = load i32, i32* %20, align 4
  %75 = sext i32 %74 to i64
  %76 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %75
  %77 = load float, float* %76, align 4
  %78 = load float, float* %31, align 4
  %79 = load float, float* %32, align 4
  call void @Step10_orig(i32 %65, float %69, float %73, float %77, float %78, float %79, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.xx, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.yy, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.zz, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.mass, i32 0, i32 0), float* %25, float* %26, float* %27)
  %80 = load i32, i32* %20, align 4
  %81 = sext i32 %80 to i64
  %82 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %81
  %83 = load float, float* %82, align 4
  %84 = load float, float* %25, align 4
  %85 = load float, float* %33, align 4
  %86 = fmul float %84, %85
  %87 = fadd float %83, %86
  %88 = load i32, i32* %20, align 4
  %89 = sext i32 %88 to i64
  %90 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %89
  store float %87, float* %90, align 4
  %91 = load i32, i32* %20, align 4
  %92 = sext i32 %91 to i64
  %93 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %92
  %94 = load float, float* %93, align 4
  %95 = load float, float* %26, align 4
  %96 = load float, float* %33, align 4
  %97 = fmul float %95, %96
  %98 = fadd float %94, %97
  %99 = load i32, i32* %20, align 4
  %100 = sext i32 %99 to i64
  %101 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %100
  store float %98, float* %101, align 4
  %102 = load i32, i32* %20, align 4
  %103 = sext i32 %102 to i64
  %104 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %103
  %105 = load float, float* %104, align 4
  %106 = load float, float* %27, align 4
  %107 = load float, float* %33, align 4
  %108 = fmul float %106, %107
  %109 = fadd float %105, %108
  %110 = load i32, i32* %20, align 4
  %111 = sext i32 %110 to i64
  %112 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %111
  store float %109, float* %112, align 4
  br label %113

; <label>:113:                                    ; preds = %61
  br label %114

; <label>:114:                                    ; preds = %113
  %115 = load i32, i32* %17, align 4
  %116 = add nsw i32 %115, 1
  store i32 %116, i32* %17, align 4
  br label %57

; <label>:117:                                    ; preds = %57
  br label %118

; <label>:118:                                    ; preds = %117
  %119 = load i32*, i32** %9, align 8
  %120 = load i32, i32* %119, align 4
  call void @__kmpc_for_static_fini(%ident_t* @0, i32 %120)
  br label %121

; <label>:121:                                    ; preds = %118, %8
  ret void
}

declare void @__kmpc_for_static_init_4(%ident_t*, i32, i32, i32*, i32*, i32*, i32*, i32, i32)

declare void @Step10_orig(i32, float, float, float, float, float, float*, float*, float*, float*, float*, float*, float*) #1

declare void @__kmpc_for_static_fini(%ident_t*, i32)

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
