; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/Step10_orig.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/HACCmk/Step10_orig.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: nounwind uwtable
define void @Step10_orig(i32, float, float, float, float, float, float*, float*, float*, float*, float*, float*, float*) #0 {
  %14 = alloca i32, align 4
  %15 = alloca float, align 4
  %16 = alloca float, align 4
  %17 = alloca float, align 4
  %18 = alloca float, align 4
  %19 = alloca float, align 4
  %20 = alloca float*, align 8
  %21 = alloca float*, align 8
  %22 = alloca float*, align 8
  %23 = alloca float*, align 8
  %24 = alloca float*, align 8
  %25 = alloca float*, align 8
  %26 = alloca float*, align 8
  %27 = alloca float, align 4
  %28 = alloca float, align 4
  %29 = alloca float, align 4
  %30 = alloca float, align 4
  %31 = alloca float, align 4
  %32 = alloca float, align 4
  %33 = alloca float, align 4
  %34 = alloca float, align 4
  %35 = alloca float, align 4
  %36 = alloca float, align 4
  %37 = alloca float, align 4
  %38 = alloca float, align 4
  %39 = alloca float, align 4
  %40 = alloca float, align 4
  %41 = alloca float, align 4
  %42 = alloca i32, align 4
  store i32 %0, i32* %14, align 4
  store float %1, float* %15, align 4
  store float %2, float* %16, align 4
  store float %3, float* %17, align 4
  store float %4, float* %18, align 4
  store float %5, float* %19, align 4
  store float* %6, float** %20, align 8
  store float* %7, float** %21, align 8
  store float* %8, float** %22, align 8
  store float* %9, float** %23, align 8
  store float* %10, float** %24, align 8
  store float* %11, float** %25, align 8
  store float* %12, float** %26, align 8
  store float 0x3FD13CA760000000, float* %27, align 4
  store float 0xBFB3399C00000000, float* %28, align 4
  store float 0x3F87833EE0000000, float* %29, align 4
  store float 0xBF51E8EB60000000, float* %30, align 4
  store float 0x3F0FBEC340000000, float* %31, align 4
  store float 0xBEB8B13440000000, float* %32, align 4
  store float 0.000000e+00, float* %39, align 4
  store float 0.000000e+00, float* %40, align 4
  store float 0.000000e+00, float* %41, align 4
  store i32 0, i32* %42, align 4
  br label %43

; <label>:43:                                     ; preds = %139, %13
  %44 = load i32, i32* %42, align 4
  %45 = load i32, i32* %14, align 4
  %46 = icmp slt i32 %44, %45
  br i1 %46, label %47, label %142

; <label>:47:                                     ; preds = %43
  %48 = load i32, i32* %42, align 4
  %49 = sext i32 %48 to i64
  %50 = load float*, float** %20, align 8
  %51 = getelementptr inbounds float, float* %50, i64 %49
  %52 = load float, float* %51, align 4
  %53 = load float, float* %15, align 4
  %54 = fsub float %52, %53
  store float %54, float* %33, align 4
  %55 = load i32, i32* %42, align 4
  %56 = sext i32 %55 to i64
  %57 = load float*, float** %21, align 8
  %58 = getelementptr inbounds float, float* %57, i64 %56
  %59 = load float, float* %58, align 4
  %60 = load float, float* %16, align 4
  %61 = fsub float %59, %60
  store float %61, float* %34, align 4
  %62 = load i32, i32* %42, align 4
  %63 = sext i32 %62 to i64
  %64 = load float*, float** %22, align 8
  %65 = getelementptr inbounds float, float* %64, i64 %63
  %66 = load float, float* %65, align 4
  %67 = load float, float* %17, align 4
  %68 = fsub float %66, %67
  store float %68, float* %35, align 4
  %69 = load float, float* %33, align 4
  %70 = load float, float* %33, align 4
  %71 = fmul float %69, %70
  %72 = load float, float* %34, align 4
  %73 = load float, float* %34, align 4
  %74 = fmul float %72, %73
  %75 = fadd float %71, %74
  %76 = load float, float* %35, align 4
  %77 = load float, float* %35, align 4
  %78 = fmul float %76, %77
  %79 = fadd float %75, %78
  store float %79, float* %37, align 4
  %80 = load float, float* %37, align 4
  %81 = load float, float* %18, align 4
  %82 = fcmp olt float %80, %81
  br i1 %82, label %83, label %89

; <label>:83:                                     ; preds = %47
  %84 = load i32, i32* %42, align 4
  %85 = sext i32 %84 to i64
  %86 = load float*, float** %23, align 8
  %87 = getelementptr inbounds float, float* %86, i64 %85
  %88 = load float, float* %87, align 4
  br label %90

; <label>:89:                                     ; preds = %47
  br label %90

; <label>:90:                                     ; preds = %89, %83
  %91 = phi float [ %88, %83 ], [ 0.000000e+00, %89 ]
  store float %91, float* %36, align 4
  %92 = load float, float* %37, align 4
  %93 = load float, float* %19, align 4
  %94 = fadd float %92, %93
  %95 = fpext float %94 to double
  %96 = call double @pow(double %95, double -1.500000e+00) #2
  %97 = load float, float* %37, align 4
  %98 = load float, float* %37, align 4
  %99 = load float, float* %37, align 4
  %100 = load float, float* %37, align 4
  %101 = load float, float* %37, align 4
  %102 = fmul float %101, 0xBEB8B13440000000
  %103 = fadd float 0x3F0FBEC340000000, %102
  %104 = fmul float %100, %103
  %105 = fadd float 0xBF51E8EB60000000, %104
  %106 = fmul float %99, %105
  %107 = fadd float 0x3F87833EE0000000, %106
  %108 = fmul float %98, %107
  %109 = fadd float 0xBFB3399C00000000, %108
  %110 = fmul float %97, %109
  %111 = fadd float 0x3FD13CA760000000, %110
  %112 = fpext float %111 to double
  %113 = fsub double %96, %112
  %114 = fptrunc double %113 to float
  store float %114, float* %38, align 4
  %115 = load float, float* %37, align 4
  %116 = fcmp ogt float %115, 0.000000e+00
  br i1 %116, label %117, label %121

; <label>:117:                                    ; preds = %90
  %118 = load float, float* %36, align 4
  %119 = load float, float* %38, align 4
  %120 = fmul float %118, %119
  br label %122

; <label>:121:                                    ; preds = %90
  br label %122

; <label>:122:                                    ; preds = %121, %117
  %123 = phi float [ %120, %117 ], [ 0.000000e+00, %121 ]
  store float %123, float* %38, align 4
  %124 = load float, float* %39, align 4
  %125 = load float, float* %38, align 4
  %126 = load float, float* %33, align 4
  %127 = fmul float %125, %126
  %128 = fadd float %124, %127
  store float %128, float* %39, align 4
  %129 = load float, float* %40, align 4
  %130 = load float, float* %38, align 4
  %131 = load float, float* %34, align 4
  %132 = fmul float %130, %131
  %133 = fadd float %129, %132
  store float %133, float* %40, align 4
  %134 = load float, float* %41, align 4
  %135 = load float, float* %38, align 4
  %136 = load float, float* %35, align 4
  %137 = fmul float %135, %136
  %138 = fadd float %134, %137
  store float %138, float* %41, align 4
  br label %139

; <label>:139:                                    ; preds = %122
  %140 = load i32, i32* %42, align 4
  %141 = add nsw i32 %140, 1
  store i32 %141, i32* %42, align 4
  br label %43

; <label>:142:                                    ; preds = %43
  %143 = load float, float* %39, align 4
  %144 = load float*, float** %24, align 8
  store float %143, float* %144, align 4
  %145 = load float, float* %40, align 4
  %146 = load float*, float** %25, align 8
  store float %145, float* %146, align 4
  %147 = load float, float* %41, align 4
  %148 = load float*, float** %26, align 8
  store float %147, float* %148, align 4
  ret void
}

; Function Attrs: nounwind
declare double @pow(double, double) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
