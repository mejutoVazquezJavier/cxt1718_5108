; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/main.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.hypre_CSRMatrix = type { double*, i32*, i32*, i32, i32, i32, i32*, i32, i32 }
%struct.hypre_Vector = type { double*, i32, i32, i32, i32, i32, i32 }

@testIter = constant i32 500, align 4
@totalWallTime = global double 0.000000e+00, align 8
@.str = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.1 = private unnamed_addr constant [17 x i8] c"//------------ \0A\00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c"// \0A\00", align 1
@.str.3 = private unnamed_addr constant [41 x i8] c"//  CORAL  AMGmk Benchmark Version 1.0 \0A\00", align 1
@.str.4 = private unnamed_addr constant [25 x i8] c"\0Amax_num_threads = %d \0A\0A\00", align 1
@.str.5 = private unnamed_addr constant [21 x i8] c"\0A testIter   = %d \0A\0A\00", align 1
@.str.6 = private unnamed_addr constant [13 x i8] c"//   MATVEC\0A\00", align 1
@.str.7 = private unnamed_addr constant [27 x i8] c"\0AWall time = %f seconds. \0A\00", align 1
@.str.8 = private unnamed_addr constant [12 x i8] c"//   Relax\0A\00", align 1
@.str.9 = private unnamed_addr constant [11 x i8] c"//   Axpy\0A\00", align 1
@.str.10 = private unnamed_addr constant [33 x i8] c"\0ATotal Wall time = %f seconds. \0A\00", align 1
@.str.11 = private unnamed_addr constant [22 x i8] c" \0A Matvec: error: %e\0A\00", align 1
@.str.12 = private unnamed_addr constant [21 x i8] c" \0A Relax: error: %e\0A\00", align 1
@.str.13 = private unnamed_addr constant [20 x i8] c" \0A Axpy: error: %e\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca double, align 8
  %7 = alloca double, align 8
  %8 = alloca double, align 8
  %9 = alloca i32, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store double 0.000000e+00, double* %6, align 8
  store double 0.000000e+00, double* %7, align 8
  store double 0.000000e+00, double* %8, align 8
  %10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  %11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.3, i32 0, i32 0))
  %14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %16 = call i32 @omp_get_num_threads() #4
  store i32 %16, i32* %9, align 4
  %17 = load i32, i32* %9, align 4
  %18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.4, i32 0, i32 0), i32 %17)
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.5, i32 0, i32 0), i32 500)
  %20 = call double @omp_get_wtime() #4
  store double %20, double* %6, align 8
  store double 0.000000e+00, double* @totalWallTime, align 8
  call void @test_Matvec()
  %21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  %22 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i32 0, i32 0))
  %25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %27 = load double, double* @totalWallTime, align 8
  %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.7, i32 0, i32 0), double %27)
  store double 0.000000e+00, double* @totalWallTime, align 8
  call void @test_Relax()
  %29 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  %30 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %31 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %32 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.8, i32 0, i32 0))
  %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %34 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %35 = load double, double* @totalWallTime, align 8
  %36 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.7, i32 0, i32 0), double %35)
  store double 0.000000e+00, double* @totalWallTime, align 8
  call void @test_Axpy()
  %37 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %39 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %40 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.9, i32 0, i32 0))
  %41 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0))
  %42 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.1, i32 0, i32 0))
  %43 = load double, double* @totalWallTime, align 8
  %44 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.7, i32 0, i32 0), double %43)
  %45 = call double @omp_get_wtime() #4
  store double %45, double* %7, align 8
  %46 = load double, double* %7, align 8
  %47 = load double, double* %6, align 8
  %48 = fsub double %46, %47
  store double %48, double* %8, align 8
  %49 = load double, double* %8, align 8
  %50 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.10, i32 0, i32 0), double %49)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind
declare i32 @omp_get_num_threads() #2

; Function Attrs: nounwind
declare double @omp_get_wtime() #2

; Function Attrs: nounwind uwtable
define void @test_Matvec() #0 {
  %1 = alloca double, align 8
  %2 = alloca double, align 8
  %3 = alloca %struct.hypre_CSRMatrix*, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca %struct.hypre_Vector*, align 8
  %6 = alloca %struct.hypre_Vector*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca double*, align 8
  %12 = alloca double*, align 8
  %13 = alloca double*, align 8
  %14 = alloca double, align 8
  %15 = alloca double, align 8
  store double 0.000000e+00, double* %1, align 8
  store double 0.000000e+00, double* %2, align 8
  store i32 50, i32* %7, align 4
  store i32 50, i32* %8, align 4
  store i32 50, i32* %9, align 4
  %16 = call i8* @hypre_CAlloc(i32 4, i32 8)
  %17 = bitcast i8* %16 to double*
  store double* %17, double** %11, align 8
  %18 = load double*, double** %11, align 8
  %19 = getelementptr inbounds double, double* %18, i64 0
  store double 6.000000e+00, double* %19, align 8
  %20 = load double*, double** %11, align 8
  %21 = getelementptr inbounds double, double* %20, i64 1
  store double -1.000000e+00, double* %21, align 8
  %22 = load double*, double** %11, align 8
  %23 = getelementptr inbounds double, double* %22, i64 2
  store double -1.000000e+00, double* %23, align 8
  %24 = load double*, double** %11, align 8
  %25 = getelementptr inbounds double, double* %24, i64 3
  store double -1.000000e+00, double* %25, align 8
  %26 = load i32, i32* %7, align 4
  %27 = load i32, i32* %8, align 4
  %28 = load i32, i32* %9, align 4
  %29 = load double*, double** %11, align 8
  %30 = call %struct.hypre_CSRMatrix* @GenerateSeqLaplacian(i32 %26, i32 %27, i32 %28, double* %29, %struct.hypre_Vector** %5, %struct.hypre_Vector** %4, %struct.hypre_Vector** %6)
  store %struct.hypre_CSRMatrix* %30, %struct.hypre_CSRMatrix** %3, align 8
  %31 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %32 = call i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector* %31, double 1.000000e+00)
  %33 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %34 = call i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector* %33, double 0.000000e+00)
  %35 = call double @omp_get_wtime() #4
  store double %35, double* %1, align 8
  store i32 0, i32* %10, align 4
  br label %36

; <label>:36:                                     ; preds = %44, %0
  %37 = load i32, i32* %10, align 4
  %38 = icmp slt i32 %37, 500
  br i1 %38, label %39, label %47

; <label>:39:                                     ; preds = %36
  %40 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %3, align 8
  %41 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %42 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %43 = call i32 @hypre_CSRMatrixMatvec(double 1.000000e+00, %struct.hypre_CSRMatrix* %40, %struct.hypre_Vector* %41, double 0.000000e+00, %struct.hypre_Vector* %42)
  br label %44

; <label>:44:                                     ; preds = %39
  %45 = load i32, i32* %10, align 4
  %46 = add nsw i32 %45, 1
  store i32 %46, i32* %10, align 4
  br label %36

; <label>:47:                                     ; preds = %36
  %48 = call double @omp_get_wtime() #4
  store double %48, double* %2, align 8
  %49 = load double, double* %2, align 8
  %50 = load double, double* %1, align 8
  %51 = fsub double %49, %50
  %52 = load double, double* @totalWallTime, align 8
  %53 = fadd double %52, %51
  store double %53, double* @totalWallTime, align 8
  %54 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %55 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %54, i32 0, i32 0
  %56 = load double*, double** %55, align 8
  store double* %56, double** %12, align 8
  %57 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %58 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %57, i32 0, i32 0
  %59 = load double*, double** %58, align 8
  store double* %59, double** %13, align 8
  store double 0.000000e+00, double* %14, align 8
  store i32 0, i32* %10, align 4
  br label %60

; <label>:60:                                     ; preds = %87, %47
  %61 = load i32, i32* %10, align 4
  %62 = load i32, i32* %7, align 4
  %63 = load i32, i32* %8, align 4
  %64 = mul nsw i32 %62, %63
  %65 = load i32, i32* %9, align 4
  %66 = mul nsw i32 %64, %65
  %67 = icmp slt i32 %61, %66
  br i1 %67, label %68, label %90

; <label>:68:                                     ; preds = %60
  %69 = load i32, i32* %10, align 4
  %70 = sext i32 %69 to i64
  %71 = load double*, double** %12, align 8
  %72 = getelementptr inbounds double, double* %71, i64 %70
  %73 = load double, double* %72, align 8
  %74 = load i32, i32* %10, align 4
  %75 = sext i32 %74 to i64
  %76 = load double*, double** %13, align 8
  %77 = getelementptr inbounds double, double* %76, i64 %75
  %78 = load double, double* %77, align 8
  %79 = fsub double %73, %78
  %80 = call double @fabs(double %79) #5
  store double %80, double* %15, align 8
  %81 = load double, double* %15, align 8
  %82 = load double, double* %14, align 8
  %83 = fcmp ogt double %81, %82
  br i1 %83, label %84, label %86

; <label>:84:                                     ; preds = %68
  %85 = load double, double* %15, align 8
  store double %85, double* %14, align 8
  br label %86

; <label>:86:                                     ; preds = %84, %68
  br label %87

; <label>:87:                                     ; preds = %86
  %88 = load i32, i32* %10, align 4
  %89 = add nsw i32 %88, 1
  store i32 %89, i32* %10, align 4
  br label %60

; <label>:90:                                     ; preds = %60
  %91 = load double, double* %14, align 8
  %92 = fcmp ogt double %91, 0.000000e+00
  br i1 %92, label %93, label %96

; <label>:93:                                     ; preds = %90
  %94 = load double, double* %14, align 8
  %95 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.11, i32 0, i32 0), double %94)
  br label %96

; <label>:96:                                     ; preds = %93, %90
  %97 = load double*, double** %11, align 8
  %98 = bitcast double* %97 to i8*
  call void @hypre_Free(i8* %98)
  store double* null, double** %11, align 8
  %99 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %3, align 8
  %100 = call i32 @hypre_CSRMatrixDestroy(%struct.hypre_CSRMatrix* %99)
  %101 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %102 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %101)
  %103 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %104 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %103)
  %105 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %106 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %105)
  ret void
}

declare i8* @hypre_CAlloc(i32, i32) #1

declare %struct.hypre_CSRMatrix* @GenerateSeqLaplacian(i32, i32, i32, double*, %struct.hypre_Vector**, %struct.hypre_Vector**, %struct.hypre_Vector**) #1

declare i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector*, double) #1

declare i32 @hypre_CSRMatrixMatvec(double, %struct.hypre_CSRMatrix*, %struct.hypre_Vector*, double, %struct.hypre_Vector*) #1

; Function Attrs: nounwind readnone
declare double @fabs(double) #3

declare void @hypre_Free(i8*) #1

declare i32 @hypre_CSRMatrixDestroy(%struct.hypre_CSRMatrix*) #1

declare i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector*) #1

; Function Attrs: nounwind uwtable
define void @test_Relax() #0 {
  %1 = alloca double, align 8
  %2 = alloca double, align 8
  %3 = alloca %struct.hypre_CSRMatrix*, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca %struct.hypre_Vector*, align 8
  %6 = alloca %struct.hypre_Vector*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca double*, align 8
  %12 = alloca double*, align 8
  %13 = alloca double, align 8
  %14 = alloca double, align 8
  store double 0.000000e+00, double* %1, align 8
  store double 0.000000e+00, double* %2, align 8
  store i32 50, i32* %7, align 4
  store i32 50, i32* %8, align 4
  store i32 50, i32* %9, align 4
  %15 = call i8* @hypre_CAlloc(i32 4, i32 8)
  %16 = bitcast i8* %15 to double*
  store double* %16, double** %11, align 8
  %17 = load double*, double** %11, align 8
  %18 = getelementptr inbounds double, double* %17, i64 0
  store double 6.000000e+00, double* %18, align 8
  %19 = load double*, double** %11, align 8
  %20 = getelementptr inbounds double, double* %19, i64 1
  store double -1.000000e+00, double* %20, align 8
  %21 = load double*, double** %11, align 8
  %22 = getelementptr inbounds double, double* %21, i64 2
  store double -1.000000e+00, double* %22, align 8
  %23 = load double*, double** %11, align 8
  %24 = getelementptr inbounds double, double* %23, i64 3
  store double -1.000000e+00, double* %24, align 8
  %25 = load i32, i32* %7, align 4
  %26 = load i32, i32* %8, align 4
  %27 = load i32, i32* %9, align 4
  %28 = load double*, double** %11, align 8
  %29 = call %struct.hypre_CSRMatrix* @GenerateSeqLaplacian(i32 %25, i32 %26, i32 %27, double* %28, %struct.hypre_Vector** %5, %struct.hypre_Vector** %4, %struct.hypre_Vector** %6)
  store %struct.hypre_CSRMatrix* %29, %struct.hypre_CSRMatrix** %3, align 8
  %30 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %31 = call i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector* %30, double 1.000000e+00)
  %32 = call double @omp_get_wtime() #4
  store double %32, double* %1, align 8
  store i32 0, i32* %10, align 4
  br label %33

; <label>:33:                                     ; preds = %41, %0
  %34 = load i32, i32* %10, align 4
  %35 = icmp slt i32 %34, 500
  br i1 %35, label %36, label %44

; <label>:36:                                     ; preds = %33
  %37 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %3, align 8
  %38 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %39 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %40 = call i32 @hypre_BoomerAMGSeqRelax(%struct.hypre_CSRMatrix* %37, %struct.hypre_Vector* %38, %struct.hypre_Vector* %39)
  br label %41

; <label>:41:                                     ; preds = %36
  %42 = load i32, i32* %10, align 4
  %43 = add nsw i32 %42, 1
  store i32 %43, i32* %10, align 4
  br label %33

; <label>:44:                                     ; preds = %33
  %45 = call double @omp_get_wtime() #4
  store double %45, double* %2, align 8
  %46 = load double, double* %2, align 8
  %47 = load double, double* %1, align 8
  %48 = fsub double %46, %47
  %49 = load double, double* @totalWallTime, align 8
  %50 = fadd double %49, %48
  store double %50, double* @totalWallTime, align 8
  %51 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %52 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %51, i32 0, i32 0
  %53 = load double*, double** %52, align 8
  store double* %53, double** %12, align 8
  store double 0.000000e+00, double* %14, align 8
  store i32 0, i32* %10, align 4
  br label %54

; <label>:54:                                     ; preds = %76, %44
  %55 = load i32, i32* %10, align 4
  %56 = load i32, i32* %7, align 4
  %57 = load i32, i32* %8, align 4
  %58 = mul nsw i32 %56, %57
  %59 = load i32, i32* %9, align 4
  %60 = mul nsw i32 %58, %59
  %61 = icmp slt i32 %55, %60
  br i1 %61, label %62, label %79

; <label>:62:                                     ; preds = %54
  %63 = load i32, i32* %10, align 4
  %64 = sext i32 %63 to i64
  %65 = load double*, double** %12, align 8
  %66 = getelementptr inbounds double, double* %65, i64 %64
  %67 = load double, double* %66, align 8
  %68 = fsub double %67, 1.000000e+00
  %69 = call double @fabs(double %68) #5
  store double %69, double* %13, align 8
  %70 = load double, double* %13, align 8
  %71 = load double, double* %14, align 8
  %72 = fcmp ogt double %70, %71
  br i1 %72, label %73, label %75

; <label>:73:                                     ; preds = %62
  %74 = load double, double* %13, align 8
  store double %74, double* %14, align 8
  br label %75

; <label>:75:                                     ; preds = %73, %62
  br label %76

; <label>:76:                                     ; preds = %75
  %77 = load i32, i32* %10, align 4
  %78 = add nsw i32 %77, 1
  store i32 %78, i32* %10, align 4
  br label %54

; <label>:79:                                     ; preds = %54
  %80 = load double, double* %14, align 8
  %81 = fcmp ogt double %80, 0.000000e+00
  br i1 %81, label %82, label %85

; <label>:82:                                     ; preds = %79
  %83 = load double, double* %14, align 8
  %84 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.12, i32 0, i32 0), double %83)
  br label %85

; <label>:85:                                     ; preds = %82, %79
  %86 = load double*, double** %11, align 8
  %87 = bitcast double* %86 to i8*
  call void @hypre_Free(i8* %87)
  store double* null, double** %11, align 8
  %88 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %3, align 8
  %89 = call i32 @hypre_CSRMatrixDestroy(%struct.hypre_CSRMatrix* %88)
  %90 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %91 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %90)
  %92 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %93 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %92)
  %94 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %95 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %94)
  ret void
}

declare i32 @hypre_BoomerAMGSeqRelax(%struct.hypre_CSRMatrix*, %struct.hypre_Vector*, %struct.hypre_Vector*) #1

; Function Attrs: nounwind uwtable
define void @test_Axpy() #0 {
  %1 = alloca double, align 8
  %2 = alloca double, align 8
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  %7 = alloca double, align 8
  %8 = alloca double, align 8
  %9 = alloca double, align 8
  %10 = alloca double*, align 8
  store double 0.000000e+00, double* %1, align 8
  store double 0.000000e+00, double* %2, align 8
  store double 5.000000e-01, double* %7, align 8
  store i32 125000, i32* %5, align 4
  %11 = load i32, i32* %5, align 4
  %12 = call %struct.hypre_Vector* @hypre_SeqVectorCreate(i32 %11)
  store %struct.hypre_Vector* %12, %struct.hypre_Vector** %3, align 8
  %13 = load i32, i32* %5, align 4
  %14 = call %struct.hypre_Vector* @hypre_SeqVectorCreate(i32 %13)
  store %struct.hypre_Vector* %14, %struct.hypre_Vector** %4, align 8
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %16 = call i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector* %15)
  %17 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %18 = call i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector* %17)
  %19 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %20 = call i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector* %19, double 1.000000e+00)
  %21 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %22 = call i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector* %21, double 1.000000e+00)
  %23 = call double @omp_get_wtime() #4
  store double %23, double* %1, align 8
  store i32 0, i32* %6, align 4
  br label %24

; <label>:24:                                     ; preds = %32, %0
  %25 = load i32, i32* %6, align 4
  %26 = icmp slt i32 %25, 500
  br i1 %26, label %27, label %35

; <label>:27:                                     ; preds = %24
  %28 = load double, double* %7, align 8
  %29 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %30 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %31 = call i32 @hypre_SeqVectorAxpy(double %28, %struct.hypre_Vector* %29, %struct.hypre_Vector* %30)
  br label %32

; <label>:32:                                     ; preds = %27
  %33 = load i32, i32* %6, align 4
  %34 = add nsw i32 %33, 1
  store i32 %34, i32* %6, align 4
  br label %24

; <label>:35:                                     ; preds = %24
  %36 = call double @omp_get_wtime() #4
  store double %36, double* %2, align 8
  %37 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %38 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %37, i32 0, i32 0
  %39 = load double*, double** %38, align 8
  store double* %39, double** %10, align 8
  store double 0.000000e+00, double* %9, align 8
  store i32 0, i32* %6, align 4
  br label %40

; <label>:40:                                     ; preds = %59, %35
  %41 = load i32, i32* %6, align 4
  %42 = load i32, i32* %5, align 4
  %43 = icmp slt i32 %41, %42
  br i1 %43, label %44, label %62

; <label>:44:                                     ; preds = %40
  %45 = load i32, i32* %6, align 4
  %46 = sext i32 %45 to i64
  %47 = load double*, double** %10, align 8
  %48 = getelementptr inbounds double, double* %47, i64 %46
  %49 = load double, double* %48, align 8
  %50 = fsub double %49, 1.000000e+00
  %51 = fsub double %50, 2.500000e+02
  %52 = call double @fabs(double %51) #5
  store double %52, double* %8, align 8
  %53 = load double, double* %8, align 8
  %54 = load double, double* %9, align 8
  %55 = fcmp ogt double %53, %54
  br i1 %55, label %56, label %58

; <label>:56:                                     ; preds = %44
  %57 = load double, double* %8, align 8
  store double %57, double* %9, align 8
  br label %58

; <label>:58:                                     ; preds = %56, %44
  br label %59

; <label>:59:                                     ; preds = %58
  %60 = load i32, i32* %6, align 4
  %61 = add nsw i32 %60, 1
  store i32 %61, i32* %6, align 4
  br label %40

; <label>:62:                                     ; preds = %40
  %63 = load double, double* %9, align 8
  %64 = fcmp ogt double %63, 0.000000e+00
  br i1 %64, label %65, label %68

; <label>:65:                                     ; preds = %62
  %66 = load double, double* %9, align 8
  %67 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.13, i32 0, i32 0), double %66)
  br label %68

; <label>:68:                                     ; preds = %65, %62
  %69 = load double, double* %2, align 8
  %70 = load double, double* %1, align 8
  %71 = fsub double %69, %70
  %72 = load double, double* @totalWallTime, align 8
  %73 = fadd double %72, %71
  store double %73, double* @totalWallTime, align 8
  %74 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %75 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %74)
  %76 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %77 = call i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector* %76)
  ret void
}

declare %struct.hypre_Vector* @hypre_SeqVectorCreate(i32) #1

declare i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector*) #1

declare i32 @hypre_SeqVectorAxpy(double, %struct.hypre_Vector*, %struct.hypre_Vector*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { nounwind readnone }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
