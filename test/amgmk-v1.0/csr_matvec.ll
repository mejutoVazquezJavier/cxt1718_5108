; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/csr_matvec.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/csr_matvec.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.hypre_CSRMatrix = type { double*, i32*, i32*, i32, i32, i32, i32*, i32, i32 }
%struct.hypre_Vector = type { double*, i32, i32, i32, i32, i32, i32 }

@stderr = external global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [25 x i8] c"hypre_assert failed: %s\0A\00", align 1
@.str.1 = private unnamed_addr constant [41 x i8] c"num_vectors == hypre_VectorNumVectors(y)\00", align 1
@.str.2 = private unnamed_addr constant [74 x i8] c"/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/csr_matvec.c\00", align 1

; Function Attrs: nounwind uwtable
define i32 @hypre_CSRMatrixMatvec(double, %struct.hypre_CSRMatrix*, %struct.hypre_Vector*, double, %struct.hypre_Vector*) #0 {
  %6 = alloca i32, align 4
  %7 = alloca double, align 8
  %8 = alloca %struct.hypre_CSRMatrix*, align 8
  %9 = alloca %struct.hypre_Vector*, align 8
  %10 = alloca double, align 8
  %11 = alloca %struct.hypre_Vector*, align 8
  %12 = alloca double*, align 8
  %13 = alloca i32*, align 8
  %14 = alloca i32*, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32*, align 8
  %18 = alloca i32, align 4
  %19 = alloca double*, align 8
  %20 = alloca double*, align 8
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca i32, align 4
  %27 = alloca i32, align 4
  %28 = alloca double, align 8
  %29 = alloca double, align 8
  %30 = alloca i32, align 4
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca double, align 8
  %35 = alloca i32, align 4
  store double %0, double* %7, align 8
  store %struct.hypre_CSRMatrix* %1, %struct.hypre_CSRMatrix** %8, align 8
  store %struct.hypre_Vector* %2, %struct.hypre_Vector** %9, align 8
  store double %3, double* %10, align 8
  store %struct.hypre_Vector* %4, %struct.hypre_Vector** %11, align 8
  %36 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %37 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %36, i32 0, i32 0
  %38 = load double*, double** %37, align 8
  store double* %38, double** %12, align 8
  %39 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %40 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %39, i32 0, i32 1
  %41 = load i32*, i32** %40, align 8
  store i32* %41, i32** %13, align 8
  %42 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %43 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %42, i32 0, i32 2
  %44 = load i32*, i32** %43, align 8
  store i32* %44, i32** %14, align 8
  %45 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %46 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %45, i32 0, i32 3
  %47 = load i32, i32* %46, align 8
  store i32 %47, i32* %15, align 4
  %48 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %49 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %48, i32 0, i32 4
  %50 = load i32, i32* %49, align 4
  store i32 %50, i32* %16, align 4
  %51 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %52 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %51, i32 0, i32 6
  %53 = load i32*, i32** %52, align 8
  store i32* %53, i32** %17, align 8
  %54 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %55 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %54, i32 0, i32 7
  %56 = load i32, i32* %55, align 8
  store i32 %56, i32* %18, align 4
  %57 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %58 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %57, i32 0, i32 0
  %59 = load double*, double** %58, align 8
  store double* %59, double** %19, align 8
  %60 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %61 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %60, i32 0, i32 0
  %62 = load double*, double** %61, align 8
  store double* %62, double** %20, align 8
  %63 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %64 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %63, i32 0, i32 1
  %65 = load i32, i32* %64, align 8
  store i32 %65, i32* %21, align 4
  %66 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %67 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %66, i32 0, i32 1
  %68 = load i32, i32* %67, align 8
  store i32 %68, i32* %22, align 4
  %69 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %70 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %69, i32 0, i32 3
  %71 = load i32, i32* %70, align 8
  store i32 %71, i32* %23, align 4
  %72 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %73 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %72, i32 0, i32 6
  %74 = load i32, i32* %73, align 4
  store i32 %74, i32* %24, align 4
  %75 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %76 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %75, i32 0, i32 5
  %77 = load i32, i32* %76, align 8
  store i32 %77, i32* %25, align 4
  %78 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %79 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %78, i32 0, i32 6
  %80 = load i32, i32* %79, align 4
  store i32 %80, i32* %26, align 4
  %81 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %82 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %81, i32 0, i32 5
  %83 = load i32, i32* %82, align 8
  store i32 %83, i32* %27, align 4
  store double 7.000000e-01, double* %34, align 8
  store i32 0, i32* %35, align 4
  %84 = load i32, i32* %23, align 4
  %85 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %86 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %85, i32 0, i32 3
  %87 = load i32, i32* %86, align 8
  %88 = icmp eq i32 %84, %87
  br i1 %88, label %92, label %89

; <label>:89:                                     ; preds = %5
  %90 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %91 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %90, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.1, i32 0, i32 0))
  call void @hypre_error_handler(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.2, i32 0, i32 0), i32 91, i32 1)
  br label %92

; <label>:92:                                     ; preds = %89, %5
  %93 = load i32, i32* %16, align 4
  %94 = load i32, i32* %21, align 4
  %95 = icmp ne i32 %93, %94
  br i1 %95, label %96, label %97

; <label>:96:                                     ; preds = %92
  store i32 1, i32* %35, align 4
  br label %97

; <label>:97:                                     ; preds = %96, %92
  %98 = load i32, i32* %15, align 4
  %99 = load i32, i32* %22, align 4
  %100 = icmp ne i32 %98, %99
  br i1 %100, label %101, label %102

; <label>:101:                                    ; preds = %97
  store i32 2, i32* %35, align 4
  br label %102

; <label>:102:                                    ; preds = %101, %97
  %103 = load i32, i32* %16, align 4
  %104 = load i32, i32* %21, align 4
  %105 = icmp ne i32 %103, %104
  br i1 %105, label %106, label %111

; <label>:106:                                    ; preds = %102
  %107 = load i32, i32* %15, align 4
  %108 = load i32, i32* %22, align 4
  %109 = icmp ne i32 %107, %108
  br i1 %109, label %110, label %111

; <label>:110:                                    ; preds = %106
  store i32 3, i32* %35, align 4
  br label %111

; <label>:111:                                    ; preds = %110, %106, %102
  %112 = load double, double* %7, align 8
  %113 = fcmp oeq double %112, 0.000000e+00
  br i1 %113, label %114, label %134

; <label>:114:                                    ; preds = %111
  store i32 0, i32* %30, align 4
  br label %115

; <label>:115:                                    ; preds = %129, %114
  %116 = load i32, i32* %30, align 4
  %117 = load i32, i32* %15, align 4
  %118 = load i32, i32* %23, align 4
  %119 = mul nsw i32 %117, %118
  %120 = icmp slt i32 %116, %119
  br i1 %120, label %121, label %132

; <label>:121:                                    ; preds = %115
  %122 = load double, double* %10, align 8
  %123 = load i32, i32* %30, align 4
  %124 = sext i32 %123 to i64
  %125 = load double*, double** %20, align 8
  %126 = getelementptr inbounds double, double* %125, i64 %124
  %127 = load double, double* %126, align 8
  %128 = fmul double %127, %122
  store double %128, double* %126, align 8
  br label %129

; <label>:129:                                    ; preds = %121
  %130 = load i32, i32* %30, align 4
  %131 = add nsw i32 %130, 1
  store i32 %131, i32* %30, align 4
  br label %115

; <label>:132:                                    ; preds = %115
  %133 = load i32, i32* %35, align 4
  store i32 %133, i32* %6, align 4
  br label %484

; <label>:134:                                    ; preds = %111
  %135 = load double, double* %10, align 8
  %136 = load double, double* %7, align 8
  %137 = fdiv double %135, %136
  store double %137, double* %28, align 8
  %138 = load double, double* %28, align 8
  %139 = fcmp une double %138, 1.000000e+00
  br i1 %139, label %140, label %179

; <label>:140:                                    ; preds = %134
  %141 = load double, double* %28, align 8
  %142 = fcmp oeq double %141, 0.000000e+00
  br i1 %142, label %143, label %159

; <label>:143:                                    ; preds = %140
  store i32 0, i32* %30, align 4
  br label %144

; <label>:144:                                    ; preds = %155, %143
  %145 = load i32, i32* %30, align 4
  %146 = load i32, i32* %15, align 4
  %147 = load i32, i32* %23, align 4
  %148 = mul nsw i32 %146, %147
  %149 = icmp slt i32 %145, %148
  br i1 %149, label %150, label %158

; <label>:150:                                    ; preds = %144
  %151 = load i32, i32* %30, align 4
  %152 = sext i32 %151 to i64
  %153 = load double*, double** %20, align 8
  %154 = getelementptr inbounds double, double* %153, i64 %152
  store double 0.000000e+00, double* %154, align 8
  br label %155

; <label>:155:                                    ; preds = %150
  %156 = load i32, i32* %30, align 4
  %157 = add nsw i32 %156, 1
  store i32 %157, i32* %30, align 4
  br label %144

; <label>:158:                                    ; preds = %144
  br label %178

; <label>:159:                                    ; preds = %140
  store i32 0, i32* %30, align 4
  br label %160

; <label>:160:                                    ; preds = %174, %159
  %161 = load i32, i32* %30, align 4
  %162 = load i32, i32* %15, align 4
  %163 = load i32, i32* %23, align 4
  %164 = mul nsw i32 %162, %163
  %165 = icmp slt i32 %161, %164
  br i1 %165, label %166, label %177

; <label>:166:                                    ; preds = %160
  %167 = load double, double* %28, align 8
  %168 = load i32, i32* %30, align 4
  %169 = sext i32 %168 to i64
  %170 = load double*, double** %20, align 8
  %171 = getelementptr inbounds double, double* %170, i64 %169
  %172 = load double, double* %171, align 8
  %173 = fmul double %172, %167
  store double %173, double* %171, align 8
  br label %174

; <label>:174:                                    ; preds = %166
  %175 = load i32, i32* %30, align 4
  %176 = add nsw i32 %175, 1
  store i32 %176, i32* %30, align 4
  br label %160

; <label>:177:                                    ; preds = %160
  br label %178

; <label>:178:                                    ; preds = %177, %158
  br label %179

; <label>:179:                                    ; preds = %178, %134
  %180 = load i32, i32* %18, align 4
  %181 = sitofp i32 %180 to double
  %182 = load double, double* %34, align 8
  %183 = load i32, i32* %15, align 4
  %184 = sitofp i32 %183 to double
  %185 = fmul double %182, %184
  %186 = fcmp olt double %181, %185
  br i1 %186, label %187, label %326

; <label>:187:                                    ; preds = %179
  store i32 0, i32* %30, align 4
  br label %188

; <label>:188:                                    ; preds = %322, %187
  %189 = load i32, i32* %30, align 4
  %190 = load i32, i32* %18, align 4
  %191 = icmp slt i32 %189, %190
  br i1 %191, label %192, label %325

; <label>:192:                                    ; preds = %188
  %193 = load i32, i32* %30, align 4
  %194 = sext i32 %193 to i64
  %195 = load i32*, i32** %17, align 8
  %196 = getelementptr inbounds i32, i32* %195, i64 %194
  %197 = load i32, i32* %196, align 4
  store i32 %197, i32* %33, align 4
  %198 = load i32, i32* %23, align 4
  %199 = icmp eq i32 %198, 1
  br i1 %199, label %200, label %247

; <label>:200:                                    ; preds = %192
  %201 = load i32, i32* %33, align 4
  %202 = sext i32 %201 to i64
  %203 = load double*, double** %20, align 8
  %204 = getelementptr inbounds double, double* %203, i64 %202
  %205 = load double, double* %204, align 8
  store double %205, double* %29, align 8
  %206 = load i32, i32* %33, align 4
  %207 = sext i32 %206 to i64
  %208 = load i32*, i32** %13, align 8
  %209 = getelementptr inbounds i32, i32* %208, i64 %207
  %210 = load i32, i32* %209, align 4
  store i32 %210, i32* %32, align 4
  br label %211

; <label>:211:                                    ; preds = %238, %200
  %212 = load i32, i32* %32, align 4
  %213 = load i32, i32* %33, align 4
  %214 = add nsw i32 %213, 1
  %215 = sext i32 %214 to i64
  %216 = load i32*, i32** %13, align 8
  %217 = getelementptr inbounds i32, i32* %216, i64 %215
  %218 = load i32, i32* %217, align 4
  %219 = icmp slt i32 %212, %218
  br i1 %219, label %220, label %241

; <label>:220:                                    ; preds = %211
  %221 = load i32, i32* %32, align 4
  %222 = sext i32 %221 to i64
  %223 = load double*, double** %12, align 8
  %224 = getelementptr inbounds double, double* %223, i64 %222
  %225 = load double, double* %224, align 8
  %226 = load i32, i32* %32, align 4
  %227 = sext i32 %226 to i64
  %228 = load i32*, i32** %14, align 8
  %229 = getelementptr inbounds i32, i32* %228, i64 %227
  %230 = load i32, i32* %229, align 4
  %231 = sext i32 %230 to i64
  %232 = load double*, double** %19, align 8
  %233 = getelementptr inbounds double, double* %232, i64 %231
  %234 = load double, double* %233, align 8
  %235 = fmul double %225, %234
  %236 = load double, double* %29, align 8
  %237 = fadd double %236, %235
  store double %237, double* %29, align 8
  br label %238

; <label>:238:                                    ; preds = %220
  %239 = load i32, i32* %32, align 4
  %240 = add nsw i32 %239, 1
  store i32 %240, i32* %32, align 4
  br label %211

; <label>:241:                                    ; preds = %211
  %242 = load double, double* %29, align 8
  %243 = load i32, i32* %33, align 4
  %244 = sext i32 %243 to i64
  %245 = load double*, double** %20, align 8
  %246 = getelementptr inbounds double, double* %245, i64 %244
  store double %242, double* %246, align 8
  br label %321

; <label>:247:                                    ; preds = %192
  store i32 0, i32* %31, align 4
  br label %248

; <label>:248:                                    ; preds = %317, %247
  %249 = load i32, i32* %31, align 4
  %250 = load i32, i32* %23, align 4
  %251 = icmp slt i32 %249, %250
  br i1 %251, label %252, label %320

; <label>:252:                                    ; preds = %248
  %253 = load i32, i32* %31, align 4
  %254 = load i32, i32* %25, align 4
  %255 = mul nsw i32 %253, %254
  %256 = load i32, i32* %33, align 4
  %257 = load i32, i32* %24, align 4
  %258 = mul nsw i32 %256, %257
  %259 = add nsw i32 %255, %258
  %260 = sext i32 %259 to i64
  %261 = load double*, double** %20, align 8
  %262 = getelementptr inbounds double, double* %261, i64 %260
  %263 = load double, double* %262, align 8
  store double %263, double* %29, align 8
  %264 = load i32, i32* %33, align 4
  %265 = sext i32 %264 to i64
  %266 = load i32*, i32** %13, align 8
  %267 = getelementptr inbounds i32, i32* %266, i64 %265
  %268 = load i32, i32* %267, align 4
  store i32 %268, i32* %32, align 4
  br label %269

; <label>:269:                                    ; preds = %302, %252
  %270 = load i32, i32* %32, align 4
  %271 = load i32, i32* %33, align 4
  %272 = add nsw i32 %271, 1
  %273 = sext i32 %272 to i64
  %274 = load i32*, i32** %13, align 8
  %275 = getelementptr inbounds i32, i32* %274, i64 %273
  %276 = load i32, i32* %275, align 4
  %277 = icmp slt i32 %270, %276
  br i1 %277, label %278, label %305

; <label>:278:                                    ; preds = %269
  %279 = load i32, i32* %32, align 4
  %280 = sext i32 %279 to i64
  %281 = load double*, double** %12, align 8
  %282 = getelementptr inbounds double, double* %281, i64 %280
  %283 = load double, double* %282, align 8
  %284 = load i32, i32* %31, align 4
  %285 = load i32, i32* %27, align 4
  %286 = mul nsw i32 %284, %285
  %287 = load i32, i32* %32, align 4
  %288 = sext i32 %287 to i64
  %289 = load i32*, i32** %14, align 8
  %290 = getelementptr inbounds i32, i32* %289, i64 %288
  %291 = load i32, i32* %290, align 4
  %292 = load i32, i32* %26, align 4
  %293 = mul nsw i32 %291, %292
  %294 = add nsw i32 %286, %293
  %295 = sext i32 %294 to i64
  %296 = load double*, double** %19, align 8
  %297 = getelementptr inbounds double, double* %296, i64 %295
  %298 = load double, double* %297, align 8
  %299 = fmul double %283, %298
  %300 = load double, double* %29, align 8
  %301 = fadd double %300, %299
  store double %301, double* %29, align 8
  br label %302

; <label>:302:                                    ; preds = %278
  %303 = load i32, i32* %32, align 4
  %304 = add nsw i32 %303, 1
  store i32 %304, i32* %32, align 4
  br label %269

; <label>:305:                                    ; preds = %269
  %306 = load double, double* %29, align 8
  %307 = load i32, i32* %31, align 4
  %308 = load i32, i32* %25, align 4
  %309 = mul nsw i32 %307, %308
  %310 = load i32, i32* %33, align 4
  %311 = load i32, i32* %24, align 4
  %312 = mul nsw i32 %310, %311
  %313 = add nsw i32 %309, %312
  %314 = sext i32 %313 to i64
  %315 = load double*, double** %20, align 8
  %316 = getelementptr inbounds double, double* %315, i64 %314
  store double %306, double* %316, align 8
  br label %317

; <label>:317:                                    ; preds = %305
  %318 = load i32, i32* %31, align 4
  %319 = add nsw i32 %318, 1
  store i32 %319, i32* %31, align 4
  br label %248

; <label>:320:                                    ; preds = %248
  br label %321

; <label>:321:                                    ; preds = %320, %241
  br label %322

; <label>:322:                                    ; preds = %321
  %323 = load i32, i32* %30, align 4
  %324 = add nsw i32 %323, 1
  store i32 %324, i32* %30, align 4
  br label %188

; <label>:325:                                    ; preds = %188
  br label %460

; <label>:326:                                    ; preds = %179
  store i32 0, i32* %30, align 4
  br label %327

; <label>:327:                                    ; preds = %456, %326
  %328 = load i32, i32* %30, align 4
  %329 = load i32, i32* %15, align 4
  %330 = icmp slt i32 %328, %329
  br i1 %330, label %331, label %459

; <label>:331:                                    ; preds = %327
  %332 = load i32, i32* %23, align 4
  %333 = icmp eq i32 %332, 1
  br i1 %333, label %334, label %381

; <label>:334:                                    ; preds = %331
  %335 = load i32, i32* %30, align 4
  %336 = sext i32 %335 to i64
  %337 = load double*, double** %20, align 8
  %338 = getelementptr inbounds double, double* %337, i64 %336
  %339 = load double, double* %338, align 8
  store double %339, double* %28, align 8
  %340 = load i32, i32* %30, align 4
  %341 = sext i32 %340 to i64
  %342 = load i32*, i32** %13, align 8
  %343 = getelementptr inbounds i32, i32* %342, i64 %341
  %344 = load i32, i32* %343, align 4
  store i32 %344, i32* %32, align 4
  br label %345

; <label>:345:                                    ; preds = %372, %334
  %346 = load i32, i32* %32, align 4
  %347 = load i32, i32* %30, align 4
  %348 = add nsw i32 %347, 1
  %349 = sext i32 %348 to i64
  %350 = load i32*, i32** %13, align 8
  %351 = getelementptr inbounds i32, i32* %350, i64 %349
  %352 = load i32, i32* %351, align 4
  %353 = icmp slt i32 %346, %352
  br i1 %353, label %354, label %375

; <label>:354:                                    ; preds = %345
  %355 = load i32, i32* %32, align 4
  %356 = sext i32 %355 to i64
  %357 = load double*, double** %12, align 8
  %358 = getelementptr inbounds double, double* %357, i64 %356
  %359 = load double, double* %358, align 8
  %360 = load i32, i32* %32, align 4
  %361 = sext i32 %360 to i64
  %362 = load i32*, i32** %14, align 8
  %363 = getelementptr inbounds i32, i32* %362, i64 %361
  %364 = load i32, i32* %363, align 4
  %365 = sext i32 %364 to i64
  %366 = load double*, double** %19, align 8
  %367 = getelementptr inbounds double, double* %366, i64 %365
  %368 = load double, double* %367, align 8
  %369 = fmul double %359, %368
  %370 = load double, double* %28, align 8
  %371 = fadd double %370, %369
  store double %371, double* %28, align 8
  br label %372

; <label>:372:                                    ; preds = %354
  %373 = load i32, i32* %32, align 4
  %374 = add nsw i32 %373, 1
  store i32 %374, i32* %32, align 4
  br label %345

; <label>:375:                                    ; preds = %345
  %376 = load double, double* %28, align 8
  %377 = load i32, i32* %30, align 4
  %378 = sext i32 %377 to i64
  %379 = load double*, double** %20, align 8
  %380 = getelementptr inbounds double, double* %379, i64 %378
  store double %376, double* %380, align 8
  br label %455

; <label>:381:                                    ; preds = %331
  store i32 0, i32* %31, align 4
  br label %382

; <label>:382:                                    ; preds = %451, %381
  %383 = load i32, i32* %31, align 4
  %384 = load i32, i32* %23, align 4
  %385 = icmp slt i32 %383, %384
  br i1 %385, label %386, label %454

; <label>:386:                                    ; preds = %382
  %387 = load i32, i32* %31, align 4
  %388 = load i32, i32* %25, align 4
  %389 = mul nsw i32 %387, %388
  %390 = load i32, i32* %30, align 4
  %391 = load i32, i32* %24, align 4
  %392 = mul nsw i32 %390, %391
  %393 = add nsw i32 %389, %392
  %394 = sext i32 %393 to i64
  %395 = load double*, double** %20, align 8
  %396 = getelementptr inbounds double, double* %395, i64 %394
  %397 = load double, double* %396, align 8
  store double %397, double* %28, align 8
  %398 = load i32, i32* %30, align 4
  %399 = sext i32 %398 to i64
  %400 = load i32*, i32** %13, align 8
  %401 = getelementptr inbounds i32, i32* %400, i64 %399
  %402 = load i32, i32* %401, align 4
  store i32 %402, i32* %32, align 4
  br label %403

; <label>:403:                                    ; preds = %436, %386
  %404 = load i32, i32* %32, align 4
  %405 = load i32, i32* %30, align 4
  %406 = add nsw i32 %405, 1
  %407 = sext i32 %406 to i64
  %408 = load i32*, i32** %13, align 8
  %409 = getelementptr inbounds i32, i32* %408, i64 %407
  %410 = load i32, i32* %409, align 4
  %411 = icmp slt i32 %404, %410
  br i1 %411, label %412, label %439

; <label>:412:                                    ; preds = %403
  %413 = load i32, i32* %32, align 4
  %414 = sext i32 %413 to i64
  %415 = load double*, double** %12, align 8
  %416 = getelementptr inbounds double, double* %415, i64 %414
  %417 = load double, double* %416, align 8
  %418 = load i32, i32* %31, align 4
  %419 = load i32, i32* %27, align 4
  %420 = mul nsw i32 %418, %419
  %421 = load i32, i32* %32, align 4
  %422 = sext i32 %421 to i64
  %423 = load i32*, i32** %14, align 8
  %424 = getelementptr inbounds i32, i32* %423, i64 %422
  %425 = load i32, i32* %424, align 4
  %426 = load i32, i32* %26, align 4
  %427 = mul nsw i32 %425, %426
  %428 = add nsw i32 %420, %427
  %429 = sext i32 %428 to i64
  %430 = load double*, double** %19, align 8
  %431 = getelementptr inbounds double, double* %430, i64 %429
  %432 = load double, double* %431, align 8
  %433 = fmul double %417, %432
  %434 = load double, double* %28, align 8
  %435 = fadd double %434, %433
  store double %435, double* %28, align 8
  br label %436

; <label>:436:                                    ; preds = %412
  %437 = load i32, i32* %32, align 4
  %438 = add nsw i32 %437, 1
  store i32 %438, i32* %32, align 4
  br label %403

; <label>:439:                                    ; preds = %403
  %440 = load double, double* %28, align 8
  %441 = load i32, i32* %31, align 4
  %442 = load i32, i32* %25, align 4
  %443 = mul nsw i32 %441, %442
  %444 = load i32, i32* %30, align 4
  %445 = load i32, i32* %24, align 4
  %446 = mul nsw i32 %444, %445
  %447 = add nsw i32 %443, %446
  %448 = sext i32 %447 to i64
  %449 = load double*, double** %20, align 8
  %450 = getelementptr inbounds double, double* %449, i64 %448
  store double %440, double* %450, align 8
  br label %451

; <label>:451:                                    ; preds = %439
  %452 = load i32, i32* %31, align 4
  %453 = add nsw i32 %452, 1
  store i32 %453, i32* %31, align 4
  br label %382

; <label>:454:                                    ; preds = %382
  br label %455

; <label>:455:                                    ; preds = %454, %375
  br label %456

; <label>:456:                                    ; preds = %455
  %457 = load i32, i32* %30, align 4
  %458 = add nsw i32 %457, 1
  store i32 %458, i32* %30, align 4
  br label %327

; <label>:459:                                    ; preds = %327
  br label %460

; <label>:460:                                    ; preds = %459, %325
  %461 = load double, double* %7, align 8
  %462 = fcmp une double %461, 1.000000e+00
  br i1 %462, label %463, label %482

; <label>:463:                                    ; preds = %460
  store i32 0, i32* %30, align 4
  br label %464

; <label>:464:                                    ; preds = %478, %463
  %465 = load i32, i32* %30, align 4
  %466 = load i32, i32* %15, align 4
  %467 = load i32, i32* %23, align 4
  %468 = mul nsw i32 %466, %467
  %469 = icmp slt i32 %465, %468
  br i1 %469, label %470, label %481

; <label>:470:                                    ; preds = %464
  %471 = load double, double* %7, align 8
  %472 = load i32, i32* %30, align 4
  %473 = sext i32 %472 to i64
  %474 = load double*, double** %20, align 8
  %475 = getelementptr inbounds double, double* %474, i64 %473
  %476 = load double, double* %475, align 8
  %477 = fmul double %476, %471
  store double %477, double* %475, align 8
  br label %478

; <label>:478:                                    ; preds = %470
  %479 = load i32, i32* %30, align 4
  %480 = add nsw i32 %479, 1
  store i32 %480, i32* %30, align 4
  br label %464

; <label>:481:                                    ; preds = %464
  br label %482

; <label>:482:                                    ; preds = %481, %460
  %483 = load i32, i32* %35, align 4
  store i32 %483, i32* %6, align 4
  br label %484

; <label>:484:                                    ; preds = %482, %132
  %485 = load i32, i32* %6, align 4
  ret i32 %485
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

declare void @hypre_error_handler(i8*, i32, i32) #1

; Function Attrs: nounwind uwtable
define i32 @hypre_CSRMatrixMatvecT(double, %struct.hypre_CSRMatrix*, %struct.hypre_Vector*, double, %struct.hypre_Vector*) #0 {
  %6 = alloca i32, align 4
  %7 = alloca double, align 8
  %8 = alloca %struct.hypre_CSRMatrix*, align 8
  %9 = alloca %struct.hypre_Vector*, align 8
  %10 = alloca double, align 8
  %11 = alloca %struct.hypre_Vector*, align 8
  %12 = alloca double*, align 8
  %13 = alloca i32*, align 8
  %14 = alloca i32*, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca double*, align 8
  %18 = alloca double*, align 8
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca double, align 8
  %27 = alloca i32, align 4
  %28 = alloca i32, align 4
  %29 = alloca i32, align 4
  %30 = alloca i32, align 4
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  store double %0, double* %7, align 8
  store %struct.hypre_CSRMatrix* %1, %struct.hypre_CSRMatrix** %8, align 8
  store %struct.hypre_Vector* %2, %struct.hypre_Vector** %9, align 8
  store double %3, double* %10, align 8
  store %struct.hypre_Vector* %4, %struct.hypre_Vector** %11, align 8
  %38 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %39 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %38, i32 0, i32 0
  %40 = load double*, double** %39, align 8
  store double* %40, double** %12, align 8
  %41 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %42 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %41, i32 0, i32 1
  %43 = load i32*, i32** %42, align 8
  store i32* %43, i32** %13, align 8
  %44 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %45 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %44, i32 0, i32 2
  %46 = load i32*, i32** %45, align 8
  store i32* %46, i32** %14, align 8
  %47 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %48 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %47, i32 0, i32 3
  %49 = load i32, i32* %48, align 8
  store i32 %49, i32* %15, align 4
  %50 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %8, align 8
  %51 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %50, i32 0, i32 4
  %52 = load i32, i32* %51, align 4
  store i32 %52, i32* %16, align 4
  %53 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %54 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %53, i32 0, i32 0
  %55 = load double*, double** %54, align 8
  store double* %55, double** %17, align 8
  %56 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %57 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %56, i32 0, i32 0
  %58 = load double*, double** %57, align 8
  store double* %58, double** %18, align 8
  %59 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %60 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %59, i32 0, i32 1
  %61 = load i32, i32* %60, align 8
  store i32 %61, i32* %19, align 4
  %62 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %63 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %62, i32 0, i32 1
  %64 = load i32, i32* %63, align 8
  store i32 %64, i32* %20, align 4
  %65 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %66 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %65, i32 0, i32 3
  %67 = load i32, i32* %66, align 8
  store i32 %67, i32* %21, align 4
  %68 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %69 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %68, i32 0, i32 6
  %70 = load i32, i32* %69, align 4
  store i32 %70, i32* %22, align 4
  %71 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %72 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %71, i32 0, i32 5
  %73 = load i32, i32* %72, align 8
  store i32 %73, i32* %23, align 4
  %74 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %75 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %74, i32 0, i32 6
  %76 = load i32, i32* %75, align 4
  store i32 %76, i32* %24, align 4
  %77 = load %struct.hypre_Vector*, %struct.hypre_Vector** %9, align 8
  %78 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %77, i32 0, i32 5
  %79 = load i32, i32* %78, align 8
  store i32 %79, i32* %25, align 4
  store i32 0, i32* %37, align 4
  %80 = load i32, i32* %21, align 4
  %81 = load %struct.hypre_Vector*, %struct.hypre_Vector** %11, align 8
  %82 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %81, i32 0, i32 3
  %83 = load i32, i32* %82, align 8
  %84 = icmp eq i32 %80, %83
  br i1 %84, label %88, label %85

; <label>:85:                                     ; preds = %5
  %86 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %87 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %86, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.1, i32 0, i32 0))
  call void @hypre_error_handler(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.2, i32 0, i32 0), i32 258, i32 1)
  br label %88

; <label>:88:                                     ; preds = %85, %5
  %89 = load i32, i32* %15, align 4
  %90 = load i32, i32* %19, align 4
  %91 = icmp ne i32 %89, %90
  br i1 %91, label %92, label %93

; <label>:92:                                     ; preds = %88
  store i32 1, i32* %37, align 4
  br label %93

; <label>:93:                                     ; preds = %92, %88
  %94 = load i32, i32* %16, align 4
  %95 = load i32, i32* %20, align 4
  %96 = icmp ne i32 %94, %95
  br i1 %96, label %97, label %98

; <label>:97:                                     ; preds = %93
  store i32 2, i32* %37, align 4
  br label %98

; <label>:98:                                     ; preds = %97, %93
  %99 = load i32, i32* %15, align 4
  %100 = load i32, i32* %19, align 4
  %101 = icmp ne i32 %99, %100
  br i1 %101, label %102, label %107

; <label>:102:                                    ; preds = %98
  %103 = load i32, i32* %16, align 4
  %104 = load i32, i32* %20, align 4
  %105 = icmp ne i32 %103, %104
  br i1 %105, label %106, label %107

; <label>:106:                                    ; preds = %102
  store i32 3, i32* %37, align 4
  br label %107

; <label>:107:                                    ; preds = %106, %102, %98
  %108 = load double, double* %7, align 8
  %109 = fcmp oeq double %108, 0.000000e+00
  br i1 %109, label %110, label %130

; <label>:110:                                    ; preds = %107
  store i32 0, i32* %27, align 4
  br label %111

; <label>:111:                                    ; preds = %125, %110
  %112 = load i32, i32* %27, align 4
  %113 = load i32, i32* %16, align 4
  %114 = load i32, i32* %21, align 4
  %115 = mul nsw i32 %113, %114
  %116 = icmp slt i32 %112, %115
  br i1 %116, label %117, label %128

; <label>:117:                                    ; preds = %111
  %118 = load double, double* %10, align 8
  %119 = load i32, i32* %27, align 4
  %120 = sext i32 %119 to i64
  %121 = load double*, double** %18, align 8
  %122 = getelementptr inbounds double, double* %121, i64 %120
  %123 = load double, double* %122, align 8
  %124 = fmul double %123, %118
  store double %124, double* %122, align 8
  br label %125

; <label>:125:                                    ; preds = %117
  %126 = load i32, i32* %27, align 4
  %127 = add nsw i32 %126, 1
  store i32 %127, i32* %27, align 4
  br label %111

; <label>:128:                                    ; preds = %111
  %129 = load i32, i32* %37, align 4
  store i32 %129, i32* %6, align 4
  br label %513

; <label>:130:                                    ; preds = %107
  %131 = load double, double* %10, align 8
  %132 = load double, double* %7, align 8
  %133 = fdiv double %131, %132
  store double %133, double* %26, align 8
  %134 = load double, double* %26, align 8
  %135 = fcmp une double %134, 1.000000e+00
  br i1 %135, label %136, label %175

; <label>:136:                                    ; preds = %130
  %137 = load double, double* %26, align 8
  %138 = fcmp oeq double %137, 0.000000e+00
  br i1 %138, label %139, label %155

; <label>:139:                                    ; preds = %136
  store i32 0, i32* %27, align 4
  br label %140

; <label>:140:                                    ; preds = %151, %139
  %141 = load i32, i32* %27, align 4
  %142 = load i32, i32* %16, align 4
  %143 = load i32, i32* %21, align 4
  %144 = mul nsw i32 %142, %143
  %145 = icmp slt i32 %141, %144
  br i1 %145, label %146, label %154

; <label>:146:                                    ; preds = %140
  %147 = load i32, i32* %27, align 4
  %148 = sext i32 %147 to i64
  %149 = load double*, double** %18, align 8
  %150 = getelementptr inbounds double, double* %149, i64 %148
  store double 0.000000e+00, double* %150, align 8
  br label %151

; <label>:151:                                    ; preds = %146
  %152 = load i32, i32* %27, align 4
  %153 = add nsw i32 %152, 1
  store i32 %153, i32* %27, align 4
  br label %140

; <label>:154:                                    ; preds = %140
  br label %174

; <label>:155:                                    ; preds = %136
  store i32 0, i32* %27, align 4
  br label %156

; <label>:156:                                    ; preds = %170, %155
  %157 = load i32, i32* %27, align 4
  %158 = load i32, i32* %16, align 4
  %159 = load i32, i32* %21, align 4
  %160 = mul nsw i32 %158, %159
  %161 = icmp slt i32 %157, %160
  br i1 %161, label %162, label %173

; <label>:162:                                    ; preds = %156
  %163 = load double, double* %26, align 8
  %164 = load i32, i32* %27, align 4
  %165 = sext i32 %164 to i64
  %166 = load double*, double** %18, align 8
  %167 = getelementptr inbounds double, double* %166, i64 %165
  %168 = load double, double* %167, align 8
  %169 = fmul double %168, %163
  store double %169, double* %167, align 8
  br label %170

; <label>:170:                                    ; preds = %162
  %171 = load i32, i32* %27, align 4
  %172 = add nsw i32 %171, 1
  store i32 %172, i32* %27, align 4
  br label %156

; <label>:173:                                    ; preds = %156
  br label %174

; <label>:174:                                    ; preds = %173, %154
  br label %175

; <label>:175:                                    ; preds = %174, %130
  store i32 1, i32* %36, align 4
  %176 = load i32, i32* %36, align 4
  %177 = icmp sgt i32 %176, 1
  br i1 %177, label %178, label %371

; <label>:178:                                    ; preds = %175
  store i32 0, i32* %28, align 4
  br label %179

; <label>:179:                                    ; preds = %367, %178
  %180 = load i32, i32* %28, align 4
  %181 = load i32, i32* %36, align 4
  %182 = icmp slt i32 %180, %181
  br i1 %182, label %183, label %370

; <label>:183:                                    ; preds = %179
  %184 = load i32, i32* %16, align 4
  %185 = load i32, i32* %36, align 4
  %186 = sdiv i32 %184, %185
  store i32 %186, i32* %34, align 4
  %187 = load i32, i32* %16, align 4
  %188 = load i32, i32* %34, align 4
  %189 = load i32, i32* %36, align 4
  %190 = mul nsw i32 %188, %189
  %191 = sub nsw i32 %187, %190
  store i32 %191, i32* %35, align 4
  %192 = load i32, i32* %28, align 4
  %193 = load i32, i32* %35, align 4
  %194 = icmp slt i32 %192, %193
  br i1 %194, label %195, label %209

; <label>:195:                                    ; preds = %183
  %196 = load i32, i32* %28, align 4
  %197 = load i32, i32* %34, align 4
  %198 = mul nsw i32 %196, %197
  %199 = load i32, i32* %28, align 4
  %200 = add nsw i32 %198, %199
  %201 = sub nsw i32 %200, 1
  store i32 %201, i32* %32, align 4
  %202 = load i32, i32* %28, align 4
  %203 = add nsw i32 %202, 1
  %204 = load i32, i32* %34, align 4
  %205 = mul nsw i32 %203, %204
  %206 = load i32, i32* %28, align 4
  %207 = add nsw i32 %205, %206
  %208 = add nsw i32 %207, 1
  store i32 %208, i32* %33, align 4
  br label %222

; <label>:209:                                    ; preds = %183
  %210 = load i32, i32* %28, align 4
  %211 = load i32, i32* %34, align 4
  %212 = mul nsw i32 %210, %211
  %213 = load i32, i32* %35, align 4
  %214 = add nsw i32 %212, %213
  %215 = sub nsw i32 %214, 1
  store i32 %215, i32* %32, align 4
  %216 = load i32, i32* %28, align 4
  %217 = add nsw i32 %216, 1
  %218 = load i32, i32* %34, align 4
  %219 = mul nsw i32 %217, %218
  %220 = load i32, i32* %35, align 4
  %221 = add nsw i32 %219, %220
  store i32 %221, i32* %33, align 4
  br label %222

; <label>:222:                                    ; preds = %209, %195
  %223 = load i32, i32* %21, align 4
  %224 = icmp eq i32 %223, 1
  br i1 %224, label %225, label %285

; <label>:225:                                    ; preds = %222
  store i32 0, i32* %27, align 4
  br label %226

; <label>:226:                                    ; preds = %281, %225
  %227 = load i32, i32* %27, align 4
  %228 = load i32, i32* %15, align 4
  %229 = icmp slt i32 %227, %228
  br i1 %229, label %230, label %284

; <label>:230:                                    ; preds = %226
  %231 = load i32, i32* %27, align 4
  %232 = sext i32 %231 to i64
  %233 = load i32*, i32** %13, align 8
  %234 = getelementptr inbounds i32, i32* %233, i64 %232
  %235 = load i32, i32* %234, align 4
  store i32 %235, i32* %31, align 4
  br label %236

; <label>:236:                                    ; preds = %277, %230
  %237 = load i32, i32* %31, align 4
  %238 = load i32, i32* %27, align 4
  %239 = add nsw i32 %238, 1
  %240 = sext i32 %239 to i64
  %241 = load i32*, i32** %13, align 8
  %242 = getelementptr inbounds i32, i32* %241, i64 %240
  %243 = load i32, i32* %242, align 4
  %244 = icmp slt i32 %237, %243
  br i1 %244, label %245, label %280

; <label>:245:                                    ; preds = %236
  %246 = load i32, i32* %31, align 4
  %247 = sext i32 %246 to i64
  %248 = load i32*, i32** %14, align 8
  %249 = getelementptr inbounds i32, i32* %248, i64 %247
  %250 = load i32, i32* %249, align 4
  store i32 %250, i32* %29, align 4
  %251 = load i32, i32* %29, align 4
  %252 = load i32, i32* %32, align 4
  %253 = icmp sgt i32 %251, %252
  br i1 %253, label %254, label %276

; <label>:254:                                    ; preds = %245
  %255 = load i32, i32* %29, align 4
  %256 = load i32, i32* %33, align 4
  %257 = icmp slt i32 %255, %256
  br i1 %257, label %258, label %276

; <label>:258:                                    ; preds = %254
  %259 = load i32, i32* %31, align 4
  %260 = sext i32 %259 to i64
  %261 = load double*, double** %12, align 8
  %262 = getelementptr inbounds double, double* %261, i64 %260
  %263 = load double, double* %262, align 8
  %264 = load i32, i32* %27, align 4
  %265 = sext i32 %264 to i64
  %266 = load double*, double** %17, align 8
  %267 = getelementptr inbounds double, double* %266, i64 %265
  %268 = load double, double* %267, align 8
  %269 = fmul double %263, %268
  %270 = load i32, i32* %29, align 4
  %271 = sext i32 %270 to i64
  %272 = load double*, double** %18, align 8
  %273 = getelementptr inbounds double, double* %272, i64 %271
  %274 = load double, double* %273, align 8
  %275 = fadd double %274, %269
  store double %275, double* %273, align 8
  br label %276

; <label>:276:                                    ; preds = %258, %254, %245
  br label %277

; <label>:277:                                    ; preds = %276
  %278 = load i32, i32* %31, align 4
  %279 = add nsw i32 %278, 1
  store i32 %279, i32* %31, align 4
  br label %236

; <label>:280:                                    ; preds = %236
  br label %281

; <label>:281:                                    ; preds = %280
  %282 = load i32, i32* %27, align 4
  %283 = add nsw i32 %282, 1
  store i32 %283, i32* %27, align 4
  br label %226

; <label>:284:                                    ; preds = %226
  br label %366

; <label>:285:                                    ; preds = %222
  store i32 0, i32* %27, align 4
  br label %286

; <label>:286:                                    ; preds = %362, %285
  %287 = load i32, i32* %27, align 4
  %288 = load i32, i32* %15, align 4
  %289 = icmp slt i32 %287, %288
  br i1 %289, label %290, label %365

; <label>:290:                                    ; preds = %286
  store i32 0, i32* %30, align 4
  br label %291

; <label>:291:                                    ; preds = %358, %290
  %292 = load i32, i32* %30, align 4
  %293 = load i32, i32* %21, align 4
  %294 = icmp slt i32 %292, %293
  br i1 %294, label %295, label %361

; <label>:295:                                    ; preds = %291
  %296 = load i32, i32* %27, align 4
  %297 = sext i32 %296 to i64
  %298 = load i32*, i32** %13, align 8
  %299 = getelementptr inbounds i32, i32* %298, i64 %297
  %300 = load i32, i32* %299, align 4
  store i32 %300, i32* %31, align 4
  br label %301

; <label>:301:                                    ; preds = %354, %295
  %302 = load i32, i32* %31, align 4
  %303 = load i32, i32* %27, align 4
  %304 = add nsw i32 %303, 1
  %305 = sext i32 %304 to i64
  %306 = load i32*, i32** %13, align 8
  %307 = getelementptr inbounds i32, i32* %306, i64 %305
  %308 = load i32, i32* %307, align 4
  %309 = icmp slt i32 %302, %308
  br i1 %309, label %310, label %357

; <label>:310:                                    ; preds = %301
  %311 = load i32, i32* %31, align 4
  %312 = sext i32 %311 to i64
  %313 = load i32*, i32** %14, align 8
  %314 = getelementptr inbounds i32, i32* %313, i64 %312
  %315 = load i32, i32* %314, align 4
  store i32 %315, i32* %29, align 4
  %316 = load i32, i32* %29, align 4
  %317 = load i32, i32* %32, align 4
  %318 = icmp sgt i32 %316, %317
  br i1 %318, label %319, label %353

; <label>:319:                                    ; preds = %310
  %320 = load i32, i32* %29, align 4
  %321 = load i32, i32* %33, align 4
  %322 = icmp slt i32 %320, %321
  br i1 %322, label %323, label %353

; <label>:323:                                    ; preds = %319
  %324 = load i32, i32* %31, align 4
  %325 = sext i32 %324 to i64
  %326 = load double*, double** %12, align 8
  %327 = getelementptr inbounds double, double* %326, i64 %325
  %328 = load double, double* %327, align 8
  %329 = load i32, i32* %27, align 4
  %330 = load i32, i32* %24, align 4
  %331 = mul nsw i32 %329, %330
  %332 = load i32, i32* %30, align 4
  %333 = load i32, i32* %25, align 4
  %334 = mul nsw i32 %332, %333
  %335 = add nsw i32 %331, %334
  %336 = sext i32 %335 to i64
  %337 = load double*, double** %17, align 8
  %338 = getelementptr inbounds double, double* %337, i64 %336
  %339 = load double, double* %338, align 8
  %340 = fmul double %328, %339
  %341 = load i32, i32* %29, align 4
  %342 = load i32, i32* %22, align 4
  %343 = mul nsw i32 %341, %342
  %344 = load i32, i32* %30, align 4
  %345 = load i32, i32* %23, align 4
  %346 = mul nsw i32 %344, %345
  %347 = add nsw i32 %343, %346
  %348 = sext i32 %347 to i64
  %349 = load double*, double** %18, align 8
  %350 = getelementptr inbounds double, double* %349, i64 %348
  %351 = load double, double* %350, align 8
  %352 = fadd double %351, %340
  store double %352, double* %350, align 8
  br label %353

; <label>:353:                                    ; preds = %323, %319, %310
  br label %354

; <label>:354:                                    ; preds = %353
  %355 = load i32, i32* %31, align 4
  %356 = add nsw i32 %355, 1
  store i32 %356, i32* %31, align 4
  br label %301

; <label>:357:                                    ; preds = %301
  br label %358

; <label>:358:                                    ; preds = %357
  %359 = load i32, i32* %30, align 4
  %360 = add nsw i32 %359, 1
  store i32 %360, i32* %30, align 4
  br label %291

; <label>:361:                                    ; preds = %291
  br label %362

; <label>:362:                                    ; preds = %361
  %363 = load i32, i32* %27, align 4
  %364 = add nsw i32 %363, 1
  store i32 %364, i32* %27, align 4
  br label %286

; <label>:365:                                    ; preds = %286
  br label %366

; <label>:366:                                    ; preds = %365, %284
  br label %367

; <label>:367:                                    ; preds = %366
  %368 = load i32, i32* %28, align 4
  %369 = add nsw i32 %368, 1
  store i32 %369, i32* %28, align 4
  br label %179

; <label>:370:                                    ; preds = %179
  br label %489

; <label>:371:                                    ; preds = %175
  store i32 0, i32* %27, align 4
  br label %372

; <label>:372:                                    ; preds = %485, %371
  %373 = load i32, i32* %27, align 4
  %374 = load i32, i32* %15, align 4
  %375 = icmp slt i32 %373, %374
  br i1 %375, label %376, label %488

; <label>:376:                                    ; preds = %372
  %377 = load i32, i32* %21, align 4
  %378 = icmp eq i32 %377, 1
  br i1 %378, label %379, label %421

; <label>:379:                                    ; preds = %376
  %380 = load i32, i32* %27, align 4
  %381 = sext i32 %380 to i64
  %382 = load i32*, i32** %13, align 8
  %383 = getelementptr inbounds i32, i32* %382, i64 %381
  %384 = load i32, i32* %383, align 4
  store i32 %384, i32* %31, align 4
  br label %385

; <label>:385:                                    ; preds = %417, %379
  %386 = load i32, i32* %31, align 4
  %387 = load i32, i32* %27, align 4
  %388 = add nsw i32 %387, 1
  %389 = sext i32 %388 to i64
  %390 = load i32*, i32** %13, align 8
  %391 = getelementptr inbounds i32, i32* %390, i64 %389
  %392 = load i32, i32* %391, align 4
  %393 = icmp slt i32 %386, %392
  br i1 %393, label %394, label %420

; <label>:394:                                    ; preds = %385
  %395 = load i32, i32* %31, align 4
  %396 = sext i32 %395 to i64
  %397 = load i32*, i32** %14, align 8
  %398 = getelementptr inbounds i32, i32* %397, i64 %396
  %399 = load i32, i32* %398, align 4
  store i32 %399, i32* %29, align 4
  %400 = load i32, i32* %31, align 4
  %401 = sext i32 %400 to i64
  %402 = load double*, double** %12, align 8
  %403 = getelementptr inbounds double, double* %402, i64 %401
  %404 = load double, double* %403, align 8
  %405 = load i32, i32* %27, align 4
  %406 = sext i32 %405 to i64
  %407 = load double*, double** %17, align 8
  %408 = getelementptr inbounds double, double* %407, i64 %406
  %409 = load double, double* %408, align 8
  %410 = fmul double %404, %409
  %411 = load i32, i32* %29, align 4
  %412 = sext i32 %411 to i64
  %413 = load double*, double** %18, align 8
  %414 = getelementptr inbounds double, double* %413, i64 %412
  %415 = load double, double* %414, align 8
  %416 = fadd double %415, %410
  store double %416, double* %414, align 8
  br label %417

; <label>:417:                                    ; preds = %394
  %418 = load i32, i32* %31, align 4
  %419 = add nsw i32 %418, 1
  store i32 %419, i32* %31, align 4
  br label %385

; <label>:420:                                    ; preds = %385
  br label %484

; <label>:421:                                    ; preds = %376
  store i32 0, i32* %30, align 4
  br label %422

; <label>:422:                                    ; preds = %480, %421
  %423 = load i32, i32* %30, align 4
  %424 = load i32, i32* %21, align 4
  %425 = icmp slt i32 %423, %424
  br i1 %425, label %426, label %483

; <label>:426:                                    ; preds = %422
  %427 = load i32, i32* %27, align 4
  %428 = sext i32 %427 to i64
  %429 = load i32*, i32** %13, align 8
  %430 = getelementptr inbounds i32, i32* %429, i64 %428
  %431 = load i32, i32* %430, align 4
  store i32 %431, i32* %31, align 4
  br label %432

; <label>:432:                                    ; preds = %476, %426
  %433 = load i32, i32* %31, align 4
  %434 = load i32, i32* %27, align 4
  %435 = add nsw i32 %434, 1
  %436 = sext i32 %435 to i64
  %437 = load i32*, i32** %13, align 8
  %438 = getelementptr inbounds i32, i32* %437, i64 %436
  %439 = load i32, i32* %438, align 4
  %440 = icmp slt i32 %433, %439
  br i1 %440, label %441, label %479

; <label>:441:                                    ; preds = %432
  %442 = load i32, i32* %31, align 4
  %443 = sext i32 %442 to i64
  %444 = load i32*, i32** %14, align 8
  %445 = getelementptr inbounds i32, i32* %444, i64 %443
  %446 = load i32, i32* %445, align 4
  store i32 %446, i32* %29, align 4
  %447 = load i32, i32* %31, align 4
  %448 = sext i32 %447 to i64
  %449 = load double*, double** %12, align 8
  %450 = getelementptr inbounds double, double* %449, i64 %448
  %451 = load double, double* %450, align 8
  %452 = load i32, i32* %27, align 4
  %453 = load i32, i32* %24, align 4
  %454 = mul nsw i32 %452, %453
  %455 = load i32, i32* %30, align 4
  %456 = load i32, i32* %25, align 4
  %457 = mul nsw i32 %455, %456
  %458 = add nsw i32 %454, %457
  %459 = sext i32 %458 to i64
  %460 = load double*, double** %17, align 8
  %461 = getelementptr inbounds double, double* %460, i64 %459
  %462 = load double, double* %461, align 8
  %463 = fmul double %451, %462
  %464 = load i32, i32* %29, align 4
  %465 = load i32, i32* %22, align 4
  %466 = mul nsw i32 %464, %465
  %467 = load i32, i32* %30, align 4
  %468 = load i32, i32* %23, align 4
  %469 = mul nsw i32 %467, %468
  %470 = add nsw i32 %466, %469
  %471 = sext i32 %470 to i64
  %472 = load double*, double** %18, align 8
  %473 = getelementptr inbounds double, double* %472, i64 %471
  %474 = load double, double* %473, align 8
  %475 = fadd double %474, %463
  store double %475, double* %473, align 8
  br label %476

; <label>:476:                                    ; preds = %441
  %477 = load i32, i32* %31, align 4
  %478 = add nsw i32 %477, 1
  store i32 %478, i32* %31, align 4
  br label %432

; <label>:479:                                    ; preds = %432
  br label %480

; <label>:480:                                    ; preds = %479
  %481 = load i32, i32* %30, align 4
  %482 = add nsw i32 %481, 1
  store i32 %482, i32* %30, align 4
  br label %422

; <label>:483:                                    ; preds = %422
  br label %484

; <label>:484:                                    ; preds = %483, %420
  br label %485

; <label>:485:                                    ; preds = %484
  %486 = load i32, i32* %27, align 4
  %487 = add nsw i32 %486, 1
  store i32 %487, i32* %27, align 4
  br label %372

; <label>:488:                                    ; preds = %372
  br label %489

; <label>:489:                                    ; preds = %488, %370
  %490 = load double, double* %7, align 8
  %491 = fcmp une double %490, 1.000000e+00
  br i1 %491, label %492, label %511

; <label>:492:                                    ; preds = %489
  store i32 0, i32* %27, align 4
  br label %493

; <label>:493:                                    ; preds = %507, %492
  %494 = load i32, i32* %27, align 4
  %495 = load i32, i32* %16, align 4
  %496 = load i32, i32* %21, align 4
  %497 = mul nsw i32 %495, %496
  %498 = icmp slt i32 %494, %497
  br i1 %498, label %499, label %510

; <label>:499:                                    ; preds = %493
  %500 = load double, double* %7, align 8
  %501 = load i32, i32* %27, align 4
  %502 = sext i32 %501 to i64
  %503 = load double*, double** %18, align 8
  %504 = getelementptr inbounds double, double* %503, i64 %502
  %505 = load double, double* %504, align 8
  %506 = fmul double %505, %500
  store double %506, double* %504, align 8
  br label %507

; <label>:507:                                    ; preds = %499
  %508 = load i32, i32* %27, align 4
  %509 = add nsw i32 %508, 1
  store i32 %509, i32* %27, align 4
  br label %493

; <label>:510:                                    ; preds = %493
  br label %511

; <label>:511:                                    ; preds = %510, %489
  %512 = load i32, i32* %37, align 4
  store i32 %512, i32* %6, align 4
  br label %513

; <label>:513:                                    ; preds = %511, %128
  %514 = load i32, i32* %6, align 4
  ret i32 %514
}

; Function Attrs: nounwind uwtable
define i32 @hypre_CSRMatrixMatvec_FF(double, %struct.hypre_CSRMatrix*, %struct.hypre_Vector*, double, %struct.hypre_Vector*, i32*, i32*, i32) #0 {
  %9 = alloca i32, align 4
  %10 = alloca double, align 8
  %11 = alloca %struct.hypre_CSRMatrix*, align 8
  %12 = alloca %struct.hypre_Vector*, align 8
  %13 = alloca double, align 8
  %14 = alloca %struct.hypre_Vector*, align 8
  %15 = alloca i32*, align 8
  %16 = alloca i32*, align 8
  %17 = alloca i32, align 4
  %18 = alloca double*, align 8
  %19 = alloca i32*, align 8
  %20 = alloca i32*, align 8
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca double*, align 8
  %24 = alloca double*, align 8
  %25 = alloca i32, align 4
  %26 = alloca i32, align 4
  %27 = alloca double, align 8
  %28 = alloca i32, align 4
  %29 = alloca i32, align 4
  %30 = alloca i32, align 4
  store double %0, double* %10, align 8
  store %struct.hypre_CSRMatrix* %1, %struct.hypre_CSRMatrix** %11, align 8
  store %struct.hypre_Vector* %2, %struct.hypre_Vector** %12, align 8
  store double %3, double* %13, align 8
  store %struct.hypre_Vector* %4, %struct.hypre_Vector** %14, align 8
  store i32* %5, i32** %15, align 8
  store i32* %6, i32** %16, align 8
  store i32 %7, i32* %17, align 4
  %31 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %11, align 8
  %32 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %31, i32 0, i32 0
  %33 = load double*, double** %32, align 8
  store double* %33, double** %18, align 8
  %34 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %11, align 8
  %35 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %34, i32 0, i32 1
  %36 = load i32*, i32** %35, align 8
  store i32* %36, i32** %19, align 8
  %37 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %11, align 8
  %38 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %37, i32 0, i32 2
  %39 = load i32*, i32** %38, align 8
  store i32* %39, i32** %20, align 8
  %40 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %11, align 8
  %41 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %40, i32 0, i32 3
  %42 = load i32, i32* %41, align 8
  store i32 %42, i32* %21, align 4
  %43 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %11, align 8
  %44 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %43, i32 0, i32 4
  %45 = load i32, i32* %44, align 4
  store i32 %45, i32* %22, align 4
  %46 = load %struct.hypre_Vector*, %struct.hypre_Vector** %12, align 8
  %47 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %46, i32 0, i32 0
  %48 = load double*, double** %47, align 8
  store double* %48, double** %23, align 8
  %49 = load %struct.hypre_Vector*, %struct.hypre_Vector** %14, align 8
  %50 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %49, i32 0, i32 0
  %51 = load double*, double** %50, align 8
  store double* %51, double** %24, align 8
  %52 = load %struct.hypre_Vector*, %struct.hypre_Vector** %12, align 8
  %53 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %52, i32 0, i32 1
  %54 = load i32, i32* %53, align 8
  store i32 %54, i32* %25, align 4
  %55 = load %struct.hypre_Vector*, %struct.hypre_Vector** %14, align 8
  %56 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %55, i32 0, i32 1
  %57 = load i32, i32* %56, align 8
  store i32 %57, i32* %26, align 4
  store i32 0, i32* %30, align 4
  %58 = load i32, i32* %22, align 4
  %59 = load i32, i32* %25, align 4
  %60 = icmp ne i32 %58, %59
  br i1 %60, label %61, label %62

; <label>:61:                                     ; preds = %8
  store i32 1, i32* %30, align 4
  br label %62

; <label>:62:                                     ; preds = %61, %8
  %63 = load i32, i32* %21, align 4
  %64 = load i32, i32* %26, align 4
  %65 = icmp ne i32 %63, %64
  br i1 %65, label %66, label %67

; <label>:66:                                     ; preds = %62
  store i32 2, i32* %30, align 4
  br label %67

; <label>:67:                                     ; preds = %66, %62
  %68 = load i32, i32* %22, align 4
  %69 = load i32, i32* %25, align 4
  %70 = icmp ne i32 %68, %69
  br i1 %70, label %71, label %76

; <label>:71:                                     ; preds = %67
  %72 = load i32, i32* %21, align 4
  %73 = load i32, i32* %26, align 4
  %74 = icmp ne i32 %72, %73
  br i1 %74, label %75, label %76

; <label>:75:                                     ; preds = %71
  store i32 3, i32* %30, align 4
  br label %76

; <label>:76:                                     ; preds = %75, %71, %67
  %77 = load double, double* %10, align 8
  %78 = fcmp oeq double %77, 0.000000e+00
  br i1 %78, label %79, label %106

; <label>:79:                                     ; preds = %76
  store i32 0, i32* %28, align 4
  br label %80

; <label>:80:                                     ; preds = %101, %79
  %81 = load i32, i32* %28, align 4
  %82 = load i32, i32* %21, align 4
  %83 = icmp slt i32 %81, %82
  br i1 %83, label %84, label %104

; <label>:84:                                     ; preds = %80
  %85 = load i32, i32* %28, align 4
  %86 = sext i32 %85 to i64
  %87 = load i32*, i32** %15, align 8
  %88 = getelementptr inbounds i32, i32* %87, i64 %86
  %89 = load i32, i32* %88, align 4
  %90 = load i32, i32* %17, align 4
  %91 = icmp eq i32 %89, %90
  br i1 %91, label %92, label %100

; <label>:92:                                     ; preds = %84
  %93 = load double, double* %13, align 8
  %94 = load i32, i32* %28, align 4
  %95 = sext i32 %94 to i64
  %96 = load double*, double** %24, align 8
  %97 = getelementptr inbounds double, double* %96, i64 %95
  %98 = load double, double* %97, align 8
  %99 = fmul double %98, %93
  store double %99, double* %97, align 8
  br label %100

; <label>:100:                                    ; preds = %92, %84
  br label %101

; <label>:101:                                    ; preds = %100
  %102 = load i32, i32* %28, align 4
  %103 = add nsw i32 %102, 1
  store i32 %103, i32* %28, align 4
  br label %80

; <label>:104:                                    ; preds = %80
  %105 = load i32, i32* %30, align 4
  store i32 %105, i32* %9, align 4
  br label %273

; <label>:106:                                    ; preds = %76
  %107 = load double, double* %13, align 8
  %108 = load double, double* %10, align 8
  %109 = fdiv double %107, %108
  store double %109, double* %27, align 8
  %110 = load double, double* %27, align 8
  %111 = fcmp une double %110, 1.000000e+00
  br i1 %111, label %112, label %165

; <label>:112:                                    ; preds = %106
  %113 = load double, double* %27, align 8
  %114 = fcmp oeq double %113, 0.000000e+00
  br i1 %114, label %115, label %138

; <label>:115:                                    ; preds = %112
  store i32 0, i32* %28, align 4
  br label %116

; <label>:116:                                    ; preds = %134, %115
  %117 = load i32, i32* %28, align 4
  %118 = load i32, i32* %21, align 4
  %119 = icmp slt i32 %117, %118
  br i1 %119, label %120, label %137

; <label>:120:                                    ; preds = %116
  %121 = load i32, i32* %28, align 4
  %122 = sext i32 %121 to i64
  %123 = load i32*, i32** %15, align 8
  %124 = getelementptr inbounds i32, i32* %123, i64 %122
  %125 = load i32, i32* %124, align 4
  %126 = load i32, i32* %17, align 4
  %127 = icmp eq i32 %125, %126
  br i1 %127, label %128, label %133

; <label>:128:                                    ; preds = %120
  %129 = load i32, i32* %28, align 4
  %130 = sext i32 %129 to i64
  %131 = load double*, double** %24, align 8
  %132 = getelementptr inbounds double, double* %131, i64 %130
  store double 0.000000e+00, double* %132, align 8
  br label %133

; <label>:133:                                    ; preds = %128, %120
  br label %134

; <label>:134:                                    ; preds = %133
  %135 = load i32, i32* %28, align 4
  %136 = add nsw i32 %135, 1
  store i32 %136, i32* %28, align 4
  br label %116

; <label>:137:                                    ; preds = %116
  br label %164

; <label>:138:                                    ; preds = %112
  store i32 0, i32* %28, align 4
  br label %139

; <label>:139:                                    ; preds = %160, %138
  %140 = load i32, i32* %28, align 4
  %141 = load i32, i32* %21, align 4
  %142 = icmp slt i32 %140, %141
  br i1 %142, label %143, label %163

; <label>:143:                                    ; preds = %139
  %144 = load i32, i32* %28, align 4
  %145 = sext i32 %144 to i64
  %146 = load i32*, i32** %15, align 8
  %147 = getelementptr inbounds i32, i32* %146, i64 %145
  %148 = load i32, i32* %147, align 4
  %149 = load i32, i32* %17, align 4
  %150 = icmp eq i32 %148, %149
  br i1 %150, label %151, label %159

; <label>:151:                                    ; preds = %143
  %152 = load double, double* %27, align 8
  %153 = load i32, i32* %28, align 4
  %154 = sext i32 %153 to i64
  %155 = load double*, double** %24, align 8
  %156 = getelementptr inbounds double, double* %155, i64 %154
  %157 = load double, double* %156, align 8
  %158 = fmul double %157, %152
  store double %158, double* %156, align 8
  br label %159

; <label>:159:                                    ; preds = %151, %143
  br label %160

; <label>:160:                                    ; preds = %159
  %161 = load i32, i32* %28, align 4
  %162 = add nsw i32 %161, 1
  store i32 %162, i32* %28, align 4
  br label %139

; <label>:163:                                    ; preds = %139
  br label %164

; <label>:164:                                    ; preds = %163, %137
  br label %165

; <label>:165:                                    ; preds = %164, %106
  store i32 0, i32* %28, align 4
  br label %166

; <label>:166:                                    ; preds = %239, %165
  %167 = load i32, i32* %28, align 4
  %168 = load i32, i32* %21, align 4
  %169 = icmp slt i32 %167, %168
  br i1 %169, label %170, label %242

; <label>:170:                                    ; preds = %166
  %171 = load i32, i32* %28, align 4
  %172 = sext i32 %171 to i64
  %173 = load i32*, i32** %15, align 8
  %174 = getelementptr inbounds i32, i32* %173, i64 %172
  %175 = load i32, i32* %174, align 4
  %176 = load i32, i32* %17, align 4
  %177 = icmp eq i32 %175, %176
  br i1 %177, label %178, label %238

; <label>:178:                                    ; preds = %170
  %179 = load i32, i32* %28, align 4
  %180 = sext i32 %179 to i64
  %181 = load double*, double** %24, align 8
  %182 = getelementptr inbounds double, double* %181, i64 %180
  %183 = load double, double* %182, align 8
  store double %183, double* %27, align 8
  %184 = load i32, i32* %28, align 4
  %185 = sext i32 %184 to i64
  %186 = load i32*, i32** %19, align 8
  %187 = getelementptr inbounds i32, i32* %186, i64 %185
  %188 = load i32, i32* %187, align 4
  store i32 %188, i32* %29, align 4
  br label %189

; <label>:189:                                    ; preds = %229, %178
  %190 = load i32, i32* %29, align 4
  %191 = load i32, i32* %28, align 4
  %192 = add nsw i32 %191, 1
  %193 = sext i32 %192 to i64
  %194 = load i32*, i32** %19, align 8
  %195 = getelementptr inbounds i32, i32* %194, i64 %193
  %196 = load i32, i32* %195, align 4
  %197 = icmp slt i32 %190, %196
  br i1 %197, label %198, label %232

; <label>:198:                                    ; preds = %189
  %199 = load i32, i32* %29, align 4
  %200 = sext i32 %199 to i64
  %201 = load i32*, i32** %20, align 8
  %202 = getelementptr inbounds i32, i32* %201, i64 %200
  %203 = load i32, i32* %202, align 4
  %204 = sext i32 %203 to i64
  %205 = load i32*, i32** %16, align 8
  %206 = getelementptr inbounds i32, i32* %205, i64 %204
  %207 = load i32, i32* %206, align 4
  %208 = load i32, i32* %17, align 4
  %209 = icmp eq i32 %207, %208
  br i1 %209, label %210, label %228

; <label>:210:                                    ; preds = %198
  %211 = load i32, i32* %29, align 4
  %212 = sext i32 %211 to i64
  %213 = load double*, double** %18, align 8
  %214 = getelementptr inbounds double, double* %213, i64 %212
  %215 = load double, double* %214, align 8
  %216 = load i32, i32* %29, align 4
  %217 = sext i32 %216 to i64
  %218 = load i32*, i32** %20, align 8
  %219 = getelementptr inbounds i32, i32* %218, i64 %217
  %220 = load i32, i32* %219, align 4
  %221 = sext i32 %220 to i64
  %222 = load double*, double** %23, align 8
  %223 = getelementptr inbounds double, double* %222, i64 %221
  %224 = load double, double* %223, align 8
  %225 = fmul double %215, %224
  %226 = load double, double* %27, align 8
  %227 = fadd double %226, %225
  store double %227, double* %27, align 8
  br label %228

; <label>:228:                                    ; preds = %210, %198
  br label %229

; <label>:229:                                    ; preds = %228
  %230 = load i32, i32* %29, align 4
  %231 = add nsw i32 %230, 1
  store i32 %231, i32* %29, align 4
  br label %189

; <label>:232:                                    ; preds = %189
  %233 = load double, double* %27, align 8
  %234 = load i32, i32* %28, align 4
  %235 = sext i32 %234 to i64
  %236 = load double*, double** %24, align 8
  %237 = getelementptr inbounds double, double* %236, i64 %235
  store double %233, double* %237, align 8
  br label %238

; <label>:238:                                    ; preds = %232, %170
  br label %239

; <label>:239:                                    ; preds = %238
  %240 = load i32, i32* %28, align 4
  %241 = add nsw i32 %240, 1
  store i32 %241, i32* %28, align 4
  br label %166

; <label>:242:                                    ; preds = %166
  %243 = load double, double* %10, align 8
  %244 = fcmp une double %243, 1.000000e+00
  br i1 %244, label %245, label %271

; <label>:245:                                    ; preds = %242
  store i32 0, i32* %28, align 4
  br label %246

; <label>:246:                                    ; preds = %267, %245
  %247 = load i32, i32* %28, align 4
  %248 = load i32, i32* %21, align 4
  %249 = icmp slt i32 %247, %248
  br i1 %249, label %250, label %270

; <label>:250:                                    ; preds = %246
  %251 = load i32, i32* %28, align 4
  %252 = sext i32 %251 to i64
  %253 = load i32*, i32** %15, align 8
  %254 = getelementptr inbounds i32, i32* %253, i64 %252
  %255 = load i32, i32* %254, align 4
  %256 = load i32, i32* %17, align 4
  %257 = icmp eq i32 %255, %256
  br i1 %257, label %258, label %266

; <label>:258:                                    ; preds = %250
  %259 = load double, double* %10, align 8
  %260 = load i32, i32* %28, align 4
  %261 = sext i32 %260 to i64
  %262 = load double*, double** %24, align 8
  %263 = getelementptr inbounds double, double* %262, i64 %261
  %264 = load double, double* %263, align 8
  %265 = fmul double %264, %259
  store double %265, double* %263, align 8
  br label %266

; <label>:266:                                    ; preds = %258, %250
  br label %267

; <label>:267:                                    ; preds = %266
  %268 = load i32, i32* %28, align 4
  %269 = add nsw i32 %268, 1
  store i32 %269, i32* %28, align 4
  br label %246

; <label>:270:                                    ; preds = %246
  br label %271

; <label>:271:                                    ; preds = %270, %242
  %272 = load i32, i32* %30, align 4
  store i32 %272, i32* %9, align 4
  br label %273

; <label>:273:                                    ; preds = %271, %104
  %274 = load i32, i32* %9, align 4
  ret i32 %274
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
