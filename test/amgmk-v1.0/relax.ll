; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/relax.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/relax.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.hypre_CSRMatrix = type { double*, i32*, i32*, i32, i32, i32, i32*, i32, i32 }
%struct.hypre_Vector = type { double*, i32, i32, i32, i32, i32, i32 }

; Function Attrs: nounwind uwtable
define i32 @hypre_BoomerAMGSeqRelax(%struct.hypre_CSRMatrix*, %struct.hypre_Vector*, %struct.hypre_Vector*) #0 {
  %4 = alloca %struct.hypre_CSRMatrix*, align 8
  %5 = alloca %struct.hypre_Vector*, align 8
  %6 = alloca %struct.hypre_Vector*, align 8
  %7 = alloca double*, align 8
  %8 = alloca i32*, align 8
  %9 = alloca i32*, align 8
  %10 = alloca i32, align 4
  %11 = alloca double*, align 8
  %12 = alloca double*, align 8
  %13 = alloca double*, align 8
  %14 = alloca double, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  store %struct.hypre_CSRMatrix* %0, %struct.hypre_CSRMatrix** %4, align 8
  store %struct.hypre_Vector* %1, %struct.hypre_Vector** %5, align 8
  store %struct.hypre_Vector* %2, %struct.hypre_Vector** %6, align 8
  %25 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %4, align 8
  %26 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %25, i32 0, i32 0
  %27 = load double*, double** %26, align 8
  store double* %27, double** %7, align 8
  %28 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %4, align 8
  %29 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %28, i32 0, i32 1
  %30 = load i32*, i32** %29, align 8
  store i32* %30, i32** %8, align 8
  %31 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %4, align 8
  %32 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %31, i32 0, i32 2
  %33 = load i32*, i32** %32, align 8
  store i32* %33, i32** %9, align 8
  %34 = load %struct.hypre_CSRMatrix*, %struct.hypre_CSRMatrix** %4, align 8
  %35 = getelementptr inbounds %struct.hypre_CSRMatrix, %struct.hypre_CSRMatrix* %34, i32 0, i32 3
  %36 = load i32, i32* %35, align 8
  store i32 %36, i32* %10, align 4
  %37 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %38 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %37, i32 0, i32 0
  %39 = load double*, double** %38, align 8
  store double* %39, double** %11, align 8
  %40 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %41 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %40, i32 0, i32 0
  %42 = load double*, double** %41, align 8
  store double* %42, double** %12, align 8
  store i32 0, i32* %23, align 4
  store i32 1, i32* %24, align 4
  %43 = load i32, i32* %10, align 4
  %44 = call i8* @hypre_CAlloc(i32 %43, i32 8)
  %45 = bitcast i8* %44 to double*
  store double* %45, double** %13, align 8
  %46 = call i32 @omp_get_num_threads() #3
  store i32 %46, i32* %24, align 4
  store i32 0, i32* %15, align 4
  br label %47

; <label>:47:                                     ; preds = %61, %3
  %48 = load i32, i32* %15, align 4
  %49 = load i32, i32* %10, align 4
  %50 = icmp slt i32 %48, %49
  br i1 %50, label %51, label %64

; <label>:51:                                     ; preds = %47
  %52 = load i32, i32* %15, align 4
  %53 = sext i32 %52 to i64
  %54 = load double*, double** %11, align 8
  %55 = getelementptr inbounds double, double* %54, i64 %53
  %56 = load double, double* %55, align 8
  %57 = load i32, i32* %15, align 4
  %58 = sext i32 %57 to i64
  %59 = load double*, double** %13, align 8
  %60 = getelementptr inbounds double, double* %59, i64 %58
  store double %56, double* %60, align 8
  br label %61

; <label>:61:                                     ; preds = %51
  %62 = load i32, i32* %15, align 4
  %63 = add nsw i32 %62, 1
  store i32 %63, i32* %15, align 4
  br label %47

; <label>:64:                                     ; preds = %47
  store i32 0, i32* %16, align 4
  br label %65

; <label>:65:                                     ; preds = %210, %64
  %66 = load i32, i32* %16, align 4
  %67 = load i32, i32* %24, align 4
  %68 = icmp slt i32 %66, %67
  br i1 %68, label %69, label %213

; <label>:69:                                     ; preds = %65
  %70 = load i32, i32* %10, align 4
  %71 = load i32, i32* %24, align 4
  %72 = sdiv i32 %70, %71
  store i32 %72, i32* %21, align 4
  %73 = load i32, i32* %10, align 4
  %74 = load i32, i32* %21, align 4
  %75 = load i32, i32* %24, align 4
  %76 = mul nsw i32 %74, %75
  %77 = sub nsw i32 %73, %76
  store i32 %77, i32* %22, align 4
  %78 = load i32, i32* %16, align 4
  %79 = load i32, i32* %22, align 4
  %80 = icmp slt i32 %78, %79
  br i1 %80, label %81, label %94

; <label>:81:                                     ; preds = %69
  %82 = load i32, i32* %16, align 4
  %83 = load i32, i32* %21, align 4
  %84 = mul nsw i32 %82, %83
  %85 = load i32, i32* %16, align 4
  %86 = add nsw i32 %84, %85
  store i32 %86, i32* %19, align 4
  %87 = load i32, i32* %16, align 4
  %88 = add nsw i32 %87, 1
  %89 = load i32, i32* %21, align 4
  %90 = mul nsw i32 %88, %89
  %91 = load i32, i32* %16, align 4
  %92 = add nsw i32 %90, %91
  %93 = add nsw i32 %92, 1
  store i32 %93, i32* %20, align 4
  br label %106

; <label>:94:                                     ; preds = %69
  %95 = load i32, i32* %16, align 4
  %96 = load i32, i32* %21, align 4
  %97 = mul nsw i32 %95, %96
  %98 = load i32, i32* %22, align 4
  %99 = add nsw i32 %97, %98
  store i32 %99, i32* %19, align 4
  %100 = load i32, i32* %16, align 4
  %101 = add nsw i32 %100, 1
  %102 = load i32, i32* %21, align 4
  %103 = mul nsw i32 %101, %102
  %104 = load i32, i32* %22, align 4
  %105 = add nsw i32 %103, %104
  store i32 %105, i32* %20, align 4
  br label %106

; <label>:106:                                    ; preds = %94, %81
  %107 = load i32, i32* %19, align 4
  store i32 %107, i32* %15, align 4
  br label %108

; <label>:108:                                    ; preds = %206, %106
  %109 = load i32, i32* %15, align 4
  %110 = load i32, i32* %20, align 4
  %111 = icmp slt i32 %109, %110
  br i1 %111, label %112, label %209

; <label>:112:                                    ; preds = %108
  %113 = load i32, i32* %15, align 4
  %114 = sext i32 %113 to i64
  %115 = load i32*, i32** %8, align 8
  %116 = getelementptr inbounds i32, i32* %115, i64 %114
  %117 = load i32, i32* %116, align 4
  %118 = sext i32 %117 to i64
  %119 = load double*, double** %7, align 8
  %120 = getelementptr inbounds double, double* %119, i64 %118
  %121 = load double, double* %120, align 8
  %122 = fcmp une double %121, 0.000000e+00
  br i1 %122, label %123, label %205

; <label>:123:                                    ; preds = %112
  %124 = load i32, i32* %15, align 4
  %125 = sext i32 %124 to i64
  %126 = load double*, double** %12, align 8
  %127 = getelementptr inbounds double, double* %126, i64 %125
  %128 = load double, double* %127, align 8
  store double %128, double* %14, align 8
  %129 = load i32, i32* %15, align 4
  %130 = sext i32 %129 to i64
  %131 = load i32*, i32** %8, align 8
  %132 = getelementptr inbounds i32, i32* %131, i64 %130
  %133 = load i32, i32* %132, align 4
  %134 = add nsw i32 %133, 1
  store i32 %134, i32* %18, align 4
  br label %135

; <label>:135:                                    ; preds = %186, %123
  %136 = load i32, i32* %18, align 4
  %137 = load i32, i32* %15, align 4
  %138 = add nsw i32 %137, 1
  %139 = sext i32 %138 to i64
  %140 = load i32*, i32** %8, align 8
  %141 = getelementptr inbounds i32, i32* %140, i64 %139
  %142 = load i32, i32* %141, align 4
  %143 = icmp slt i32 %136, %142
  br i1 %143, label %144, label %189

; <label>:144:                                    ; preds = %135
  %145 = load i32, i32* %18, align 4
  %146 = sext i32 %145 to i64
  %147 = load i32*, i32** %9, align 8
  %148 = getelementptr inbounds i32, i32* %147, i64 %146
  %149 = load i32, i32* %148, align 4
  store i32 %149, i32* %17, align 4
  %150 = load i32, i32* %17, align 4
  %151 = load i32, i32* %19, align 4
  %152 = icmp sge i32 %150, %151
  br i1 %152, label %153, label %171

; <label>:153:                                    ; preds = %144
  %154 = load i32, i32* %17, align 4
  %155 = load i32, i32* %20, align 4
  %156 = icmp slt i32 %154, %155
  br i1 %156, label %157, label %171

; <label>:157:                                    ; preds = %153
  %158 = load i32, i32* %18, align 4
  %159 = sext i32 %158 to i64
  %160 = load double*, double** %7, align 8
  %161 = getelementptr inbounds double, double* %160, i64 %159
  %162 = load double, double* %161, align 8
  %163 = load i32, i32* %17, align 4
  %164 = sext i32 %163 to i64
  %165 = load double*, double** %11, align 8
  %166 = getelementptr inbounds double, double* %165, i64 %164
  %167 = load double, double* %166, align 8
  %168 = fmul double %162, %167
  %169 = load double, double* %14, align 8
  %170 = fsub double %169, %168
  store double %170, double* %14, align 8
  br label %185

; <label>:171:                                    ; preds = %153, %144
  %172 = load i32, i32* %18, align 4
  %173 = sext i32 %172 to i64
  %174 = load double*, double** %7, align 8
  %175 = getelementptr inbounds double, double* %174, i64 %173
  %176 = load double, double* %175, align 8
  %177 = load i32, i32* %17, align 4
  %178 = sext i32 %177 to i64
  %179 = load double*, double** %13, align 8
  %180 = getelementptr inbounds double, double* %179, i64 %178
  %181 = load double, double* %180, align 8
  %182 = fmul double %176, %181
  %183 = load double, double* %14, align 8
  %184 = fsub double %183, %182
  store double %184, double* %14, align 8
  br label %185

; <label>:185:                                    ; preds = %171, %157
  br label %186

; <label>:186:                                    ; preds = %185
  %187 = load i32, i32* %18, align 4
  %188 = add nsw i32 %187, 1
  store i32 %188, i32* %18, align 4
  br label %135

; <label>:189:                                    ; preds = %135
  %190 = load double, double* %14, align 8
  %191 = load i32, i32* %15, align 4
  %192 = sext i32 %191 to i64
  %193 = load i32*, i32** %8, align 8
  %194 = getelementptr inbounds i32, i32* %193, i64 %192
  %195 = load i32, i32* %194, align 4
  %196 = sext i32 %195 to i64
  %197 = load double*, double** %7, align 8
  %198 = getelementptr inbounds double, double* %197, i64 %196
  %199 = load double, double* %198, align 8
  %200 = fdiv double %190, %199
  %201 = load i32, i32* %15, align 4
  %202 = sext i32 %201 to i64
  %203 = load double*, double** %11, align 8
  %204 = getelementptr inbounds double, double* %203, i64 %202
  store double %200, double* %204, align 8
  br label %205

; <label>:205:                                    ; preds = %189, %112
  br label %206

; <label>:206:                                    ; preds = %205
  %207 = load i32, i32* %15, align 4
  %208 = add nsw i32 %207, 1
  store i32 %208, i32* %15, align 4
  br label %108

; <label>:209:                                    ; preds = %108
  br label %210

; <label>:210:                                    ; preds = %209
  %211 = load i32, i32* %16, align 4
  %212 = add nsw i32 %211, 1
  store i32 %212, i32* %16, align 4
  br label %65

; <label>:213:                                    ; preds = %65
  %214 = load double*, double** %13, align 8
  %215 = bitcast double* %214 to i8*
  call void @hypre_Free(i8* %215)
  store double* null, double** %13, align 8
  %216 = load i32, i32* %23, align 4
  ret i32 %216
}

declare i8* @hypre_CAlloc(i32, i32) #1

; Function Attrs: nounwind
declare i32 @omp_get_num_threads() #2

declare void @hypre_Free(i8*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
