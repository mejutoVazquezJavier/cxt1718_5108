; ModuleID = '/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/vector.c'
source_filename = "/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/vector.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.hypre_Vector = type { double*, i32, i32, i32, i32, i32, i32 }

@.str = private unnamed_addr constant [2 x i8] c"r\00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"%le\00", align 1
@stderr = external global %struct._IO_FILE*, align 8
@.str.3 = private unnamed_addr constant [25 x i8] c"hypre_assert failed: %s\0A\00", align 1
@.str.4 = private unnamed_addr constant [36 x i8] c"hypre_VectorNumVectors(vector) == 1\00", align 1
@.str.5 = private unnamed_addr constant [70 x i8] c"/home/fran/llvm-3.9.0.src/tools/cxt1718_5108/test/amgmk-v1.0/vector.c\00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str.7 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@.str.8 = private unnamed_addr constant [23 x i8] c"%d vectors of size %d\0A\00", align 1
@.str.9 = private unnamed_addr constant [11 x i8] c"vector %d\0A\00", align 1
@.str.10 = private unnamed_addr constant [7 x i8] c"%.14e\0A\00", align 1

; Function Attrs: nounwind uwtable
define %struct.hypre_Vector* @hypre_SeqVectorCreate(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca %struct.hypre_Vector*, align 8
  store i32 %0, i32* %2, align 4
  %4 = call i8* @hypre_CAlloc(i32 1, i32 32)
  %5 = bitcast i8* %4 to %struct.hypre_Vector*
  store %struct.hypre_Vector* %5, %struct.hypre_Vector** %3, align 8
  %6 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %7 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %6, i32 0, i32 0
  store double* null, double** %7, align 8
  %8 = load i32, i32* %2, align 4
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 1
  store i32 %8, i32* %10, align 8
  %11 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %12 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %11, i32 0, i32 3
  store i32 1, i32* %12, align 8
  %13 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %14 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %13, i32 0, i32 4
  store i32 0, i32* %14, align 4
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 2
  store i32 1, i32* %16, align 4
  %17 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  ret %struct.hypre_Vector* %17
}

declare i8* @hypre_CAlloc(i32, i32) #1

; Function Attrs: nounwind uwtable
define %struct.hypre_Vector* @hypre_SeqMultiVectorCreate(i32, i32) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca %struct.hypre_Vector*, align 8
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %6 = load i32, i32* %3, align 4
  %7 = call %struct.hypre_Vector* @hypre_SeqVectorCreate(i32 %6)
  store %struct.hypre_Vector* %7, %struct.hypre_Vector** %5, align 8
  %8 = load i32, i32* %4, align 4
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 3
  store i32 %8, i32* %10, align 8
  %11 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  ret %struct.hypre_Vector* %11
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorDestroy(%struct.hypre_Vector*) #0 {
  %2 = alloca %struct.hypre_Vector*, align 8
  %3 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %2, align 8
  store i32 0, i32* %3, align 4
  %4 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %5 = icmp ne %struct.hypre_Vector* %4, null
  br i1 %5, label %6, label %21

; <label>:6:                                      ; preds = %1
  %7 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %8 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %7, i32 0, i32 2
  %9 = load i32, i32* %8, align 4
  %10 = icmp ne i32 %9, 0
  br i1 %10, label %11, label %18

; <label>:11:                                     ; preds = %6
  %12 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %13 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %12, i32 0, i32 0
  %14 = load double*, double** %13, align 8
  %15 = bitcast double* %14 to i8*
  call void @hypre_Free(i8* %15)
  %16 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %17 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %16, i32 0, i32 0
  store double* null, double** %17, align 8
  br label %18

; <label>:18:                                     ; preds = %11, %6
  %19 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %20 = bitcast %struct.hypre_Vector* %19 to i8*
  call void @hypre_Free(i8* %20)
  store %struct.hypre_Vector* null, %struct.hypre_Vector** %2, align 8
  br label %21

; <label>:21:                                     ; preds = %18, %1
  %22 = load i32, i32* %3, align 4
  ret i32 %22
}

declare void @hypre_Free(i8*) #1

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector*) #0 {
  %2 = alloca %struct.hypre_Vector*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %2, align 8
  %7 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %8 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %7, i32 0, i32 1
  %9 = load i32, i32* %8, align 8
  store i32 %9, i32* %3, align 4
  store i32 0, i32* %4, align 4
  %10 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %11 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %10, i32 0, i32 3
  %12 = load i32, i32* %11, align 8
  store i32 %12, i32* %5, align 4
  %13 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %14 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %13, i32 0, i32 4
  %15 = load i32, i32* %14, align 4
  store i32 %15, i32* %6, align 4
  %16 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %17 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %16, i32 0, i32 0
  %18 = load double*, double** %17, align 8
  %19 = icmp ne double* %18, null
  br i1 %19, label %28, label %20

; <label>:20:                                     ; preds = %1
  %21 = load i32, i32* %5, align 4
  %22 = load i32, i32* %3, align 4
  %23 = mul nsw i32 %21, %22
  %24 = call i8* @hypre_CAlloc(i32 %23, i32 8)
  %25 = bitcast i8* %24 to double*
  %26 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %27 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %26, i32 0, i32 0
  store double* %25, double** %27, align 8
  br label %28

; <label>:28:                                     ; preds = %20, %1
  %29 = load i32, i32* %6, align 4
  %30 = icmp eq i32 %29, 0
  br i1 %30, label %31, label %37

; <label>:31:                                     ; preds = %28
  %32 = load i32, i32* %3, align 4
  %33 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %34 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %33, i32 0, i32 5
  store i32 %32, i32* %34, align 8
  %35 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %36 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %35, i32 0, i32 6
  store i32 1, i32* %36, align 4
  br label %50

; <label>:37:                                     ; preds = %28
  %38 = load i32, i32* %6, align 4
  %39 = icmp eq i32 %38, 1
  br i1 %39, label %40, label %46

; <label>:40:                                     ; preds = %37
  %41 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %42 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %41, i32 0, i32 5
  store i32 1, i32* %42, align 8
  %43 = load i32, i32* %5, align 4
  %44 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %45 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %44, i32 0, i32 6
  store i32 %43, i32* %45, align 4
  br label %49

; <label>:46:                                     ; preds = %37
  %47 = load i32, i32* %4, align 4
  %48 = add nsw i32 %47, 1
  store i32 %48, i32* %4, align 4
  br label %49

; <label>:49:                                     ; preds = %46, %40
  br label %50

; <label>:50:                                     ; preds = %49, %31
  %51 = load i32, i32* %4, align 4
  ret i32 %51
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorSetDataOwner(%struct.hypre_Vector*, i32) #0 {
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  %6 = load i32, i32* %4, align 4
  %7 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %8 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %7, i32 0, i32 2
  store i32 %6, i32* %8, align 4
  %9 = load i32, i32* %5, align 4
  ret i32 %9
}

; Function Attrs: nounwind uwtable
define %struct.hypre_Vector* @hypre_SeqVectorRead(i8*) #0 {
  %2 = alloca i8*, align 8
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca %struct._IO_FILE*, align 8
  %5 = alloca double*, align 8
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  store i8* %0, i8** %2, align 8
  %8 = load i8*, i8** %2, align 8
  %9 = call %struct._IO_FILE* @fopen(i8* %8, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  store %struct._IO_FILE* %9, %struct._IO_FILE** %4, align 8
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8
  %11 = call i32 (%struct._IO_FILE*, i8*, ...) @__isoc99_fscanf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %6)
  %12 = load i32, i32* %6, align 4
  %13 = call %struct.hypre_Vector* @hypre_SeqVectorCreate(i32 %12)
  store %struct.hypre_Vector* %13, %struct.hypre_Vector** %3, align 8
  %14 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %15 = call i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector* %14)
  %16 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %17 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %16, i32 0, i32 0
  %18 = load double*, double** %17, align 8
  store double* %18, double** %5, align 8
  store i32 0, i32* %7, align 4
  br label %19

; <label>:19:                                     ; preds = %30, %1
  %20 = load i32, i32* %7, align 4
  %21 = load i32, i32* %6, align 4
  %22 = icmp slt i32 %20, %21
  br i1 %22, label %23, label %33

; <label>:23:                                     ; preds = %19
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8
  %25 = load i32, i32* %7, align 4
  %26 = sext i32 %25 to i64
  %27 = load double*, double** %5, align 8
  %28 = getelementptr inbounds double, double* %27, i64 %26
  %29 = call i32 (%struct._IO_FILE*, i8*, ...) @__isoc99_fscanf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), double* %28)
  br label %30

; <label>:30:                                     ; preds = %23
  %31 = load i32, i32* %7, align 4
  %32 = add nsw i32 %31, 1
  store i32 %32, i32* %7, align 4
  br label %19

; <label>:33:                                     ; preds = %19
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8
  %35 = call i32 @fclose(%struct._IO_FILE* %34)
  %36 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %37 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %36, i32 0, i32 3
  %38 = load i32, i32* %37, align 8
  %39 = icmp eq i32 %38, 1
  br i1 %39, label %43, label %40

; <label>:40:                                     ; preds = %33
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %42 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %41, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.4, i32 0, i32 0))
  call void @hypre_error_handler(i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.5, i32 0, i32 0), i32 177, i32 1)
  br label %43

; <label>:43:                                     ; preds = %40, %33
  %44 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  ret %struct.hypre_Vector* %44
}

declare %struct._IO_FILE* @fopen(i8*, i8*) #1

declare i32 @__isoc99_fscanf(%struct._IO_FILE*, i8*, ...) #1

declare i32 @fclose(%struct._IO_FILE*) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

declare void @hypre_error_handler(i8*, i32, i32) #1

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorPrint(%struct.hypre_Vector*, i8*) #0 {
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca i8*, align 8
  %5 = alloca %struct._IO_FILE*, align 8
  %6 = alloca double*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %3, align 8
  store i8* %1, i8** %4, align 8
  store i32 0, i32* %13, align 4
  %14 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %15 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %14, i32 0, i32 3
  %16 = load i32, i32* %15, align 8
  store i32 %16, i32* %8, align 4
  %17 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %18 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %17, i32 0, i32 5
  %19 = load i32, i32* %18, align 8
  store i32 %19, i32* %9, align 4
  %20 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %21 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %20, i32 0, i32 6
  %22 = load i32, i32* %21, align 4
  store i32 %22, i32* %10, align 4
  %23 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %24 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %23, i32 0, i32 0
  %25 = load double*, double** %24, align 8
  store double* %25, double** %6, align 8
  %26 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %27 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %26, i32 0, i32 1
  %28 = load i32, i32* %27, align 8
  store i32 %28, i32* %7, align 4
  %29 = load i8*, i8** %4, align 8
  %30 = call %struct._IO_FILE* @fopen(i8* %29, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  store %struct._IO_FILE* %30, %struct._IO_FILE** %5, align 8
  %31 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %32 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %31, i32 0, i32 3
  %33 = load i32, i32* %32, align 8
  %34 = icmp eq i32 %33, 1
  br i1 %34, label %35, label %39

; <label>:35:                                     ; preds = %2
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %37 = load i32, i32* %7, align 4
  %38 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.7, i32 0, i32 0), i32 %37)
  br label %44

; <label>:39:                                     ; preds = %2
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %41 = load i32, i32* %8, align 4
  %42 = load i32, i32* %7, align 4
  %43 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i32 0, i32 0), i32 %41, i32 %42)
  br label %44

; <label>:44:                                     ; preds = %39, %35
  %45 = load i32, i32* %8, align 4
  %46 = icmp sgt i32 %45, 1
  br i1 %46, label %47, label %82

; <label>:47:                                     ; preds = %44
  store i32 0, i32* %12, align 4
  br label %48

; <label>:48:                                     ; preds = %78, %47
  %49 = load i32, i32* %12, align 4
  %50 = load i32, i32* %8, align 4
  %51 = icmp slt i32 %49, %50
  br i1 %51, label %52, label %81

; <label>:52:                                     ; preds = %48
  %53 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %54 = load i32, i32* %12, align 4
  %55 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %53, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.9, i32 0, i32 0), i32 %54)
  store i32 0, i32* %11, align 4
  br label %56

; <label>:56:                                     ; preds = %74, %52
  %57 = load i32, i32* %11, align 4
  %58 = load i32, i32* %7, align 4
  %59 = icmp slt i32 %57, %58
  br i1 %59, label %60, label %77

; <label>:60:                                     ; preds = %56
  %61 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %62 = load i32, i32* %12, align 4
  %63 = load i32, i32* %9, align 4
  %64 = mul nsw i32 %62, %63
  %65 = load i32, i32* %11, align 4
  %66 = load i32, i32* %10, align 4
  %67 = mul nsw i32 %65, %66
  %68 = add nsw i32 %64, %67
  %69 = sext i32 %68 to i64
  %70 = load double*, double** %6, align 8
  %71 = getelementptr inbounds double, double* %70, i64 %69
  %72 = load double, double* %71, align 8
  %73 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %61, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.10, i32 0, i32 0), double %72)
  br label %74

; <label>:74:                                     ; preds = %60
  %75 = load i32, i32* %11, align 4
  %76 = add nsw i32 %75, 1
  store i32 %76, i32* %11, align 4
  br label %56

; <label>:77:                                     ; preds = %56
  br label %78

; <label>:78:                                     ; preds = %77
  %79 = load i32, i32* %12, align 4
  %80 = add nsw i32 %79, 1
  store i32 %80, i32* %12, align 4
  br label %48

; <label>:81:                                     ; preds = %48
  br label %99

; <label>:82:                                     ; preds = %44
  store i32 0, i32* %11, align 4
  br label %83

; <label>:83:                                     ; preds = %95, %82
  %84 = load i32, i32* %11, align 4
  %85 = load i32, i32* %7, align 4
  %86 = icmp slt i32 %84, %85
  br i1 %86, label %87, label %98

; <label>:87:                                     ; preds = %83
  %88 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %89 = load i32, i32* %11, align 4
  %90 = sext i32 %89 to i64
  %91 = load double*, double** %6, align 8
  %92 = getelementptr inbounds double, double* %91, i64 %90
  %93 = load double, double* %92, align 8
  %94 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %88, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.10, i32 0, i32 0), double %93)
  br label %95

; <label>:95:                                     ; preds = %87
  %96 = load i32, i32* %11, align 4
  %97 = add nsw i32 %96, 1
  store i32 %97, i32* %11, align 4
  br label %83

; <label>:98:                                     ; preds = %83
  br label %99

; <label>:99:                                     ; preds = %98, %81
  %100 = load %struct._IO_FILE*, %struct._IO_FILE** %5, align 8
  %101 = call i32 @fclose(%struct._IO_FILE* %100)
  %102 = load i32, i32* %13, align 4
  ret i32 %102
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorSetConstantValues(%struct.hypre_Vector*, double) #0 {
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca double, align 8
  %5 = alloca double*, align 8
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %3, align 8
  store double %1, double* %4, align 8
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 0
  %11 = load double*, double** %10, align 8
  store double* %11, double** %5, align 8
  %12 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %13 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %12, i32 0, i32 1
  %14 = load i32, i32* %13, align 8
  store i32 %14, i32* %6, align 4
  store i32 0, i32* %8, align 4
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 3
  %17 = load i32, i32* %16, align 8
  %18 = load i32, i32* %6, align 4
  %19 = mul nsw i32 %18, %17
  store i32 %19, i32* %6, align 4
  store i32 0, i32* %7, align 4
  br label %20

; <label>:20:                                     ; preds = %30, %2
  %21 = load i32, i32* %7, align 4
  %22 = load i32, i32* %6, align 4
  %23 = icmp slt i32 %21, %22
  br i1 %23, label %24, label %33

; <label>:24:                                     ; preds = %20
  %25 = load double, double* %4, align 8
  %26 = load i32, i32* %7, align 4
  %27 = sext i32 %26 to i64
  %28 = load double*, double** %5, align 8
  %29 = getelementptr inbounds double, double* %28, i64 %27
  store double %25, double* %29, align 8
  br label %30

; <label>:30:                                     ; preds = %24
  %31 = load i32, i32* %7, align 4
  %32 = add nsw i32 %31, 1
  store i32 %32, i32* %7, align 4
  br label %20

; <label>:33:                                     ; preds = %20
  %34 = load i32, i32* %8, align 4
  ret i32 %34
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorCopy(%struct.hypre_Vector*, %struct.hypre_Vector*) #0 {
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca double*, align 8
  %6 = alloca double*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %3, align 8
  store %struct.hypre_Vector* %1, %struct.hypre_Vector** %4, align 8
  %10 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %11 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %10, i32 0, i32 0
  %12 = load double*, double** %11, align 8
  store double* %12, double** %5, align 8
  %13 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %14 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %13, i32 0, i32 0
  %15 = load double*, double** %14, align 8
  store double* %15, double** %6, align 8
  %16 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %17 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %16, i32 0, i32 1
  %18 = load i32, i32* %17, align 8
  store i32 %18, i32* %7, align 4
  store i32 0, i32* %9, align 4
  %19 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %20 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %19, i32 0, i32 3
  %21 = load i32, i32* %20, align 8
  %22 = load i32, i32* %7, align 4
  %23 = mul nsw i32 %22, %21
  store i32 %23, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %24

; <label>:24:                                     ; preds = %38, %2
  %25 = load i32, i32* %8, align 4
  %26 = load i32, i32* %7, align 4
  %27 = icmp slt i32 %25, %26
  br i1 %27, label %28, label %41

; <label>:28:                                     ; preds = %24
  %29 = load i32, i32* %8, align 4
  %30 = sext i32 %29 to i64
  %31 = load double*, double** %5, align 8
  %32 = getelementptr inbounds double, double* %31, i64 %30
  %33 = load double, double* %32, align 8
  %34 = load i32, i32* %8, align 4
  %35 = sext i32 %34 to i64
  %36 = load double*, double** %6, align 8
  %37 = getelementptr inbounds double, double* %36, i64 %35
  store double %33, double* %37, align 8
  br label %38

; <label>:38:                                     ; preds = %28
  %39 = load i32, i32* %8, align 4
  %40 = add nsw i32 %39, 1
  store i32 %40, i32* %8, align 4
  br label %24

; <label>:41:                                     ; preds = %24
  %42 = load i32, i32* %9, align 4
  ret i32 %42
}

; Function Attrs: nounwind uwtable
define %struct.hypre_Vector* @hypre_SeqVectorCloneDeep(%struct.hypre_Vector*) #0 {
  %2 = alloca %struct.hypre_Vector*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca %struct.hypre_Vector*, align 8
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %2, align 8
  %6 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %7 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %6, i32 0, i32 1
  %8 = load i32, i32* %7, align 8
  store i32 %8, i32* %3, align 4
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 3
  %11 = load i32, i32* %10, align 8
  store i32 %11, i32* %4, align 4
  %12 = load i32, i32* %3, align 4
  %13 = load i32, i32* %4, align 4
  %14 = call %struct.hypre_Vector* @hypre_SeqMultiVectorCreate(i32 %12, i32 %13)
  store %struct.hypre_Vector* %14, %struct.hypre_Vector** %5, align 8
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 4
  %17 = load i32, i32* %16, align 4
  %18 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %19 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %18, i32 0, i32 4
  store i32 %17, i32* %19, align 4
  %20 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %21 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %20, i32 0, i32 5
  %22 = load i32, i32* %21, align 8
  %23 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %24 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %23, i32 0, i32 5
  store i32 %22, i32* %24, align 8
  %25 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %26 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %25, i32 0, i32 6
  %27 = load i32, i32* %26, align 4
  %28 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %29 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %28, i32 0, i32 6
  store i32 %27, i32* %29, align 4
  %30 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %31 = call i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector* %30)
  %32 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %33 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %34 = call i32 @hypre_SeqVectorCopy(%struct.hypre_Vector* %32, %struct.hypre_Vector* %33)
  %35 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  ret %struct.hypre_Vector* %35
}

; Function Attrs: nounwind uwtable
define %struct.hypre_Vector* @hypre_SeqVectorCloneShallow(%struct.hypre_Vector*) #0 {
  %2 = alloca %struct.hypre_Vector*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca %struct.hypre_Vector*, align 8
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %2, align 8
  %6 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %7 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %6, i32 0, i32 1
  %8 = load i32, i32* %7, align 8
  store i32 %8, i32* %3, align 4
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 3
  %11 = load i32, i32* %10, align 8
  store i32 %11, i32* %4, align 4
  %12 = load i32, i32* %3, align 4
  %13 = load i32, i32* %4, align 4
  %14 = call %struct.hypre_Vector* @hypre_SeqMultiVectorCreate(i32 %12, i32 %13)
  store %struct.hypre_Vector* %14, %struct.hypre_Vector** %5, align 8
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 4
  %17 = load i32, i32* %16, align 4
  %18 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %19 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %18, i32 0, i32 4
  store i32 %17, i32* %19, align 4
  %20 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %21 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %20, i32 0, i32 5
  %22 = load i32, i32* %21, align 8
  %23 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %24 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %23, i32 0, i32 5
  store i32 %22, i32* %24, align 8
  %25 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %26 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %25, i32 0, i32 6
  %27 = load i32, i32* %26, align 4
  %28 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %29 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %28, i32 0, i32 6
  store i32 %27, i32* %29, align 4
  %30 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %31 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %30, i32 0, i32 0
  %32 = load double*, double** %31, align 8
  %33 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %34 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %33, i32 0, i32 0
  store double* %32, double** %34, align 8
  %35 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %36 = call i32 @hypre_SeqVectorSetDataOwner(%struct.hypre_Vector* %35, i32 0)
  %37 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %38 = call i32 @hypre_SeqVectorInitialize(%struct.hypre_Vector* %37)
  %39 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  ret %struct.hypre_Vector* %39
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorScale(double, %struct.hypre_Vector*) #0 {
  %3 = alloca double, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca double*, align 8
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store double %0, double* %3, align 8
  store %struct.hypre_Vector* %1, %struct.hypre_Vector** %4, align 8
  %9 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %10 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %9, i32 0, i32 0
  %11 = load double*, double** %10, align 8
  store double* %11, double** %5, align 8
  %12 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %13 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %12, i32 0, i32 1
  %14 = load i32, i32* %13, align 8
  store i32 %14, i32* %6, align 4
  store i32 0, i32* %8, align 4
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 3
  %17 = load i32, i32* %16, align 8
  %18 = load i32, i32* %6, align 4
  %19 = mul nsw i32 %18, %17
  store i32 %19, i32* %6, align 4
  store i32 0, i32* %7, align 4
  br label %20

; <label>:20:                                     ; preds = %32, %2
  %21 = load i32, i32* %7, align 4
  %22 = load i32, i32* %6, align 4
  %23 = icmp slt i32 %21, %22
  br i1 %23, label %24, label %35

; <label>:24:                                     ; preds = %20
  %25 = load double, double* %3, align 8
  %26 = load i32, i32* %7, align 4
  %27 = sext i32 %26 to i64
  %28 = load double*, double** %5, align 8
  %29 = getelementptr inbounds double, double* %28, i64 %27
  %30 = load double, double* %29, align 8
  %31 = fmul double %30, %25
  store double %31, double* %29, align 8
  br label %32

; <label>:32:                                     ; preds = %24
  %33 = load i32, i32* %7, align 4
  %34 = add nsw i32 %33, 1
  store i32 %34, i32* %7, align 4
  br label %20

; <label>:35:                                     ; preds = %20
  %36 = load i32, i32* %8, align 4
  ret i32 %36
}

; Function Attrs: nounwind uwtable
define i32 @hypre_SeqVectorAxpy(double, %struct.hypre_Vector*, %struct.hypre_Vector*) #0 {
  %4 = alloca double, align 8
  %5 = alloca %struct.hypre_Vector*, align 8
  %6 = alloca %struct.hypre_Vector*, align 8
  %7 = alloca double*, align 8
  %8 = alloca double*, align 8
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  store double %0, double* %4, align 8
  store %struct.hypre_Vector* %1, %struct.hypre_Vector** %5, align 8
  store %struct.hypre_Vector* %2, %struct.hypre_Vector** %6, align 8
  %12 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %13 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %12, i32 0, i32 0
  %14 = load double*, double** %13, align 8
  store double* %14, double** %7, align 8
  %15 = load %struct.hypre_Vector*, %struct.hypre_Vector** %6, align 8
  %16 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %15, i32 0, i32 0
  %17 = load double*, double** %16, align 8
  store double* %17, double** %8, align 8
  %18 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %19 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %18, i32 0, i32 1
  %20 = load i32, i32* %19, align 8
  store i32 %20, i32* %9, align 4
  store i32 0, i32* %11, align 4
  %21 = load %struct.hypre_Vector*, %struct.hypre_Vector** %5, align 8
  %22 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %21, i32 0, i32 3
  %23 = load i32, i32* %22, align 8
  %24 = load i32, i32* %9, align 4
  %25 = mul nsw i32 %24, %23
  store i32 %25, i32* %9, align 4
  store i32 0, i32* %10, align 4
  br label %26

; <label>:26:                                     ; preds = %44, %3
  %27 = load i32, i32* %10, align 4
  %28 = load i32, i32* %9, align 4
  %29 = icmp slt i32 %27, %28
  br i1 %29, label %30, label %47

; <label>:30:                                     ; preds = %26
  %31 = load double, double* %4, align 8
  %32 = load i32, i32* %10, align 4
  %33 = sext i32 %32 to i64
  %34 = load double*, double** %7, align 8
  %35 = getelementptr inbounds double, double* %34, i64 %33
  %36 = load double, double* %35, align 8
  %37 = fmul double %31, %36
  %38 = load i32, i32* %10, align 4
  %39 = sext i32 %38 to i64
  %40 = load double*, double** %8, align 8
  %41 = getelementptr inbounds double, double* %40, i64 %39
  %42 = load double, double* %41, align 8
  %43 = fadd double %42, %37
  store double %43, double* %41, align 8
  br label %44

; <label>:44:                                     ; preds = %30
  %45 = load i32, i32* %10, align 4
  %46 = add nsw i32 %45, 1
  store i32 %46, i32* %10, align 4
  br label %26

; <label>:47:                                     ; preds = %26
  %48 = load i32, i32* %11, align 4
  ret i32 %48
}

; Function Attrs: nounwind uwtable
define double @hypre_SeqVectorInnerProd(%struct.hypre_Vector*, %struct.hypre_Vector*) #0 {
  %3 = alloca %struct.hypre_Vector*, align 8
  %4 = alloca %struct.hypre_Vector*, align 8
  %5 = alloca double*, align 8
  %6 = alloca double*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca double, align 8
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %3, align 8
  store %struct.hypre_Vector* %1, %struct.hypre_Vector** %4, align 8
  %10 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %11 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %10, i32 0, i32 0
  %12 = load double*, double** %11, align 8
  store double* %12, double** %5, align 8
  %13 = load %struct.hypre_Vector*, %struct.hypre_Vector** %4, align 8
  %14 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %13, i32 0, i32 0
  %15 = load double*, double** %14, align 8
  store double* %15, double** %6, align 8
  %16 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %17 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %16, i32 0, i32 1
  %18 = load i32, i32* %17, align 8
  store i32 %18, i32* %7, align 4
  store double 0.000000e+00, double* %9, align 8
  %19 = load %struct.hypre_Vector*, %struct.hypre_Vector** %3, align 8
  %20 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %19, i32 0, i32 3
  %21 = load i32, i32* %20, align 8
  %22 = load i32, i32* %7, align 4
  %23 = mul nsw i32 %22, %21
  store i32 %23, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %24

; <label>:24:                                     ; preds = %42, %2
  %25 = load i32, i32* %8, align 4
  %26 = load i32, i32* %7, align 4
  %27 = icmp slt i32 %25, %26
  br i1 %27, label %28, label %45

; <label>:28:                                     ; preds = %24
  %29 = load i32, i32* %8, align 4
  %30 = sext i32 %29 to i64
  %31 = load double*, double** %6, align 8
  %32 = getelementptr inbounds double, double* %31, i64 %30
  %33 = load double, double* %32, align 8
  %34 = load i32, i32* %8, align 4
  %35 = sext i32 %34 to i64
  %36 = load double*, double** %5, align 8
  %37 = getelementptr inbounds double, double* %36, i64 %35
  %38 = load double, double* %37, align 8
  %39 = fmul double %33, %38
  %40 = load double, double* %9, align 8
  %41 = fadd double %40, %39
  store double %41, double* %9, align 8
  br label %42

; <label>:42:                                     ; preds = %28
  %43 = load i32, i32* %8, align 4
  %44 = add nsw i32 %43, 1
  store i32 %44, i32* %8, align 4
  br label %24

; <label>:45:                                     ; preds = %24
  %46 = load double, double* %9, align 8
  ret double %46
}

; Function Attrs: nounwind uwtable
define double @hypre_VectorSumElts(%struct.hypre_Vector*) #0 {
  %2 = alloca %struct.hypre_Vector*, align 8
  %3 = alloca double, align 8
  %4 = alloca double*, align 8
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store %struct.hypre_Vector* %0, %struct.hypre_Vector** %2, align 8
  store double 0.000000e+00, double* %3, align 8
  %7 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %8 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %7, i32 0, i32 0
  %9 = load double*, double** %8, align 8
  store double* %9, double** %4, align 8
  %10 = load %struct.hypre_Vector*, %struct.hypre_Vector** %2, align 8
  %11 = getelementptr inbounds %struct.hypre_Vector, %struct.hypre_Vector* %10, i32 0, i32 1
  %12 = load i32, i32* %11, align 8
  store i32 %12, i32* %5, align 4
  store i32 0, i32* %6, align 4
  br label %13

; <label>:13:                                     ; preds = %25, %1
  %14 = load i32, i32* %6, align 4
  %15 = load i32, i32* %5, align 4
  %16 = icmp slt i32 %14, %15
  br i1 %16, label %17, label %28

; <label>:17:                                     ; preds = %13
  %18 = load i32, i32* %6, align 4
  %19 = sext i32 %18 to i64
  %20 = load double*, double** %4, align 8
  %21 = getelementptr inbounds double, double* %20, i64 %19
  %22 = load double, double* %21, align 8
  %23 = load double, double* %3, align 8
  %24 = fadd double %23, %22
  store double %24, double* %3, align 8
  br label %25

; <label>:25:                                     ; preds = %17
  %26 = load i32, i32* %6, align 4
  %27 = add nsw i32 %26, 1
  store i32 %27, i32* %6, align 4
  br label %13

; <label>:28:                                     ; preds = %13
  %29 = load double, double* %3, align 8
  ret double %29
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-4ubuntu3~16.04.2 (tags/RELEASE_391/rc2)"}
