#include <iostream>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Signals.h"
#include "llvm/IR/Function.h"


#include <Analyzer.h>

using namespace std;
using namespace llvm;


std::unique_ptr<llvm::Module> getModuleFromFile(llvm::LLVMContext &context, const StringRef file) {
    llvm::SMDiagnostic Err;

    auto Mod = llvm::parseIRFile(file, Err, context);

    if (Err.getLoc().isValid()) {
      std::cerr << Err.getMessage().str() << "\n";
      exit(1);
    }
    return Mod;
}


int main(int argc, char *argv[]) 
{
  llvm_shutdown_obj Y; // Call llvm_shutdown() on exit.
  LLVMContext context;

  llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);
  llvm::PrettyStackTraceProgram X(argc, argv);

  // Parse the command-line arguments
  if (argc <= 1) {
    std::cerr << argv[0] << ": error: no input files\n";
    return 1;
  }

  // For each input file specified in the command-line
  for (int narg = 1; narg < argc; ++narg) {
      const std::string file = argv[narg];

      // Parse the input file .ll and create a LLVM Module object.
      unique_ptr<Module> M = getModuleFromFile(context, file);
      if (!M) {
        std::cerr << "error: Invalid llvm assemble/bitcode file\n";
        return 1;
      }

      // Analyze the Key Performance Indicators (KPIs) of the program
      // in the LLVM intermediate representation.
      analyzeKeyPerformanceIndicators(M.get());
  }

  return 0;
}
